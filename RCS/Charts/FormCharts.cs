﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RCS.Charts
{
    public partial class FormCharts : Form
    {
        public FormCharts()
        {
            InitializeComponent();
        }

        private void uiSymbolButton1_Click(object sender, EventArgs e)
        {
            UIBarOption option = new UIBarOption();
            option.Title = new UITitle();
            option.Title.Text = "SunnyUI";
            option.Title.SubText = "BarChart";

            //设置Legend
            option.Legend = new UILegend();
            option.Legend.Orient = UIOrient.Horizontal;
            option.Legend.Top = UITopAlignment.Top;
            option.Legend.Left = UILeftAlignment.Left;
            option.Legend.AddData("Bar1");
            option.Legend.AddData("Bar2");

            var series = new UIBarSeries();
            series.Name = "Bar1";
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            option.Series.Add(series);

            series = new UIBarSeries();
            series.Name = "Bar2";
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            series.AddData(-1.1);
            option.Series.Add(series);

            option.XAxis.Data.Add("D1");
            option.XAxis.Data.Add("D2");
            option.XAxis.Data.Add("D3");
            option.XAxis.Data.Add("D4");
            option.XAxis.Data.Add("D5");
            option.XAxis.Data.Add("D6");
            option.XAxis.Data.Add("D7");
            option.XAxis.Data.Add("D8");

            option.ToolTip.Visible = true;
            option.YAxis.Scale = true;

            option.XAxis.Name = "日期";
            option.YAxis.Name = "数值";
            option.YAxis.AxisLabel.DecimalCount = 1;
            option.YAxis.AxisLabel.AutoFormat = false;

            option.YAxisScaleLines.Add(new UIScaleLine() { Color = Color.Red, Name = "上限", Value = 12 });
            option.YAxisScaleLines.Add(new UIScaleLine() { Color = Color.Gold, Name = "下限", Value = -20 });

            option.ToolTip.AxisPointer.Type = UIAxisPointerType.Shadow;

            option.ShowValue = true;

            BarChart.SetOption(option);

            uiSymbolButton2.Enabled = true;
        }

        private void uiSymbolButton2_Click(object sender, EventArgs e)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            BarChart.Update("Bar1", 0, random.Next(10));
            BarChart.Update("Bar1", 1, random.Next(10));
            BarChart.Update("Bar1", 2, random.Next(10));
            BarChart.Update("Bar1", 3, random.Next(10));
            BarChart.Update("Bar1", 4, random.Next(10));
            BarChart.Update("Bar1", 5, random.Next(10));
            BarChart.Update("Bar1", 6, random.Next(10));
            BarChart.Update("Bar1", 7, random.Next(10));

            BarChart.Update("Bar2", 0, random.Next(10));
            BarChart.Update("Bar2", 1, random.Next(10));
            BarChart.Update("Bar2", 2, random.Next(10));
            BarChart.Update("Bar2", 3, random.Next(10));
            BarChart.Update("Bar2", 4, random.Next(10));
            BarChart.Update("Bar2", 5, random.Next(10));
            BarChart.Update("Bar2", 6, random.Next(10));
            BarChart.Update("Bar2", 7, random.Next(10));

            BarChart.Refresh();
        }

        private void uiImageButton3_Click(object sender, EventArgs e)
        {
            BarChart.ChartStyleType = UIChartStyleType.Dark;
        }

        private void uiImageButton2_Click(object sender, EventArgs e)
        {
            BarChart.ChartStyleType = UIChartStyleType.Plain;
        }

        private void uiImageButton1_Click(object sender, EventArgs e)
        {
            BarChart.ChartStyleType = UIChartStyleType.Default;
        }

        private void FormCharts_Load(object sender, EventArgs e)
        {

        }
    }
}
