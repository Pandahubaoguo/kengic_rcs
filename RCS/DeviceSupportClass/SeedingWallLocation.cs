﻿using Kengic.RCS.Common;
using Kengic.RCS.Models.Table;
using Newtonsoft.Json;
using RCS.DeviceForm;
using S7Comm;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using WinformControlLibraryExtension;

namespace RCS.DeviceSupportClass
{
    public class SeedingWallLocation
    {
        #region 自定义变量
        /// <summary>
        /// 主窗体
        /// </summary>
        public FormMain frmMain;
        public ISqlSugarClient sqlSugarClient = new SqlSugarScope(new ConnectionConfig()
        {
            ConnectionString = ConfigurationManager.AppSettings["LocalConnectionString"],
            DbType = SqlSugar.DbType.SqlServer,
            InitKeyType = InitKeyType.Attribute,
            IsAutoCloseConnection = true  //是否自动关闭连接  Yes
        });
        /// <summary>
        /// 画设备的图片
        /// </summary>
        public Label pic = new Label();
        /// <summary>
        /// 画设备的图片
        /// </summary>
        public PictureBox pic1 = new PictureBox();
        /// <summary>
        /// 画设备的图片
        /// </summary>
        public PictureBox pic2 = new PictureBox();
        /// <summary>
        /// 是否是播种墙小车 true 是 false 否
        /// </summary>
        private bool isSeedingWallShuttle = false;
        #endregion 
        public string deviceId;
        public string deviceName;
        /// <summary>
        /// 目的设备编号
        /// </summary>
        public string toDeviceId;
        #region Socket

        /// <summary>
        /// 自动命令线程
        /// </summary>
        private Thread autoCmdThread;

        /// <summary>
        /// Socket客户端
        /// </summary>
        public static Socket socketClient { get; set; }
        /// <summary>
        /// 重连定时器
        /// </summary>
        public static System.Timers.Timer reconnectTimer;
        /// <summary>
        /// 心跳定时器
        /// </summary>
        public static System.Timers.Timer heartbeatTimer;
        #endregion

        #region 字典变量
        /// <summary>
        /// 设备状态字典
        /// </summary>
        public Dictionary<string, DeviceStatus> deviceStatusDic = new Dictionary<string, DeviceStatus>();
        public Dictionary<string, string> seedingWallDic = new Dictionary<string, string>();

        /// <summary>
        /// 读取json绑定的返回DB
        /// 读取json绑定的写入DB
        /// </summary>
        public Dictionary<string, SeedingWallRead> s7PlcReadDic = new Dictionary<string, SeedingWallRead>();
        public Dictionary<string, SeedingWallWrite> s7PlcWriteDic = new Dictionary<string, SeedingWallWrite>();
        #endregion

        public bool closing = false;

        protected UtilsResult _UtilsResult;

        protected Point point;

        /// <summary>
        /// SH状态结构体对象
        /// </summary>
        public s7ReadValue s7PlcReadValue;
        /// <summary>
        /// SH命令结构体对象
        /// </summary>
        public s7WriteValue s7PlcWriteValue;

        int horizontalSpeed = 7;//水平速度

        int verticalSpeed = 3;//垂直速度
        /// <summary>
        /// 定时刷新小车移动
        /// </summary>
        public static System.Timers.Timer shuttleTimer;
        /// <summary>
        /// 定义PLC连接
        /// </summary>
        public SiemensS7 simensS7;

        public long timesTamp;

        #region 构造函数
        /// <summary>
        /// 初始化货格
        /// </summary>
        /// <param name="mainFrm">主窗体</param>
        /// <param name="row">一行设备信息</param>
        public SeedingWallLocation(FormMain mainFrm, table_rcs_base_device row, SiemensS7 s7Plc)
            : base()
        {
            InitSeedingWall(mainFrm, row, s7Plc);
        }

        private void InitSeedingWall(FormMain mainFrm, table_rcs_base_device row, SiemensS7 s7Plc)
        {
            frmMain = mainFrm;
            simensS7 = s7Plc;

            deviceId = row.DEVICE_ID;
            deviceName = row.DEVICE_NAME;
            deviceType = row.DEVICE_TYPE;
            commandDB = row.COMMAND_DB;
            //命令字段存在IP 地址 我们可以将其认为是播种墙小车
            isSeedingWallShuttle = commandDB?.Length > 0 ? true : false;

            x = row.X;
            y = row.Y;
            width = row.WIDTH;
            height = row.HEIGHT;
            kqId = row.AREA_NO;
            isAutoReq = row.IS_AUTO.ToString();
            scannerNo = row.SCANNER_NO;
            Bitmap bitmap2;

            // 添加图片双击事件
            if (!isSeedingWallShuttle)
            {
                if (int.Parse(deviceId.Substring(deviceId.Length - 1)) % 2 == 1)
                    bitmap2 = new Bitmap(Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wall11.PNG"), new Size(width, height));
                else
                    bitmap2 = new Bitmap(Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wall22.PNG"), new Size(width, height));

                pic1.Location = new Point(x, y);
                pic1.BackColor = Color.Transparent;
                pic1.SizeMode = PictureBoxSizeMode.StretchImage;
                pic1.Image = bitmap2;
                pic1.Size = pic1.Image.Size;

                #region Label描绘
                /*pic.Location = new Point(x, y);
                pic.Size = new Size(width, height);
                pic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
                pic.TextAlign = ContentAlignment.MiddleCenter;
                pic.Font = new System.Drawing.Font("微软雅黑", 8);
                pic.Name = deviceName;
                pic.Text = deviceId.Substring(3);
                pic.ForeColor = Color.White;*/
                #endregion
                //不是播种墙小车 那么就是注册悬浮和离开事件
                pic1.MouseHover += new System.EventHandler(Pic_MouseHoverClick);
                pic1.MouseLeave += new System.EventHandler(Pic_MouseLeaveClick);
                //pic1.DoubleClick += new System.EventHandler(this.Pic_DoubleClick);
            }
            else
            {
                point = pic2.Location;
                #region Picture 描绘
                Bitmap bitmap = new Bitmap(Image.FromFile(Application.StartupPath + @"\SeedingWallImage\SeedingWallShuttle.PNG"), new Size(width, height));
                Graphics g = Graphics.FromImage(bitmap);
                g.DrawString(deviceId, new Font("宋体", 9), Brushes.Black, new PointF(4, 2));
                pic2.Location = new Point(x, y);
                pic2.BackColor = Color.Transparent;
                pic2.SizeMode = PictureBoxSizeMode.StretchImage;
                pic2.Image = bitmap;
                #endregion
                //是 则注册双击事件  切开始刷新线程开启
                pic2.DoubleClick += new System.EventHandler(this.Pic_DoubleClick);

                //将Json中配置好的DB 读取出来进行读取使用
                readPlcJson();
                //将读取的变量初始化值
                InitReadStruct();
                //注册连接小车timer事件
                //ConnectSeedingWallShuttle();

                //启动线程刷新
                autoCmdThread = new Thread(new ThreadStart(AutoSeedingWallShuttle));
                autoCmdThread.IsBackground = true; //后台运行线程
                if (!autoCmdThread.IsAlive)
                    autoCmdThread.Start();

                shuttleTimer = new System.Timers.Timer();
                shuttleTimer.Interval = 1000;
                shuttleTimer.Elapsed += new System.Timers.ElapsedEventHandler(shuttleMoveTimer);
                shuttleTimer.Start();
            }

            InitDics();
            //BindPLC();
        }
        #endregion

        #region 注册Time事件  进行Socket连接
        private void ConnectSeedingWallShuttle()
        {
            //5000ms重连一次
            reconnectTimer = new System.Timers.Timer();
            reconnectTimer.Interval = 5000;
            reconnectTimer.Elapsed += new System.Timers.ElapsedEventHandler(timeIsUp);

            heartbeatTimer = new System.Timers.Timer();
            heartbeatTimer.Interval = 60000;
            heartbeatTimer.Elapsed += new System.Timers.ElapsedEventHandler(heartbeatTimerIsUp);
            ConneceToServer();
        }

        #endregion


        #region 自动刷新播种墙小车方法
        private void AutoSeedingWallShuttle()
        {
            //循环
            while (!closing)
            {
                //判断系统是否已经初始化成功
                if (frmMain.deviceInited)
                {
                    //如果刷新状态时 返回false说明和PLC 断开连接 直接返回
                    if (RefreshStatus())
                    {
                        RefreshDisplay();
                        //从PLC 获取到的心跳值 如果是True  则写入0 False 否则写入True
                        //var heartBeat = s7PlcReadValue.heartBeat == "0" ? "False" : "True";
                        if (s7PlcReadValue.heartBeat == "0")
                            s7PlcWriteValue.heartBeat = "True";
                        else
                            s7PlcWriteValue.heartBeat = "False";
                        if (!s7PlcWriteDic.ContainsKey(deviceId))
                        {
                            //判断是否存在当前写入字典  不存在返回
                            return;
                        }
                        //得到当前设备的写入DB 
                        var s7PlcWrite = s7PlcWriteDic[deviceId];
                        var writeResult = simensS7.WriteVar(s7PlcWrite.heartBeat, VarType.Bit, s7PlcWriteValue.heartBeat);
                        if (!writeResult)
                        {
                            //写入失败 返回结果
                            return;
                        }
                        //判断设备是否正常  是否自动   任务可接受新任务状态  联机模式后加
                        if (s7PlcReadValue.deviceStatus == "0" && s7PlcReadValue.workMode == "0" && s7PlcReadValue.taskStatus == "10")
                        {
                            //找到播种墙小车任务进行小车任务写入
                            //Task();
                            List<S7Comm.SiemensItemVar> SiemensItemVar = new List<S7Comm.SiemensItemVar>
                        {
                            new S7Comm.SiemensItemVar(s7PlcWrite.taskId,S7Comm. VarType.Short,"123"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.taskType,S7Comm. VarType.Short,"3"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.startRow,S7Comm. VarType.Short,"1"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.startBay,S7Comm. VarType.Short,"1"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.startLevel,S7Comm. VarType.Short,"1"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.endRow,S7Comm. VarType.Short,"2"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.endBay,S7Comm. VarType.Short,"2"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.endLevel,S7Comm. VarType.Short,"2"),

                           /* new S7Comm.SiemensItemVar(s7PlcWrite.barCode,S7Comm. VarType.Byte,"123456789"),
                            new S7Comm.SiemensItemVar(s7PlcWrite.barCode,S7Comm. VarType.Byte,"123456789"),*/
                           //new S7Comm.SiemensItemVar();
                        };
                            byte[] barCode = BitConverter.GetBytes(123456789);
                            Array.Reverse(barCode);

                            List<S7Comm.SiemensItemGroup> siemensItemGroup = new List<S7Comm.SiemensItemGroup>
                        {
                            new S7Comm.SiemensItemGroup( S7Comm.MemoryArea.DataBlock,2,24,barCode),
                            //这个是可以读多个地址的 
                        };

                            var writePlcBarcoe = simensS7.WriteVar(siemensItemGroup);

                            //判断 是否写入成功 不成功则提示错误
                            var writePlc = simensS7.WriteVar(SiemensItemVar);
                            if (!writePlc)
                            {
                                return;
                            }
                            //将任务报完成
                            //上报上游系统  调用接口


                        }
                    }
                }
                Thread.Sleep(300);
            }
            #region socket通讯 暂时屏蔽改用S7协议
            /*try
            {
                //注册连接心跳事件
                //注册连接服务事件
                ConnectSeedingWallShuttle();
                //开始循环
                while (!closing)
                {
                    //判断链是否已经正常
                    if (socketClient != null && socketClient.Connected)
                    {
                        //接受数据
                        byte[] resByteReceive = new byte[1024];
                        int resInt = socketClient.Receive(resByteReceive);
                        if (resInt > 0)
                        {
                            Console.WriteLine(Encoding.Default.GetString(resByteReceive, 0, resInt));
                        }
                        //发送数据
                        string res = Console.ReadLine();
                        byte[] resByteSend = Encoding.Default.GetBytes(res);
                        socketClient.Send(resByteSend);
                    }
                }

            }
            catch (SocketException sex)
            {
                if (sex.SocketErrorCode == SocketError.ConnectionRefused)
                {
                    Console.WriteLine("连接到服务端失败！5s后开始重连！");
                    reconnectTimer.Start();
                    heartbeatTimer.Stop();
                }
                if (sex.SocketErrorCode == SocketError.ConnectionReset)
                {
                    Console.WriteLine("服务端断开！5s后重连！");
                    reconnectTimer.Start();
                    heartbeatTimer.Stop();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("程序出现异常:" + ex);
                heartbeatTimer.Stop();
            }*/
            #endregion
        }
        #endregion

        #region  Socket 连接处理
        /// <summary>
        /// 心跳定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void heartbeatTimerIsUp(object sender, ElapsedEventArgs e)
        {
            //状态为连接则发送当前时间,否则停止心跳定时器
            if (socketClient.Connected)
            {
                byte[] resByte = Encoding.Default.GetBytes(DateTime.Now.ToShortDateString());
                socketClient.Send(resByte);
            }
            else
            {
                heartbeatTimer.Stop();
                reconnectTimer.Start();
            }
        }

        /// <summary>
        /// 重连定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timeIsUp(object sender, EventArgs e)
        {
            reconnectTimer.Stop();
            ConneceToServer();
        }

        /// <summary>
        /// 连接到服务端
        /// </summary>
        private void ConneceToServer()
        {
            // 新建客户端实例，并连接到服务端所在的端口号
            socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socketClient.Connect("127.0.0.1", 20000);
            Console.WriteLine("成功连接到服务端");
            heartbeatTimer.Start();
        }

        /// <summary>
        /// 接收消息
        /// </summary>
        private static void thdRevMethod()
        {
            try
            {
                while (true)
                {
                    byte[] resByte = new byte[1024];
                    int resInt = socketClient.Receive(resByte);
                    if (resInt > 0)
                    {
                        Console.WriteLine(Encoding.Default.GetString(resByte, 0, resInt));
                    }
                }

            }
            catch (SocketException sex)
            {
                if (sex.SocketErrorCode == SocketError.ConnectionReset)
                {
                    Console.WriteLine("服务端断开！5s后重连！");
                    reconnectTimer.Start();
                    heartbeatTimer.Stop();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("程序出现异常:" + ex);
                heartbeatTimer.Stop();
            }
        }

        //发送信息
        private static void thdSendMethod()
        {
            try
            {
                while (true)
                {
                    string res = Console.ReadLine();
                    byte[] resByte = Encoding.Default.GetBytes(res);
                    socketClient.Send(resByte);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("程序出现异常:" + ex);
                heartbeatTimer.Stop();
            }
        }
    #endregion


        #region 图片事件
    /// <summary>
    /// 图片的双击事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void Pic_DoubleClick(object sender, EventArgs e)
        {
            FormSeedingWallShuttle fsw = new FormSeedingWallShuttle(this);
            fsw.ShowDialog();
            //确定图片位置和大小用
            /*FrmCtrLocAndSize frmCtrLocAndSize = new FrmCtrLocAndSize(pic1, deviceId);
            frmCtrLocAndSize.ShowDialog();*/
        }
        GroupPanelExt groupPanel = new GroupPanelExt();
        /// <summary>
        /// 鼠标悬浮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Pic_MouseHoverClick(object sender, EventArgs e)
        {
            //SeedingWallArea SeedingWallArea
            if (frmMain.tabControlExt1.SelectedTab != null && frmMain.tabControlExt1.SelectedTab.Created && frmMain.tabControlExt1.SelectedTab.Tag.Equals(kqId))
            {
                frmMain.tabControlExt1.SelectedTab.Controls.Remove(groupPanel);
                groupPanel.Title = deviceName;
                groupPanel.Location = new Point(x, y + 61);
                groupPanel.Size = new Size(width * 2, height * 2);
                groupPanel.TitleBackColor = Color.FromArgb(255, 216, 216, 216);
                groupPanel.BorderColor = Color.FromArgb(255, 216, 216, 216);
                frmMain.tabControlExt1.SelectedTab.Controls.Add(groupPanel);
                groupPanel.BringToFront();

                #region 获取标签下的内容信息
                var tb_node_masters = sqlSugarClient.Queryable<table_rcs_node_master>().Where(r => r.LOCATION_NO == deviceId).ToList();
                var i = 0;
                foreach (var tb_master in tb_node_masters)
                {
                    foreach (System.Reflection.PropertyInfo p in tb_master.GetType().GetProperties())
                    {
                        if (seedingWallDic.Keys.Contains(p.Name))
                        {
                            TextBox tb = new TextBox();
                            tb.BorderStyle = BorderStyle.None;
                            tb.Text = seedingWallDic[p.Name] + p.GetValue(tb_master);
                            tb.Location = new Point(0, 5 + i * 20);
                            groupPanel.Controls.Add(tb);
                            i++;
                        }
                        var a = p.Name;
                        var b = p.GetValue(tb_master);
                    }
                }
                #endregion
            }
        }
        /// <summary>
        /// 鼠标离开 消失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Pic_MouseLeaveClick(object sender, EventArgs e)
        {
            if (frmMain.tabControlExt1.SelectedTab != null && frmMain.tabControlExt1.SelectedTab.Created && frmMain.tabControlExt1.SelectedTab.Tag.Equals(kqId))
            {
                frmMain.tabControlExt1.SelectedTab.Controls.Remove(groupPanel);
            }
        }

        #endregion
        /// <summary>
        /// 手动命令
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        /// <returns></returns>
        public bool ManulCommand(string cmdId)
        {
            return true;
        }
        /// <summary>
        /// 手动命令
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        /// <returns></returns>
        public bool ManulCommandTask(string cmdId)
        {
            return false;
        }

        /// <summary>
        /// 手动命令写入，并添加命令列表
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        public void ManualCommandWrite(string cmdId)
        {
            if (!simensS7.WriteVar(s7PlcWriteDic[deviceId].workMode, VarType.Short, cmdId))
            {
                MessageBox.Show(s7PlcWriteDic[deviceId].workMode + ",写入失败！值:" + cmdId);
                return;
            }
        }


        /// <summary>
        /// 控制命令写入，并添加命令列表
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        public void ControlCommandWrite(string cmdId)
        {
            if (!simensS7.WriteVar(s7PlcWriteDic[deviceId].controlMode, VarType.Short, cmdId))
            {
                MessageBox.Show(s7PlcWriteDic[deviceId].controlMode + ",写入失败！值:" + cmdId);
                return;
            }
        }

        /// <summary>
        /// 命令设备刷新状态
        /// </summary>
        /// <returns></returns>
        public bool RefreshStatus()
        {
            //判断字典里面是否存在该设备，存在进行读取操作
            if (s7PlcReadDic.ContainsKey(deviceId))
            {
                //得到s7读取的DB进行读取处理
                var s7PlcRead = s7PlcReadDic[deviceId];

                //var abc = simensS7.WriteVar(MemoryArea.DataBlock, "YF123456789", S7Comm.VarType.Short,0);

                //var dfghj = simensS7.WriteVar(MemoryArea.DataBlock, 2, 24, "YF123456", VarType.Char, 0);


                byte[] s7PlcReadByte = (byte[])simensS7.ReadVar(S7Comm.MemoryArea.DataBlock, 2, 0, 66, S7Comm.VarType.Byte, 0);
                //判断读取的字节数组是否为空
                if (s7PlcReadByte != null && s7PlcReadByte?.Length > 0)
                {
                    //心跳
                    s7PlcReadValue.heartBeat = string.Concat(s7PlcReadByte[0], s7PlcReadByte[1]) == "10" ? "1" :"0";

                    //工作模式
                    s7PlcReadValue.workMode = string.Concat(s7PlcReadByte[2], s7PlcReadByte[3]).TrimStart('0');

                    //设备状态
                    s7PlcReadValue.deviceStatus = string.Concat(s7PlcReadByte[4], s7PlcReadByte[5]).TrimStart('0');

                    //装载状态 有货1  无货0 
                    s7PlcReadValue.loadGoods = string.Concat(s7PlcReadByte[6], s7PlcReadByte[7]).TrimStart('0');

                    //任务号
                    s7PlcReadValue.taskId = string.Concat(s7PlcReadByte[8], s7PlcReadByte[9], s7PlcReadByte[10], s7PlcReadByte[11]).TrimStart('0');

                    //任务类型
                    s7PlcReadValue.taskType = string.Concat(s7PlcReadByte[12], s7PlcReadByte[13]).TrimStart('0');

                    //起始排
                    s7PlcReadValue.startRow = string.Concat(s7PlcReadByte[14], s7PlcReadByte[15]).TrimStart('0');

                    //起始列
                    s7PlcReadValue.startBay = string.Concat(s7PlcReadByte[16], s7PlcReadByte[17]).TrimStart('0');

                    //起始层
                    s7PlcReadValue.startLevel = string.Concat(s7PlcReadByte[18], s7PlcReadByte[19]).TrimStart('0');

                    //目的排
                    s7PlcReadValue.endRow = string.Concat(s7PlcReadByte[20], s7PlcReadByte[21]).TrimStart('0');

                    //目的列
                    s7PlcReadValue.endBay = string.Concat(s7PlcReadByte[22], s7PlcReadByte[23]).TrimStart('0');

                    //目的层
                    s7PlcReadValue.endLevel = string.Concat(s7PlcReadByte[24], s7PlcReadByte[25]).TrimStart('0');
                    //for循环 定义好的位置就是索引26开始 读取20个字节
                    byte[] buff = new byte[20];
                    int index = 0;
                    for (var i = 26; i < s7PlcReadByte.Length - 20; i++)
                    {
                        buff[index] = s7PlcReadByte[i];
                        ++index;
                    }
                    //条码
                    s7PlcReadValue.barCode = Encoding.Default.GetString(buff).Replace(" ", "").Replace("\0", "");

                    //任务状态 10为可下任务，1为执行中
                    s7PlcReadValue.taskStatus = string.Concat(s7PlcReadByte[46], s7PlcReadByte[47]).TrimStart('0');

                    //当前排
                    s7PlcReadValue.currentRow = string.Concat(s7PlcReadByte[48], s7PlcReadByte[49]).TrimStart('0');

                    //当前列
                    s7PlcReadValue.currentBay = string.Concat(s7PlcReadByte[50], s7PlcReadByte[51]).TrimStart('0');

                    //当前层
                    s7PlcReadValue.currenLevel = string.Concat(s7PlcReadByte[52], s7PlcReadByte[53]).TrimStart('0');

                    //当前位置
                    s7PlcReadValue.position = string.Concat(s7PlcReadByte[54], s7PlcReadByte[55]).TrimStart('0');

                    //当前步序
                    s7PlcReadValue.currentStep = string.Concat(s7PlcReadByte[56], s7PlcReadByte[57]) == "00" ? "0" : string.Concat(s7PlcReadByte[56], s7PlcReadByte[57]).TrimStart('0');

                    //电池剩余电量
                    s7PlcReadValue.socBat = string.Concat(s7PlcReadByte[58], s7PlcReadByte[59]).TrimStart('0');

                    //电池充电状态 1充电 2放电
                    s7PlcReadValue.chargingBat = string.Concat(s7PlcReadByte[60], s7PlcReadByte[61]).TrimStart('0');

                    //电容剩余电量
                    s7PlcReadValue.socCap = string.Concat(s7PlcReadByte[62], s7PlcReadByte[63]).TrimStart('0');

                    //电容充电状态 1充电 2放电
                    s7PlcReadValue.chargingCap = string.Concat(s7PlcReadByte[64], s7PlcReadByte[65]).TrimStart('0');
                }

                return true;
                #region 单个读取暂时不使用
                /*try
                {
                    var abc = simensS7.ReadVar(s7PlcRead.taskId, S7Comm.VarType.Short);
                    //任务号
                    s7PlcReadValue.taskId = simensS7.ReadVar(s7PlcRead.taskId, S7Comm.VarType.Short)?.ToString();
                    //任务类型
                    s7PlcReadValue.taskType = simensS7.ReadVar(s7PlcRead.taskType, S7Comm.VarType.Short)?.ToString();
                    //起始排
                    s7PlcReadValue.startRow = simensS7.ReadVar(s7PlcRead.startRow, S7Comm.VarType.Short)?.ToString();
                    //起始列
                    s7PlcReadValue.startBay = simensS7.ReadVar(s7PlcRead.startBay, S7Comm.VarType.Short)?.ToString();
                    //起始层
                    s7PlcReadValue.startLevel = simensS7.ReadVar(s7PlcRead.startLevel, S7Comm.VarType.Short)?.ToString();
                    //目的排
                    s7PlcReadValue.endRow = simensS7.ReadVar(s7PlcRead.endRow, S7Comm.VarType.Short)?.ToString();
                    //目的列
                    s7PlcReadValue.endBay = simensS7.ReadVar(s7PlcRead.endBay, S7Comm.VarType.Short)?.ToString();
                    //目的层
                    s7PlcReadValue.endLevel = simensS7.ReadVar(s7PlcRead.endLevel, S7Comm.VarType.Short)?.ToString();
                    //心跳
                    s7PlcReadValue.heartBeat = simensS7.ReadVar(s7PlcRead.heartBeat, S7Comm.VarType.Short)?.ToString();
                    //工作模式
                    s7PlcReadValue.workMode = simensS7.ReadVar(s7PlcRead.workMode, S7Comm.VarType.Short)?.ToString();
                    //设备状态
                    s7PlcReadValue.deviceStatus = simensS7.ReadVar(s7PlcRead.deviceStatus, S7Comm.VarType.Short)?.ToString();
                    //装载状态 有货1  无货0 
                    s7PlcReadValue.loadGoods = simensS7.ReadVar(s7PlcRead.loadGoods, S7Comm.VarType.Short)?.ToString();

                    s7PlcReadValue.barCode = simensS7.ReadVar(S7Comm.MemoryArea.DataBlock, 2, 24, 20, S7Comm.VarType.Byte, 0)?.ToString();
                    //任务状态 10为可下任务，1为执行中
                    s7PlcReadValue.taskStatus = simensS7.ReadVar(s7PlcRead.taskStatus, S7Comm.VarType.Short)?.ToString();
                    //当前排
                    s7PlcReadValue.currentRow = simensS7.ReadVar(s7PlcRead.currentRow, S7Comm.VarType.Short)?.ToString();
                    //当前列
                    s7PlcReadValue.currentBay = simensS7.ReadVar(s7PlcRead.currentBay, S7Comm.VarType.Short)?.ToString();
                    //当前层
                    s7PlcReadValue.currenLevel = simensS7.ReadVar(s7PlcRead.currenLevel, S7Comm.VarType.Short)?.ToString();
                    //当前步序
                    s7PlcReadValue.currentStep = simensS7.ReadVar(s7PlcRead.currentStep, S7Comm.VarType.Short)?.ToString();
                    //当前位置
                    s7PlcReadValue.position = simensS7.ReadVar(s7PlcRead.position, S7Comm.VarType.Short)?.ToString();
                    //电池剩余电量
                    s7PlcReadValue.socBat = simensS7.ReadVar(s7PlcRead.socBat, S7Comm.VarType.Short)?.ToString();
                    //电容剩余电量
                    s7PlcReadValue.socCap = simensS7.ReadVar(s7PlcRead.socCap, S7Comm.VarType.Short)?.ToString();
                    //电池充电状态 1充电 2放电
                    s7PlcReadValue.chargingBat = simensS7.ReadVar(s7PlcRead.chargingBat, S7Comm.VarType.Short)?.ToString();
                    //电容充电状态 1充电 2放电
                    s7PlcReadValue.chargingCap = simensS7.ReadVar(s7PlcRead.chargingCap, S7Comm.VarType.Short)?.ToString();
                }
                catch (Exception ex)
                {
                    
                    //throw ex;
                    return false;
                }*/
                #endregion
            }
            return false;
        }

        /// <summary>
        /// 刷新图片显示
        /// </summary>
        public void RefreshDisplay()
        {
            //读取格口的状态
            //状态DB  读取
            //pic1.BackColor = Color.Red;


         /*   var imageFile = Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wallError2.PNG");

            *//* pic1.Location = new Point(x, y);
             pic1.BackColor = Color.Transparent;*//*
            pic1.Size = new Size(width, height);
            pic1.SizeMode = PictureBoxSizeMode.StretchImage;
            pic1.Image = imageFile;
            pic1.Size = pic1.Image.Size;*/
        }


        #region  刷新小车移动距离
        /// <summary>
        /// 小车移动定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shuttleMoveTimer(object sender, ElapsedEventArgs e)
        {
            //① 当前坐标 X<300 Y>150 说明已经到达上升位置
            if (pic2.Left <= 300 && pic2.Top > 75)
            {
                pic2.Top = pic2.Top - 50;
            }
            //②当前坐标X>300 Y>150  说明在底层可以往前移动
            if (pic2.Left > 250 && pic2.Top == 375)
            {
                pic2.Left = pic2.Left - 50;
            }
            //③当前坐标X>=300 Y<=150
            if (pic2.Left >= 300 && pic2.Top <= 75)
            {
                pic2.Left = pic2.Left + 50;
            }
            //④当前坐标 300<X<1000 Y>=30
            if (pic2.Left ==1150 && pic2.Top >= 75)
            {
                pic2.Top = pic2.Top + 50;
            }

            //pic2.Location = new Point((int)point.X, (int)point.Y);


            /* point.X += horizontalSpeed;

             point.Y += verticalSpeed;*/

            /* if (pic2.Left < 0) { horizontalSpeed = -horizontalSpeed; }

             if (pic2.Right > frmMain.Width) { horizontalSpeed = -horizontalSpeed; }

             if (pic2.Top < 0) { verticalSpeed = -verticalSpeed; }

             if (pic2.Bottom > frmMain.Height) { verticalSpeed = -verticalSpeed; }*/
        }
        #endregion

        #region 初始化字典
        /// <summary>
        /// 初始化字典
        /// </summary>
        private void InitDics()
        {
            seedingWallDic.Add("UNIT_STATUS", "货位状态：");
            seedingWallDic.Add("IS_FREEZE", "货位冻结：");
            seedingWallDic.Add("USE_STATUS", "货位使用状态：");
        }
        #endregion

        #region 读取Json里面的DB 方法
        private void readPlcJson()
        {
            using (var readJson = new StreamReader(@".\SeedingWallDBJson\SeedingWallRead.json"))
            {
                var read = readJson.ReadToEnd();
                var seedingWallReadList = JsonConvert.DeserializeObject<List<SeedingWallRead>>(read);
                if (seedingWallReadList.Any())
                {
                    foreach (var seedingWallRead in seedingWallReadList)
                    {
                        SeedingWallRead swr = new SeedingWallRead()
                        {
                            shuttleId = seedingWallRead.shuttleId,
                            heartBeat = seedingWallRead.heartBeat,
                            workMode = seedingWallRead.workMode,
                            deviceStatus = seedingWallRead.deviceStatus,
                            loadGoods = seedingWallRead.loadGoods,
                            taskId = seedingWallRead.taskId,
                            taskType = seedingWallRead.taskType,
                            startRow = seedingWallRead.startRow,
                            startBay = seedingWallRead.startBay,
                            startLevel = seedingWallRead.startLevel,
                            endRow = seedingWallRead.endRow,
                            endBay = seedingWallRead.endBay,
                            endLevel = seedingWallRead.endLevel,
                            barCode = seedingWallRead.barCode,
                            taskStatus = seedingWallRead.taskStatus,
                            currentRow = seedingWallRead.currentRow,
                            currentBay = seedingWallRead.currentBay,
                            currenLevel = seedingWallRead.currenLevel,
                            position = seedingWallRead.position,
                            currentStep = seedingWallRead.currentStep,
                            socBat = seedingWallRead.socBat,
                            chargingBat = seedingWallRead.chargingBat,
                            socCap = seedingWallRead.socCap,
                            chargingCap = seedingWallRead.chargingCap
                        };
                        //循环添加到字典中来
                        s7PlcReadDic.Add(swr.shuttleId, swr);
                    }
                }
            }

            using (var readJson = new StreamReader(@".\SeedingWallDBJson\SeedingWallWrite.json"))
            {
                var read = readJson.ReadToEnd();
                var seedingWallReadList = JsonConvert.DeserializeObject<List<SeedingWallWrite>>(read);
                if (seedingWallReadList.Any())
                {
                    foreach (var seedingWallRead in seedingWallReadList)
                    {
                        SeedingWallWrite swr = new SeedingWallWrite()
                        {
                            shuttleId = seedingWallRead.shuttleId,
                            heartBeat = seedingWallRead.heartBeat,
                            workMode = seedingWallRead.workMode,
                            controlMode = seedingWallRead.controlMode,
                            taskId = seedingWallRead.taskId,
                            taskType = seedingWallRead.taskType,
                            startRow = seedingWallRead.startRow,
                            startBay = seedingWallRead.startBay,
                            startLevel = seedingWallRead.startLevel,
                            endRow = seedingWallRead.endRow,
                            endBay = seedingWallRead.endBay,
                            endLevel = seedingWallRead.endLevel,
                            barCode = seedingWallRead.barCode,
                        };
                        //循环添加到字典中来
                        s7PlcWriteDic.Add(swr.shuttleId, swr);
                    }
                }
            }
        }
        #endregion

        #region 设备属性变量
        /// <summary>
        /// PLC数据块,命令
        /// </summary>
        public string commandDB;
        /// <summary>
        /// PLC数据块,返回
        /// </summary>
        public string returnDB;
        /// <summary>
        /// PLC数据块,控制
        /// </summary>
        public string controlDB;
        /// <summary>
        /// PLC数据块,状态
        /// </summary>
        public string statusDB;
        /// <summary>
        /// PLC数据块,装载
        /// </summary>
        public string loadDB;
        /// <summary>
        /// 设备类型
        /// </summary>
        public string deviceType;
        /// <summary>
        /// 设备相关类型，命令
        /// </summary>
        public string commandType;
        /// <summary>
        /// 设备相关类型，控制
        /// </summary>
        public string controlType;
        /// <summary>
        /// 设备相关类型，状态
        /// </summary>
        public string statusType;
        /// <summary>
        /// 设备相关类型，装载状态
        /// </summary>
        public string loadType;
        /// <summary>
        /// 设备的位置和尺寸
        /// </summary>
        public Int32 x, y, width, height;
        /// <summary>
        /// 所属库区
        /// </summary>
        public string kqId;
        /// <summary>
        /// 是否自动申请任务 1自动，0不自动
        /// </summary>
        public string isAutoReq;
        /// <summary>
        /// 条码扫描器号
        /// </summary>
        public string scannerNo;
        /// <summary>
        /// 是否启用核对条码1核对，0不核对
        /// </summary>
        public bool isCheckBarcode;
        #endregion
        #region 播种墙小车读取信息
        public class SeedingWallRead
        {
            //小车编号
            public string shuttleId { get; set; }
            //心跳
            public string heartBeat { get; set; }
            //工作模式
            public string workMode { get; set; }
            //设备状态
            public string deviceStatus { get; set; }
            //有无货
            public string loadGoods { get; set; }
            //任务号
            public string taskId { get; set; }
            //任务类型
            public string taskType { get; set; }
            //起始排
            public string startRow { get; set; }
            //起始列
            public string startBay { get; set; }
            //起始层
            public string startLevel { get; set; }
            //目的排
            public string endRow { get; set; }
            //目的列
            public string endBay { get; set; }
            //目的层
            public string endLevel { get; set; }
            //条码
            public string barCode { get; set; }
            //任务状态 10为可下任务，1为执行中
            public string taskStatus { get; set; }
            //当前排
            public string currentRow { get; set; }
            //当前列
            public string currentBay { get; set; }
            //当前层
            public string currenLevel { get; set; }
            //当前位置
            public string position { get; set; }
            //当前步序
            public string currentStep { get; set; }
            //电池剩余电量
            public string socBat { get; set; }
            //当前电池剩余状态 1.充电 2.放电
            public string chargingBat { get; set; }
            //电容剩余电量
            public string socCap { get; set; }
            //当前电容剩余状态 1.充电  2.放电
            public string chargingCap { get; set; }
        }

        public struct s7ReadValue
        {
            //小车编号
            public string shuttleId { get; set; }
            //心跳
            public string heartBeat { get; set; }
            //工作模式
            public string workMode { get; set; }
            //设备状态
            public string deviceStatus { get; set; }
            //有无货
            public string loadGoods { get; set; }
            //任务号
            public string taskId { get; set; }
            //任务类型
            public string taskType { get; set; }
            //起始排
            public string startRow { get; set; }
            //起始列
            public string startBay { get; set; }
            //起始层
            public string startLevel { get; set; }
            //目的排
            public string endRow { get; set; }
            //目的列
            public string endBay { get; set; }
            //目的层
            public string endLevel { get; set; }
            //条码
            public string barCode { get; set; }
            //任务状态 10为可下任务，1为执行中
            public string taskStatus { get; set; }
            //当前排
            public string currentRow { get; set; }
            //当前列
            public string currentBay { get; set; }
            //当前层
            public string currenLevel { get; set; }
            //当前位置
            public string position { get; set; }
            //当前步序
            public string currentStep { get; set; }
            //电池剩余电量
            public string socBat { get; set; }
            //当前电池剩余状态 1.充电 2.放电
            public string chargingBat { get; set; }
            //电容剩余电量
            public string socCap { get; set; }
            //当前电容剩余状态 1.充电  2.放电
            public string chargingCap { get; set; }
        }

        #endregion
        #region 播种墙小车写入信息信息
        public class SeedingWallWrite
        {
            //小车编号
            public string shuttleId { get; set; }
            //心跳
            public string heartBeat { get; set; }
            //工作模式
            public string workMode { get; set; }
            //控制模式
            public string controlMode { get; set; }
            //任务号
            public string taskId { get; set; }
            //任务类型
            public string taskType { get; set; }
            //起始排
            public string startRow { get; set; }
            //起始列
            public string startBay { get; set; }
            //起始层
            public string startLevel { get; set; }
            //目的排
            public string endRow { get; set; }
            //目的列
            public string endBay { get; set; }
            //目的层
            public string endLevel { get; set; }
            //条码
            public string barCode { get; set; }
        }

        public struct s7WriteValue
        {
            //小车编号
            public string shuttleId { get; set; }
            //心跳
            public string heartBeat { get; set; }
            //工作模式
            public string workMode { get; set; }
            //控制模式
            public string controlMode { get; set; }
            //任务号
            public string taskId { get; set; }
            //任务类型
            public string taskType { get; set; }
            //起始排
            public string startRow { get; set; }
            //起始列
            public string startBay { get; set; }
            //起始层
            public string startLevel { get; set; }
            //目的排
            public string endRow { get; set; }
            //目的列
            public string endBay { get; set; }
            //目的层
            public string endLevel { get; set; }
            //条码
            public string barCode { get; set; }
        }

        #endregion

        #region 初始化读取结构体
        public void InitReadStruct()
        {
            #region 读取值
            //心跳
            s7PlcReadValue.heartBeat = "0";
            //工作模式
            s7PlcReadValue.workMode = "0";
            //设备状态
            s7PlcReadValue.deviceStatus = "0";
            //有无货
            s7PlcReadValue.loadGoods = "0";
            //任务号
            s7PlcReadValue.taskId ="1";
            //任务类型
            s7PlcReadValue.taskType = "1";
            //起始排
            s7PlcReadValue.startRow = "0";
            //起始列
            s7PlcReadValue.startBay = "0";
            //起始层
            s7PlcReadValue.startLevel = "0";
            //目的排
            s7PlcReadValue.endRow = "0";
            //目的列
            s7PlcReadValue.endBay = "0";
            //目的层
            s7PlcReadValue.endLevel = "0";
            //条码
            s7PlcReadValue.barCode = "123456789";
            //任务状态 10为可下任务，1为执行中
            s7PlcReadValue.taskStatus = "0";
            //当前排
            s7PlcReadValue.currentRow = "0";
            //当前列
            s7PlcReadValue.currentBay = "0";
            //当前层
            s7PlcReadValue.currenLevel = "0";
            //当前位置
            s7PlcReadValue.position = "0";
            s7PlcReadValue.currentStep = "0";
            //电池剩余电量
            s7PlcReadValue.socBat = "10";
            //当前电池剩余状态 1.充电 2.放电
            s7PlcReadValue.chargingBat = "1";
            //电容剩余电量
            s7PlcReadValue.socCap = "10";
            //当前电容剩余状态 1.充电  2.放电
            s7PlcReadValue.chargingCap = "1";
            #endregion


            #region 写入值
            //心跳
            s7PlcWriteValue.heartBeat = "0";
            //工作模式
            s7PlcWriteValue.workMode = "0";
            //任务号
            s7PlcWriteValue.taskId = "1";
            //任务类型
            s7PlcWriteValue.taskType = "1";
            //起始排
            s7PlcWriteValue.startRow = "0";
            //起始列
            s7PlcWriteValue.startBay = "0";
            //起始层
            s7PlcWriteValue.startLevel = "0";
            //目的排
            s7PlcWriteValue.endRow = "0";
            //目的列
            s7PlcWriteValue.endBay = "0";
            //目的层
            s7PlcWriteValue.endLevel = "0";
            //条码
            s7PlcWriteValue.barCode = "123456789";
            #endregion
        }

        #endregion

    }
}
