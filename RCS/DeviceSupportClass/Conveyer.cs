﻿using Kengic.RCS.Common;
using Kengic.RCS.Models.Table;
using S7.Net;
using S7Comm;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RCS.DeviceSupportClass
{
    public class Conveyer
    {
        #region 自定义变量
        /// <summary>
        /// 主窗体
        /// </summary>
        public FormMain frmMain;
        /// <summary>
        /// PLC连接
        /// </summary>
        public Plc plc;
        public SiemensS7 siemensS7;
        /// <summary>
        /// 上次任务申请返回信息
        /// </summary>
        private string lastReqTaskRet = string.Empty;
        /// <summary>
        /// 上次任务处理情况
        /// </summary>
        private string lastTaskPress = string.Empty;
        /// <summary>
        /// 上次装载状态
        /// </summary>
        private string lastStatusLoadStatus = "2";
        /// <summary>
        /// 上次装载状态
        /// </summary>
        //private string lastStatusLoadStatus2 = "3";
        /// <summary>
        /// 上次设备状态
        /// </summary>
        private string lastDeviceStatus = string.Empty;
        /// <summary>
        /// 上次读状态信息的状态
        /// </summary>
        private string lastReadStatusRet = string.Empty;
        /// <summary>
        /// 上次读装载信息的状态
        /// </summary>
        private string lastReadLoadRet = string.Empty;
        /// <summary>
        /// 上次读返回信息的状态
        /// </summary>
        private string lastReadReturnRet = string.Empty;
        /// <summary>
        /// 上次绑定PLC状态
        /// </summary>
        private string lastBindPlc = string.Empty;
        /// <summary>
        /// 上次刷新状态异常
        /// </summary>
        private string lastRefreshStatusEx = string.Empty;
        /// <summary>
        /// 画设备的图片
        /// </summary>
        public Label pic = new Label();
        /// <summary>
        /// 是否是命令设备 true命令设备 false 非命令设备
        /// </summary>
        private bool isCmdDevice = false;
        /// <summary>
        /// 手动任务的任务号1...200
        /// </summary>
        public int manualTaskId = 1;
        /// <summary>
        /// 数据块对应的句柄
        /// </summary>
        private int[] commandHandle = new int[1];
        private int[] returnHandle = new int[1];
        private int[] controlHandle = new int[1];
        private int[] lockHandle = new int[1];
        private int[] statusHandle = new int[1];
        private int[] loadHandle = new int[1];
        private string[] writeValues = new string[1];
        private int statusClientHandle;
        private int loadClientHandle;
        #endregion 
        /// <summary>
        /// 设备编号
        /// </summary>
        public string deviceId;
        // <summary>
        /// 设备名称
        /// </summary>
        public string deviceName;
        #region 任务相关变量
        #region 任务返回信息变量
        /// <summary>
        /// 返回任务状态 [0]
        /// 10为可写任务状态，1为任务执行中，2为错误任务数据
        /// </summary>
        public string returnTaskStatus;
        /// <summary>
        /// 返回任务类型 [1]
        /// 1入库，2出库，10 出入库任务初始化
        /// </summary>
        public string returnTaskType;
        /// <summary>
        /// 条码
        /// </summary>
        public string returnBarcode;
        /// <summary>
        /// 返回任务号 [4] 通常为子任务号
        /// </summary>
        public string returnTaskId;

        /// <summary>
        /// 返回有无货
        /// </summary>
        public string returnLoads;
        #endregion

        #region 任务指令变量
        /// <summary>
        /// 任务类型 [1]
        /// 1入库，2出库，10出入库
        /// </summary>
        public string taskType;
        /// <summary>
        /// 目的地址 [2] 设备号(数字部分)
        /// </summary>
        public int toAddr;
        /// <summary>
        /// 起始地址 [3] 设备号(数字部分)
        /// </summary>
        public int fromAddr;
        /// <summary>
        /// 任务号  [4] 为route_task_seq
        /// </summary>
        public string taskId;
        #endregion
        /// <summary>
        /// 自动命令线程
        /// </summary>
        private Thread autoCmdThread;
        /// <summary>
        /// 是否已绑定PLC
        /// </summary>
        public bool isBindPLC = false;
        #endregion

        #region 状态变量
        /// <summary>
        /// PLC返回的设备状态DB101,状态代码，"0"为正常状态，其他非正常
        /// </summary>
        public string deviceStatus = "-1";
        //Load状态DB103
        /// <summary>
        /// 装载状态 [0] 0无货 1有货 2空托盘
        /// </summary>
        public string statusLoadStatus = "0";
        /// <summary>
        /// 任务类型 [1] 0任务 1入库 2出库 10出入库
        /// </summary>
        public string statusLoadTaskType;
        /// <summary>
        /// 任务ID   [4] 和returnTaskId相同
        /// </summary>
        public string statusLoadTaskId;
        /// <summary>
        /// 备用
        /// </summary>
        public string statusExample;
        /// <summary>
        /// 重量
        /// </summary>
        public string statusWeight = "0";
        /// <summary>
        /// 装载路向号
        /// </summary>
        public string statusTaskDirection;

        #endregion
        #region 字典变量
        /// <summary>
        /// 设备状态字典
        /// </summary>
        public Dictionary<string, DeviceStatus> deviceStatusDic = new Dictionary<string, DeviceStatus>();
        #endregion
        //private LogFile logFile;
        string routeTaskId = string.Empty;
        string TaskDirection = string.Empty;
        string weight = string.Empty;
        public bool closing = false;
        /// <summary>
        /// 上一个返回的任务号
        /// </summary>
        private string lastReturnTaskId = "-1";

        protected UtilsResult _UtilsResult;
        /// <summary>
        /// 是否链接PLC成功标志
        /// </summary>
        private bool isPlcConnect = false;
        #region 构造函数
        /// <summary>
        /// 初始化密集备货设备
        /// </summary>
        /// <param name="mainFrm">主窗体</param>
        /// <param name="row">一行设备信息</param>
        public Conveyer(FormMain mainFrm, table_rcs_base_device row, SiemensS7 s7Plc)
            : base()
        {
            InitConveryer(mainFrm, row, s7Plc);
        }

        private void InitConveryer(FormMain mainFrm, table_rcs_base_device row, SiemensS7 s7Plc)
        {
            frmMain = mainFrm;
            siemensS7 = s7Plc;

            deviceId = row.DEVICE_ID;
            deviceName = row.DEVICE_NAME;
            commandDB = row.COMMAND_DB;
            returnDB = row.RETURN_DB;
            controlDB = row.CONTROL_DB;
            statusDB = row.STATUS_DB;
            loadDB = row.LOAD_DB;

            deviceType = row.DEVICE_TYPE;
            commandType = row.COMMAND_DB;
            isCmdDevice = (commandType.Length > 0) ? true : false; //该设备是否为命令相关设备
            /* controlType = row.CONTROL_DB;
             statusType = row.STATUS_DB;
             loadType = row.LOAD_DB;*/
            x = row.X;
            y = row.Y;
            width = row.WIDTH;
            height = row.HEIGHT;
            kqId = row.AREA_NO;
            isAutoReq = row.IS_AUTO.ToString();
            scannerNo = row.SCANNER_NO;
            /*if (row["is_check_barcode"].Tostring() == "1")
                isCheckBarcode = true;
            else
                isCheckBarcode = false;*/

            pic.Location = new Point(x, y);
            //pic.Size = new Size(width, height);
            //pic.BackColor = Color.LimeGreen;
            //pic.Name = deviceName;
            pic.Size = new Size(width, height);
            pic.BackColor = Color.LimeGreen;  //  SlateBlue   SteelBlue   SkyBlue
            pic.BackColor = Color.DarkSlateBlue;
            pic.TextAlign = ContentAlignment.MiddleCenter;
            pic.Font = new System.Drawing.Font("微软雅黑", 8);
            pic.Name = deviceName;
            pic.Text = deviceId.Substring(3);
            pic.ForeColor = Color.White;
            // 添加图片双击事件
            pic.DoubleClick += new System.EventHandler(this.Pic_DoubleClick);
            InitDics();
            if (isCmdDevice)
            {
                //logFile = new LogFile(deviceId);
                autoCmdThread = new Thread(new ThreadStart(AutoCmd));
                autoCmdThread.IsBackground = true;
                if (!autoCmdThread.IsAlive)
                    autoCmdThread.Start();
            }
        }
        #endregion

        private void AutoCmd()
        {
            while (!closing)
            {
                /*try
                {
                    //判断plc是否为空  且 标志联结字段是否为true
                    if (plc != null && this.isPlcConnect)
                    {
                        plc.Close();
                        this.isPlcConnect = false;
                    }
                    //实例化 PLC  打开
                    plc = new Plc(CpuType.S71200, frmMain.plcIp, 0, 1);
                    plc.Open();
                }
                catch (PlcException ex)
                {
                    plc.Close();
                    throw;
                }
                this.isPlcConnect = true;*/
                //刷新设备状态
                RefreshStatus();
                Thread.Sleep(300);
            }
        }

        #region 保存输送设备当前状态
        public string lastCurrStatus = "   ";
        public void SaveCurrStatus()
        {
            string currStatus = "";
            if (deviceStatusDic.ContainsKey(deviceStatus))
            {
                if (deviceStatusDic[deviceStatus].statusKind != "0")
                {
                    currStatus = deviceStatusDic[deviceStatus].statusDesc;
                }
            }
            else
            {
                currStatus = "未定义状态" + deviceStatus;
            }
            if (currStatus != lastCurrStatus) //保存输送设备的当前状态
            {
                //ConveyerDbIntf.SaveConvCurrStatus(deviceId, currStatus);
                lastCurrStatus = currStatus;
            }
        }
        #endregion

        /// <summary>
        /// 图片的双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Pic_DoubleClick(object sender, EventArgs e)
        {
            //确定图片位置和大小用
            //FrmCtrLocAndSize frmCtrLocAndSize = new FrmCtrLocAndSize(frmMain.dbConn, pic, deviceId);
            //frmCtrLocAndSize.ShowDialog();
            ////测试用，弹出该设备ID+设备名称，正式使用去掉
            // MessageBox.Show(this.deviceId + this.deviceName);
            //FrmConveyer frmConveyer = new FrmConveyer(this);
            //frmConveyer.ShowDialog();
        }

        /// <summary>
        /// 手动命令
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        /// <returns></returns>
        public bool ManulCommand(string cmdId)
        {
            if (isCmdDevice)
            {
                if (!RefreshStatus())
                {
                    MessageBox.Show(deviceId + "刷新设备状态失败!");
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 手动命令
        /// </summary>
        /// <param name="cmdId">命令ID</param>
        /// <returns></returns>
        public bool ManulCommandTask(string cmdId)
        {
            if (isCmdDevice)
            {
                if (!RefreshStatus())
                {
                    MessageBox.Show(deviceId + "刷新设备状态失败!");
                    return false;
                }
            }
            else
            {
                MessageBox.Show(deviceId + "不是命令设备!");
                return false;
            }
            /*if (!frmMain.plcKQs[kqId].RefreshStatus())
            {
                MessageBox.Show(deviceId + "刷新库区状态失败!");
                return false;
            }*/
            /*if (frmMain.plcKQs[kqId].runStatus &&
                 frmMain.plcKQs[kqId].autoStatus &&
                 frmMain.workMode == 0)
            {
                if (returnTaskStatus == "10")
                {
                    string[] writeValues = new string[1];
                    writeValues[0] = cmdId;
                    SyncWrite(writeValues, commandHandle);
                    frmMain.logHandling.OperationEvent(deviceId, "手工任务 " + writeValues[0], "1");
                    return true;
                }
                else
                {
                    MessageBox.Show(deviceId + "输送机不在可写命令状态！");
                    return false;
                }
            }
            else
            {
                MessageBox.Show("不满足指令条件，所在库区必须工作在自动、脱机模式！");
                return false;
            }*/
            return false;
        }


        /// <summary>
        /// 命令设备刷新状态
        /// </summary>
        /// <returns></returns>
        public bool RefreshStatus()
        {
            try
            {
                string[] readValues = new string[1];
                #region 任务返回信息读取
                if (returnDB.Length > 2)  //处理返回信息
                {
                    var test10 = siemensS7.ReadVar(S7Comm.MemoryArea.DataBlock, 67,0, 5, S7Comm.VarType.Int, 0);

                    var test1 = siemensS7.ReadVar("DB67.DBD0.5", S7Comm.VarType.Int);

                    SiemensItemVar[] siemensItemVars = new SiemensItemVar[] {
                        new SiemensItemVar{
                        varAddress = "DB67.DBD0",
                        varType = S7Comm.VarType.Int
                        }
                    };
                    var test2 = siemensS7.ReadVar(siemensItemVars);

                    /* var test2 = plc.Read("DB67.DBD0.5");
                     var dWords = plc.Read(DataType.DataBlock, 67, 0, VarType.String, 5);
                     returnStruct rs = new returnStruct()
                     {
                         ID = 1,
                         From =3,
                         To = 2,
                         Reserve1 = 4,
                         Ready = 5
                     };
                   plc.WriteStruct(rs, 67);

                     returnStruct readTest = (returnStruct)plc.ReadStruct(typeof(returnStruct), 67);
                     var taskId = readTest.ID;
                     var routeNo = readTest.From;
                     var simple1 = readTest.To;
                     var simple2 = readTest.Reserve1;
                     var barcode = readTest.Ready;*/

                    // plc.Close();
                }
                #endregion
            }
            catch (PlcException ex)
            {
                plc.Close();
                this.isPlcConnect = false;
                return false;
            }
            return true;
        }

        /// <summary>
        /// 刷新图片显示
        /// </summary>
        public void RefreshDisplay()
        {
            #region 刷新设备的显示
            try
            {
                /*if (frmMain.runMode == "T")
                {
                    SaveCurrStatus();
                }*/
                if (statusDB.Length > 2 && deviceStatus != lastDeviceStatus
                    || loadDB.Length >= 2 && statusLoadStatus != lastStatusLoadStatus)
                {
                    if (deviceStatusDic.ContainsKey(deviceStatus))
                    {
                        if (deviceStatusDic[deviceStatus].statusKind == "0")
                        {
                            //显示
                            if (loadDB.Length >= 2 && statusLoadStatus != "0")

                                pic.BackColor = Color.Green;
                            else
                                pic.BackColor = Color.LimeGreen;
                        }
                        else
                        {
                            if (loadDB.Length >= 2 && statusLoadStatus == "0") //既有错误信息，又有装载信息的情况
                                pic.BackColor = Color.Magenta;
                            else
                                if (deviceType == "SS" && deviceStatus == "9")
                                pic.BackColor = Color.Green;
                            else
                                pic.BackColor = Color.Red;
                        }
                    }
                    else
                        pic.BackColor = Color.Red;
                    if (deviceStatus != lastDeviceStatus)
                    {
                        if (deviceStatusDic.ContainsKey(deviceStatus))
                        {
                            //frmMain.logHandling.WarningEvent(deviceId, statusType, deviceStatus,
                            //deviceStatusDic[deviceStatus].statusDesc, deviceStatusDic[deviceStatus].statusKind);
                        }
                        else
                        {
                            //frmMain.logHandling.WarningEvent(deviceId, statusType, deviceStatus, "未定义状态" + deviceStatus, "5");
                        }
                    }
                    lastDeviceStatus = deviceStatus;
                }
            }
            catch (Exception ex)
            {
                string estr = ex.Message.ToString();
                //frmMain.logHandling.ProgramEvent(deviceId + "RefreshDisplay", "程序异常" + estr, "1");
            }
            #endregion
        }



        #region 初始化字典
        /// <summary>
        /// 初始化字典
        /// </summary>
        private void InitDics()
        {

            /* DataSet ds = MainDbIntf.GetDic("status_id [key],status_desc,status_kind", "td_wcs_device_status_dic", " and status_type = '" + statusType + "' ", "status_type,status_id");
             DeviceStatus tmpDeviceStatus;
             foreach (DataRow row in ds.Tables[0].Rows)
             {
                 tmpDeviceStatus.statusDesc = row["status_desc"].Tostring();
                 tmpDeviceStatus.statusKind = row["status_kind"].Tostring();
                 deviceStatusDic.Add(row["key"].Tostring(), tmpDeviceStatus);
             }*/
        }
        #endregion

        #region struct
        /// <summary>
        /// 命令struct
        /// </summary>
        public struct commandStruct
        {
            public Int32 ID;
            public Int32 From;
            public Int32 To;
            public Int32 Reserve1;
            public Int32 Ready;
        }
        /// <summary>
        /// 返回struct
        /// </summary>
        public struct returnStruct
        {
            public Int32 ID;
            public Int32 From;
            public Int32 To;
            public Int32 Reserve1;
            public Int32 Ready;
        }
        #endregion


        #region 设备属性变量
        /// <summary>
        /// PLC数据块,命令
        /// </summary>
        public string commandDB;
        /// <summary>
        /// PLC数据块,返回
        /// </summary>
        public string returnDB;
        /// <summary>
        /// PLC数据块,控制
        /// </summary>
        public string controlDB;
        /// <summary>
        /// PLC数据块,状态
        /// </summary>
        public string statusDB;
        /// <summary>
        /// PLC数据块,装载
        /// </summary>
        public string loadDB;
        /// <summary>
        /// 设备类型
        /// </summary>
        public string deviceType;
        /// <summary>
        /// 设备相关类型，命令
        /// </summary>
        public string commandType;
        /// <summary>
        /// 设备相关类型，控制
        /// </summary>
        public string controlType;
        /// <summary>
        /// 设备相关类型，状态
        /// </summary>
        public string statusType;
        /// <summary>
        /// 设备相关类型，装载状态
        /// </summary>
        public string loadType;
        /// <summary>
        /// 设备的位置和尺寸
        /// </summary>
        public Int32 x, y, width, height;
        /// <summary>
        /// 所属库区
        /// </summary>
        public string kqId;
        /// <summary>
        /// 是否自动申请任务 1自动，0不自动
        /// </summary>
        public string isAutoReq;
        /// <summary>
        /// 条码扫描器号
        /// </summary>
        public string scannerNo;
        /// <summary>
        /// 是否启用核对条码1核对，0不核对
        /// </summary>
        public bool isCheckBarcode;
        #endregion 
        
    }
}
