﻿
namespace RCS
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panl_top = new System.Windows.Forms.Panel();
            this.btn_exit_system = new WinformControlLibraryExtension.ButtonExt();
            this.btn_set_system = new WinformControlLibraryExtension.ButtonExt();
            this.btn_start_system = new WinformControlLibraryExtension.ButtonExt();
            this.panl_main = new System.Windows.Forms.Panel();
            this.tabControlExt1 = new WinformControlLibraryExtension.TabControlExt();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panl_bottom1 = new System.Windows.Forms.Panel();
            this.panl_bottom2 = new System.Windows.Forms.Panel();
            this.panl_bottom3 = new System.Windows.Forms.Panel();
            this.statusStripExt1 = new WinformControlLibraryExtension.StatusStripExt();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panl_top.SuspendLayout();
            this.panl_main.SuspendLayout();
            this.statusStripExt1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panl_top
            // 
            this.panl_top.BackColor = System.Drawing.Color.White;
            this.panl_top.Controls.Add(this.btn_exit_system);
            this.panl_top.Controls.Add(this.btn_set_system);
            this.panl_top.Controls.Add(this.btn_start_system);
            this.panl_top.Font = new System.Drawing.Font("宋体", 9F);
            this.panl_top.Location = new System.Drawing.Point(6, 1);
            this.panl_top.Name = "panl_top";
            this.panl_top.Size = new System.Drawing.Size(1220, 53);
            this.panl_top.TabIndex = 0;
            this.panl_top.Paint += new System.Windows.Forms.PaintEventHandler(this.panl_top_Paint);
            // 
            // btn_exit_system
            // 
            this.btn_exit_system.Animation.Options.AllTransformTime = 250D;
            this.btn_exit_system.Animation.Options.Data = null;
            this.btn_exit_system.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.btn_exit_system.FlatAppearance.BorderSize = 0;
            this.btn_exit_system.Location = new System.Drawing.Point(184, 8);
            this.btn_exit_system.Name = "btn_exit_system";
            this.btn_exit_system.Size = new System.Drawing.Size(66, 35);
            this.btn_exit_system.TabIndex = 2;
            this.btn_exit_system.Text = "退出系统";
            this.btn_exit_system.Click += new System.EventHandler(this.btn_exit_system_Click);
            // 
            // btn_set_system
            // 
            this.btn_set_system.Animation.Options.AllTransformTime = 250D;
            this.btn_set_system.Animation.Options.Data = null;
            this.btn_set_system.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.btn_set_system.FlatAppearance.BorderSize = 0;
            this.btn_set_system.Location = new System.Drawing.Point(97, 8);
            this.btn_set_system.Name = "btn_set_system";
            this.btn_set_system.Size = new System.Drawing.Size(66, 35);
            this.btn_set_system.TabIndex = 1;
            this.btn_set_system.Text = "设置";
            this.btn_set_system.Click += new System.EventHandler(this.btn_set_system_Click);
            // 
            // btn_start_system
            // 
            this.btn_start_system.Animation.Options.AllTransformTime = 250D;
            this.btn_start_system.Animation.Options.Data = null;
            this.btn_start_system.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.btn_start_system.FlatAppearance.BorderSize = 0;
            this.btn_start_system.Location = new System.Drawing.Point(11, 8);
            this.btn_start_system.Name = "btn_start_system";
            this.btn_start_system.Size = new System.Drawing.Size(66, 35);
            this.btn_start_system.TabIndex = 0;
            this.btn_start_system.Text = "启动系统";
            this.btn_start_system.Click += new System.EventHandler(this.btn_start_system_Click);
            // 
            // panl_main
            // 
            this.panl_main.BackColor = System.Drawing.Color.White;
            this.panl_main.Controls.Add(this.tabControlExt1);
            this.panl_main.Location = new System.Drawing.Point(6, 60);
            this.panl_main.Name = "panl_main";
            this.panl_main.Size = new System.Drawing.Size(1220, 554);
            this.panl_main.TabIndex = 1;
            // 
            // tabControlExt1
            // 
            this.tabControlExt1.Font = new System.Drawing.Font("宋体", 9F);
            this.tabControlExt1.ImageList = this.imageList1;
            this.tabControlExt1.Location = new System.Drawing.Point(7, 11);
            this.tabControlExt1.Name = "tabControlExt1";
            this.tabControlExt1.SelectedIndex = 0;
            this.tabControlExt1.Size = new System.Drawing.Size(1210, 540);
            this.tabControlExt1.TabBackNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(168)))), ((int)(((byte)(255)))));
            this.tabControlExt1.TabBackSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(131)))), ((int)(((byte)(229)))));
            this.tabControlExt1.TabCloseBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlExt1.TabImageSize = new System.Drawing.Size(20, 20);
            this.tabControlExt1.TabIndex = 0;
            this.tabControlExt1.TabPageBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(168)))), ((int)(((byte)(255)))));
            this.tabControlExt1.TabRadiusLeftTop = 10;
            this.tabControlExt1.TabRadiusRightTop = 10;
            this.tabControlExt1.TabTextNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlExt1.TabTextSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabControlExt1.Tag = "";
            this.tabControlExt1.SelectedIndexChanged += new System.EventHandler(this.tabControlExt1_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "tabControl.png");
            // 
            // panl_bottom1
            // 
            this.panl_bottom1.BackColor = System.Drawing.Color.White;
            this.panl_bottom1.Location = new System.Drawing.Point(8, 621);
            this.panl_bottom1.Name = "panl_bottom1";
            this.panl_bottom1.Size = new System.Drawing.Size(379, 232);
            this.panl_bottom1.TabIndex = 2;
            this.panl_bottom1.Visible = false;
            // 
            // panl_bottom2
            // 
            this.panl_bottom2.BackColor = System.Drawing.Color.White;
            this.panl_bottom2.Location = new System.Drawing.Point(415, 621);
            this.panl_bottom2.Name = "panl_bottom2";
            this.panl_bottom2.Size = new System.Drawing.Size(395, 232);
            this.panl_bottom2.TabIndex = 3;
            this.panl_bottom2.Visible = false;
            // 
            // panl_bottom3
            // 
            this.panl_bottom3.BackColor = System.Drawing.Color.White;
            this.panl_bottom3.Location = new System.Drawing.Point(837, 621);
            this.panl_bottom3.Name = "panl_bottom3";
            this.panl_bottom3.Size = new System.Drawing.Size(386, 232);
            this.panl_bottom3.TabIndex = 4;
            this.panl_bottom3.Visible = false;
            // 
            // statusStripExt1
            // 
            this.statusStripExt1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.statusStripExt1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStripExt1.Location = new System.Drawing.Point(0, 823);
            this.statusStripExt1.Name = "statusStripExt1";
            this.statusStripExt1.Size = new System.Drawing.Size(1231, 22);
            this.statusStripExt1.TabIndex = 5;
            this.statusStripExt1.Text = "statusStripExt1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.ForeColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.ForeColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(0, 17);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 845);
            this.Controls.Add(this.statusStripExt1);
            this.Controls.Add(this.panl_bottom3);
            this.Controls.Add(this.panl_bottom2);
            this.Controls.Add(this.panl_bottom1);
            this.Controls.Add(this.panl_main);
            this.Controls.Add(this.panl_top);
            this.Font = new System.Drawing.Font("宋体", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RCS-RailShuttleControlSystem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panl_top.ResumeLayout(false);
            this.panl_main.ResumeLayout(false);
            this.statusStripExt1.ResumeLayout(false);
            this.statusStripExt1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panl_top;
        private System.Windows.Forms.Panel panl_main;
        public System.Windows.Forms.Panel panl_bottom1;
        public System.Windows.Forms.Panel panl_bottom3;
        public System.Windows.Forms.Panel panl_bottom2;
        private System.Windows.Forms.ImageList imageList1;
        public WinformControlLibraryExtension.TabControlExt tabControlExt1;
        private WinformControlLibraryExtension.ButtonExt btn_exit_system;
        private WinformControlLibraryExtension.ButtonExt btn_set_system;
        private WinformControlLibraryExtension.ButtonExt btn_start_system;
        private WinformControlLibraryExtension.StatusStripExt statusStripExt1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
    }
}

