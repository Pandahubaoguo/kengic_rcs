﻿
namespace RCS.DeviceForm
{
    partial class FormAreaSystemSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAreaSystemSet));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_area_name_box = new System.Windows.Forms.TextBox();
            this.tb_area_box = new System.Windows.Forms.TextBox();
            this.btn_insert_area = new WinformControlLibraryExtension.ButtonExt();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.sbe_show_status = new WinformControlLibraryExtension.SwitchButtonExt();
            this.cob_area_no = new System.Windows.Forms.ComboBox();
            this.statusStripExt1 = new WinformControlLibraryExtension.StatusStripExt();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStripExt1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.panel1.Controls.Add(this.tb_area_name_box);
            this.panel1.Controls.Add(this.tb_area_box);
            this.panel1.Controls.Add(this.btn_insert_area);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(666, 193);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // tb_area_name_box
            // 
            this.tb_area_name_box.Font = new System.Drawing.Font("宋体", 14F);
            this.tb_area_name_box.Location = new System.Drawing.Point(227, 98);
            this.tb_area_name_box.Name = "tb_area_name_box";
            this.tb_area_name_box.Size = new System.Drawing.Size(175, 29);
            this.tb_area_name_box.TabIndex = 6;
            // 
            // tb_area_box
            // 
            this.tb_area_box.Font = new System.Drawing.Font("宋体", 14F);
            this.tb_area_box.Location = new System.Drawing.Point(227, 61);
            this.tb_area_box.Name = "tb_area_box";
            this.tb_area_box.Size = new System.Drawing.Size(175, 29);
            this.tb_area_box.TabIndex = 5;
            this.tb_area_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_area_box_KeyPress);
            // 
            // btn_insert_area
            // 
            this.btn_insert_area.Animation.Options.AllTransformTime = 250D;
            this.btn_insert_area.Animation.Options.Data = null;
            this.btn_insert_area.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.btn_insert_area.FlatAppearance.BorderSize = 0;
            this.btn_insert_area.Font = new System.Drawing.Font("宋体", 14F);
            this.btn_insert_area.Location = new System.Drawing.Point(430, 98);
            this.btn_insert_area.Name = "btn_insert_area";
            this.btn_insert_area.Size = new System.Drawing.Size(87, 40);
            this.btn_insert_area.TabIndex = 4;
            this.btn_insert_area.Text = "添加";
            this.btn_insert_area.Click += new System.EventHandler(this.btn_insert_area_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 14F);
            this.label4.Location = new System.Drawing.Point(13, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "添加库区";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 14F);
            this.label2.Location = new System.Drawing.Point(98, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "库区名称：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14F);
            this.label1.Location = new System.Drawing.Point(117, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "库区号：";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.sbe_show_status);
            this.panel2.Controls.Add(this.cob_area_no);
            this.panel2.Location = new System.Drawing.Point(4, 202);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(665, 239);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 14F);
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "展示库区";
            // 
            // sbe_show_status
            // 
            this.sbe_show_status.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(168)))), ((int)(((byte)(255)))));
            this.sbe_show_status.CheckedSlideBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.DisableBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sbe_show_status.DisableSlideBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.DisableSlideBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.DisableTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.sbe_show_status.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.sbe_show_status.Location = new System.Drawing.Point(430, 82);
            this.sbe_show_status.Name = "sbe_show_status";
            this.sbe_show_status.Size = new System.Drawing.Size(90, 30);
            this.sbe_show_status.TabIndex = 1;
            this.sbe_show_status.UnCheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(204)))), ((int)(((byte)(233)))));
            this.sbe_show_status.UnCheckedBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(204)))), ((int)(((byte)(233)))));
            this.sbe_show_status.UnCheckedSlideBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.UnCheckedSlideBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.UnCheckedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.sbe_show_status.Click += new System.EventHandler(this.sbe_show_status_Click);
            // 
            // cob_area_no
            // 
            this.cob_area_no.Font = new System.Drawing.Font("宋体", 14F);
            this.cob_area_no.FormattingEnabled = true;
            this.cob_area_no.Location = new System.Drawing.Point(194, 83);
            this.cob_area_no.Name = "cob_area_no";
            this.cob_area_no.Size = new System.Drawing.Size(198, 27);
            this.cob_area_no.TabIndex = 0;
            this.cob_area_no.SelectedIndexChanged += new System.EventHandler(this.cob_area_no_SelectedIndexChanged);
            // 
            // statusStripExt1
            // 
            this.statusStripExt1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F);
            this.statusStripExt1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.statusStripExt1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStripExt1.Location = new System.Drawing.Point(0, 446);
            this.statusStripExt1.Name = "statusStripExt1";
            this.statusStripExt1.Size = new System.Drawing.Size(671, 22);
            this.statusStripExt1.TabIndex = 2;
            this.statusStripExt1.Text = "statusStripExt1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // FormAreaSystemSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 468);
            this.Controls.Add(this.statusStripExt1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormAreaSystemSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAreaSystemSet";
            this.Load += new System.EventHandler(this.FormAreaSystemSet_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStripExt1.ResumeLayout(false);
            this.statusStripExt1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb_area_name_box;
        private System.Windows.Forms.TextBox tb_area_box;
        private WinformControlLibraryExtension.ButtonExt btn_insert_area;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cob_area_no;
        private WinformControlLibraryExtension.SwitchButtonExt sbe_show_status;
        private System.Windows.Forms.Label label3;
        private WinformControlLibraryExtension.StatusStripExt statusStripExt1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}