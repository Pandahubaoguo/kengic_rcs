﻿using RCS.DeviceSupportClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace RCS.DeviceForm
{
    public partial class FormSeedingWallShuttle : Form
    {

        #region 变量定义
        /// <summary>
        /// 定义播种墙 对象
        /// </summary>
        private SeedingWallLocation device;
        /// <summary>
        /// Timer刷新 播种墙小车状态
        /// </summary>
        public static System.Timers.Timer timer;
        /// <summary>
        /// 测试使用  将速度动起来
        /// </summary>
        List<int> intlist6 = new List<int>() { 60, 30, 35, 80, 45, 30, 70, 85 };
        int index = 0;
        #endregion

        public FormSeedingWallShuttle(SeedingWallLocation seedingWallLocation)
        {
            InitializeComponent();
            this.device = seedingWallLocation;
        }
        #region 加载Load
        private void FormSeedingWallShuttle_Load(object sender, EventArgs e)
        {
            #region 设备名字追加
            this.Text += device.deviceName;
            #endregion

            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(refreshSeedingWallStatus);
            timer.Start();

        }
        #endregion

        #region 手自动命令
        private void but_manual_cmd_Click(object sender, EventArgs e)
        {
            var mannualCommand = "";
            if (but_manual_cmd.Text == "自动")
            {
                mannualCommand = "00";
            }
            else
            {
                mannualCommand = "01";
            }
            device.ManualCommandWrite(mannualCommand);
            device.frmMain.InsertLvEvent(device.kqId,"系统：手自动处理指令;");
        }
        #endregion

        #region 清错指令
        private void but_clear_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("01");
            device.frmMain.InsertLvEvent(device.kqId,"系统：清错处理指令;");
        }
        #endregion


        #region 专家指令
        private void btn_expert_cmd_Click(object sender, EventArgs e)
        {
            device.ManualCommandWrite("02");
        }
        #endregion

        #region 小车前进指令
        private void btn_forward_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("11");
        }
        #endregion

        #region 小车后退指令
        private void btn_retreat_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("12");
        }
        #endregion

        #region 皮带正转指令
        private void btn_forwardFRotation_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("13");
        }
        #endregion

        #region 皮带反转指令
        private void btn_reversal_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("14");
        }
        #endregion

        #region 初始化命令
        private void but_inital_cmd_Click(object sender, EventArgs e)
        {
            device.ControlCommandWrite("02");
            device.frmMain.InsertLvEvent(device.kqId,"系统：初始化处理指令;");
        }
        private void but_close_cmd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 刷新穿梭车状态

        private void refreshSeedingWallStatus(object sender, ElapsedEventArgs e)
        {
            #region 小车返回状态数据
            //心跳
            lbl_st_heartBeat.Text = device.s7PlcReadValue.heartBeat ;

            //工作模式
            lbl_st_workMode.Text = device.s7PlcReadValue.workMode == "1" ? "自动" : "手动";
            //工作模式按钮处理一下
            but_manual_cmd.Text = device.s7PlcReadValue.workMode == "1" ? "手动" : "自动";


            //任务号
            lbl_st_taskId.Text = device.s7PlcReadValue.taskId;

            //设备状态
            lbl_st_deviceStatus.Text = device.s7PlcReadValue.deviceStatus;

            //任务状态
            lbl_st_taskStatus.Text = device.s7PlcReadValue.taskStatus;

            //载货状态
            lbl_st_loadGoods.Text = device.s7PlcReadValue.loadGoods;

            //任务类型
            lbl_st_taskType.Text = device.s7PlcReadValue.taskType;

            //起始排
            lbl_st_startRow.Text = device.s7PlcReadValue.startRow;

            //起始列
            lbl_st_startBay.Text = device.s7PlcReadValue.startBay;

            //起始层
            lbl_st_startLevel.Text = device.s7PlcReadValue.startLevel;

            //目的排
            lbl_st_endRow.Text = device.s7PlcReadValue.endRow;

            //目的列
            lbl_st_endBay.Text = device.s7PlcReadValue.endBay;

            //目的层
            lbl_st_endLevel.Text = device.s7PlcReadValue.endLevel;

            //当前排
            lbl_st_currentRow.Text = device.s7PlcReadValue.currentRow;

            //当前列
            lbl_st_currentBay.Text = device.s7PlcReadValue.currentBay;

            //当前层
            lbl_st_currentLevel.Text = device.s7PlcReadValue.currenLevel;

            //条码
            lbl_st_barCode.Text = device.s7PlcReadValue.barCode;

            //当前位置
            lbl_st_position.Text = device.s7PlcReadValue.position;

            //当前步序
            lbl_st_setUp.Text = device.s7PlcReadValue.currentStep;

            //剩余电量(电池)
            lbl_st_socBat.Text = device.s7PlcReadValue.socBat;

            //剩余电量(电容)
            lbl_st_socCap.Text = device.s7PlcReadValue.socCap;

            //充电状态(电池)
            lbl_charging_bat.Text = device.s7PlcReadValue.chargingBat;

            //充电状态(电容)
            lbl_charging_cap.Text = device.s7PlcReadValue.chargingCap;

            //速度
            ++index;
            if (index >= intlist6.Count)
                index = 0;
            meter_speed.Value = intlist6[index];
            #endregion

            #region 写入小车命令数据
            //心跳
            lab_command_heart.Text = device.s7PlcWriteValue.heartBeat == "True" ? "1" : "0";

            //任务号
            lab_command_taskId.Text = device.s7PlcWriteValue.taskId;

            //任务类型
            lab_command_taskType.Text = device.s7PlcWriteValue.taskType;

            //起始排
            lab_command_startRow.Text = device.s7PlcWriteValue.startRow;

            //起始列
            lab_command_startBay.Text = device.s7PlcWriteValue.startBay;

            //起始层
            lab_command_startLevel.Text = device.s7PlcWriteValue.startLevel;

            //目的排
            lab_command_endRow.Text = device.s7PlcWriteValue.endRow;

            //目的列
            lab_command_endBay.Text = device.s7PlcWriteValue.endBay;

            //目的层
            lab_command_endLevel.Text = device.s7PlcWriteValue.endLevel;

            //条码
            lab_command_barCode.Text = device.s7PlcWriteValue.barCode;


            #endregion

        }
        #endregion

        #region 关闭后停止timer
        private void FormSeedingWallShuttle_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer.Close();
        }
        #endregion
    }
}
