﻿
namespace RCS.DeviceForm
{
    partial class FormSeedingWallShuttle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSeedingWallShuttle));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lab_command_barCode = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lab_command_endLevel = new System.Windows.Forms.Label();
            this.lab_command_endBay = new System.Windows.Forms.Label();
            this.lab_command_endRow = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lab_command_startLevel = new System.Windows.Forms.Label();
            this.lab_command_startBay = new System.Windows.Forms.Label();
            this.lab_command_startRow = new System.Windows.Forms.Label();
            this.lab_command_taskType = new System.Windows.Forms.Label();
            this.lab_command_taskId = new System.Windows.Forms.Label();
            this.lab_command_heart = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_charging_cap = new System.Windows.Forms.Label();
            this.lbl_st_socCap = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.lbl_st_socBat = new System.Windows.Forms.Label();
            this.lbl_st_setUp = new System.Windows.Forms.Label();
            this.lbl_st_position = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.lbl_charging_bat = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.lbl_st_currentLevel = new System.Windows.Forms.Label();
            this.lbl_st_currentBay = new System.Windows.Forms.Label();
            this.lbl_st_currentRow = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.lbl_st_taskStatus = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.lbl_st_barCode = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lbl_st_taskType = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.lbl_st_workMode = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lbl_st_deviceStatus = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbl_st_endLevel = new System.Windows.Forms.Label();
            this.lbl_st_endBay = new System.Windows.Forms.Label();
            this.lbl_st_endRow = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbl_st_startLevel = new System.Windows.Forms.Label();
            this.lbl_st_startBay = new System.Windows.Forms.Label();
            this.lbl_st_startRow = new System.Windows.Forms.Label();
            this.lbl_st_loadGoods = new System.Windows.Forms.Label();
            this.lbl_st_taskId = new System.Windows.Forms.Label();
            this.lbl_st_heartBeat = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_expert_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.btn_reversal_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.btn_forwardFRotation_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.but_close_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.btn_retreat_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.btn_forward_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.but_inital_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.but_clear_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.but_manual_cmd = new WinformControlLibraryExtension.ButtonExt();
            this.panel4 = new System.Windows.Forms.Panel();
            this.meter_speed = new WinformControlLibraryExtension.MeterExt();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lab_command_barCode);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.lab_command_endLevel);
            this.panel1.Controls.Add(this.lab_command_endBay);
            this.panel1.Controls.Add(this.lab_command_endRow);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.lab_command_startLevel);
            this.panel1.Controls.Add(this.lab_command_startBay);
            this.panel1.Controls.Add(this.lab_command_startRow);
            this.panel1.Controls.Add(this.lab_command_taskType);
            this.panel1.Controls.Add(this.lab_command_taskId);
            this.panel1.Controls.Add(this.lab_command_heart);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 239);
            this.panel1.TabIndex = 0;
            // 
            // lab_command_barCode
            // 
            this.lab_command_barCode.AutoSize = true;
            this.lab_command_barCode.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_barCode.Location = new System.Drawing.Point(234, 152);
            this.lab_command_barCode.Name = "lab_command_barCode";
            this.lab_command_barCode.Size = new System.Drawing.Size(49, 14);
            this.lab_command_barCode.TabIndex = 20;
            this.lab_command_barCode.Text = "label8";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label17.Location = new System.Drawing.Point(191, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 14);
            this.label17.TabIndex = 19;
            this.label17.Text = "条码：";
            // 
            // lab_command_endLevel
            // 
            this.lab_command_endLevel.AutoSize = true;
            this.lab_command_endLevel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_endLevel.Location = new System.Drawing.Point(234, 125);
            this.lab_command_endLevel.Name = "lab_command_endLevel";
            this.lab_command_endLevel.Size = new System.Drawing.Size(49, 14);
            this.lab_command_endLevel.TabIndex = 18;
            this.lab_command_endLevel.Text = "label7";
            // 
            // lab_command_endBay
            // 
            this.lab_command_endBay.AutoSize = true;
            this.lab_command_endBay.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_endBay.Location = new System.Drawing.Point(234, 98);
            this.lab_command_endBay.Name = "lab_command_endBay";
            this.lab_command_endBay.Size = new System.Drawing.Size(49, 14);
            this.lab_command_endBay.TabIndex = 17;
            this.lab_command_endBay.Text = "label8";
            // 
            // lab_command_endRow
            // 
            this.lab_command_endRow.AutoSize = true;
            this.lab_command_endRow.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_endRow.Location = new System.Drawing.Point(234, 68);
            this.lab_command_endRow.Name = "lab_command_endRow";
            this.lab_command_endRow.Size = new System.Drawing.Size(56, 14);
            this.lab_command_endRow.TabIndex = 16;
            this.lab_command_endRow.Text = "label10";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label10.Location = new System.Drawing.Point(177, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 14);
            this.label10.TabIndex = 15;
            this.label10.Text = "目的层：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label14.Location = new System.Drawing.Point(177, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 14);
            this.label14.TabIndex = 14;
            this.label14.Text = "目的列：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label15.Location = new System.Drawing.Point(177, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 14);
            this.label15.TabIndex = 13;
            this.label15.Text = "目的排：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 16F);
            this.label25.Location = new System.Drawing.Point(14, 15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 22);
            this.label25.TabIndex = 12;
            this.label25.Text = "下发指令";
            // 
            // lab_command_startLevel
            // 
            this.lab_command_startLevel.AutoSize = true;
            this.lab_command_startLevel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_startLevel.Location = new System.Drawing.Point(89, 202);
            this.lab_command_startLevel.Name = "lab_command_startLevel";
            this.lab_command_startLevel.Size = new System.Drawing.Size(56, 14);
            this.lab_command_startLevel.TabIndex = 11;
            this.lab_command_startLevel.Text = "label12";
            // 
            // lab_command_startBay
            // 
            this.lab_command_startBay.AutoSize = true;
            this.lab_command_startBay.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_startBay.Location = new System.Drawing.Point(88, 174);
            this.lab_command_startBay.Name = "lab_command_startBay";
            this.lab_command_startBay.Size = new System.Drawing.Size(56, 14);
            this.lab_command_startBay.TabIndex = 10;
            this.lab_command_startBay.Text = "label11";
            // 
            // lab_command_startRow
            // 
            this.lab_command_startRow.AutoSize = true;
            this.lab_command_startRow.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_startRow.Location = new System.Drawing.Point(89, 152);
            this.lab_command_startRow.Name = "lab_command_startRow";
            this.lab_command_startRow.Size = new System.Drawing.Size(56, 14);
            this.lab_command_startRow.TabIndex = 9;
            this.lab_command_startRow.Text = "label10";
            // 
            // lab_command_taskType
            // 
            this.lab_command_taskType.AutoSize = true;
            this.lab_command_taskType.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_taskType.Location = new System.Drawing.Point(89, 125);
            this.lab_command_taskType.Name = "lab_command_taskType";
            this.lab_command_taskType.Size = new System.Drawing.Size(49, 14);
            this.lab_command_taskType.TabIndex = 8;
            this.lab_command_taskType.Text = "label9";
            // 
            // lab_command_taskId
            // 
            this.lab_command_taskId.AutoSize = true;
            this.lab_command_taskId.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_taskId.Location = new System.Drawing.Point(88, 98);
            this.lab_command_taskId.Name = "lab_command_taskId";
            this.lab_command_taskId.Size = new System.Drawing.Size(49, 14);
            this.lab_command_taskId.TabIndex = 7;
            this.lab_command_taskId.Text = "label8";
            // 
            // lab_command_heart
            // 
            this.lab_command_heart.AutoSize = true;
            this.lab_command_heart.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lab_command_heart.Location = new System.Drawing.Point(88, 68);
            this.lab_command_heart.Name = "lab_command_heart";
            this.lab_command_heart.Size = new System.Drawing.Size(49, 14);
            this.lab_command_heart.TabIndex = 6;
            this.lab_command_heart.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label6.Location = new System.Drawing.Point(29, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "起始层：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label5.Location = new System.Drawing.Point(28, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 4;
            this.label5.Text = "起始列：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label4.Location = new System.Drawing.Point(28, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "起始排：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label3.Location = new System.Drawing.Point(15, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "任务类型：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label2.Location = new System.Drawing.Point(28, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "任务号：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label1.Location = new System.Drawing.Point(14, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "上位心跳：";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lbl_charging_cap);
            this.panel2.Controls.Add(this.lbl_st_socCap);
            this.panel2.Controls.Add(this.label62);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.lbl_st_socBat);
            this.panel2.Controls.Add(this.lbl_st_setUp);
            this.panel2.Controls.Add(this.lbl_st_position);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Controls.Add(this.label57);
            this.panel2.Controls.Add(this.label58);
            this.panel2.Controls.Add(this.lbl_charging_bat);
            this.panel2.Controls.Add(this.label60);
            this.panel2.Controls.Add(this.lbl_st_currentLevel);
            this.panel2.Controls.Add(this.lbl_st_currentBay);
            this.panel2.Controls.Add(this.lbl_st_currentRow);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.lbl_st_taskStatus);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.lbl_st_barCode);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.lbl_st_taskType);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.lbl_st_workMode);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.lbl_st_deviceStatus);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.lbl_st_endLevel);
            this.panel2.Controls.Add(this.lbl_st_endBay);
            this.panel2.Controls.Add(this.lbl_st_endRow);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.lbl_st_startLevel);
            this.panel2.Controls.Add(this.lbl_st_startBay);
            this.panel2.Controls.Add(this.lbl_st_startRow);
            this.panel2.Controls.Add(this.lbl_st_loadGoods);
            this.panel2.Controls.Add(this.lbl_st_taskId);
            this.panel2.Controls.Add(this.lbl_st_heartBeat);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Location = new System.Drawing.Point(338, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(335, 404);
            this.panel2.TabIndex = 1;
            // 
            // lbl_charging_cap
            // 
            this.lbl_charging_cap.AutoSize = true;
            this.lbl_charging_cap.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_charging_cap.Location = new System.Drawing.Point(275, 331);
            this.lbl_charging_cap.Name = "lbl_charging_cap";
            this.lbl_charging_cap.Size = new System.Drawing.Size(49, 14);
            this.lbl_charging_cap.TabIndex = 66;
            this.lbl_charging_cap.Text = "label8";
            // 
            // lbl_st_socCap
            // 
            this.lbl_st_socCap.AutoSize = true;
            this.lbl_st_socCap.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_socCap.Location = new System.Drawing.Point(111, 360);
            this.lbl_st_socCap.Name = "lbl_st_socCap";
            this.lbl_st_socCap.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_socCap.TabIndex = 65;
            this.lbl_st_socCap.Text = "label61";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label62.Location = new System.Drawing.Point(3, 359);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(119, 14);
            this.label62.TabIndex = 64;
            this.label62.Text = "剩余电量(电容)：";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label63.Location = new System.Drawing.Point(165, 331);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(119, 14);
            this.label63.TabIndex = 63;
            this.label63.Text = "充电状态(电容)：";
            // 
            // lbl_st_socBat
            // 
            this.lbl_st_socBat.AutoSize = true;
            this.lbl_st_socBat.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_socBat.Location = new System.Drawing.Point(111, 330);
            this.lbl_st_socBat.Name = "lbl_st_socBat";
            this.lbl_st_socBat.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_socBat.TabIndex = 62;
            this.lbl_st_socBat.Text = "label53";
            // 
            // lbl_st_setUp
            // 
            this.lbl_st_setUp.AutoSize = true;
            this.lbl_st_setUp.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_setUp.Location = new System.Drawing.Point(247, 277);
            this.lbl_st_setUp.Name = "lbl_st_setUp";
            this.lbl_st_setUp.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_setUp.TabIndex = 61;
            this.lbl_st_setUp.Text = "label54";
            // 
            // lbl_st_position
            // 
            this.lbl_st_position.AutoSize = true;
            this.lbl_st_position.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_position.Location = new System.Drawing.Point(82, 303);
            this.lbl_st_position.Name = "lbl_st_position";
            this.lbl_st_position.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_position.TabIndex = 60;
            this.lbl_st_position.Text = "label10";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label56.Location = new System.Drawing.Point(3, 330);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(119, 14);
            this.label56.TabIndex = 59;
            this.label56.Text = "剩余电量(电池)：";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label57.Location = new System.Drawing.Point(168, 277);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(77, 14);
            this.label57.TabIndex = 58;
            this.label57.Text = "当前步序：";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label58.Location = new System.Drawing.Point(10, 303);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(77, 14);
            this.label58.TabIndex = 57;
            this.label58.Text = "当前位置：";
            // 
            // lbl_charging_bat
            // 
            this.lbl_charging_bat.AutoSize = true;
            this.lbl_charging_bat.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_charging_bat.Location = new System.Drawing.Point(275, 303);
            this.lbl_charging_bat.Name = "lbl_charging_bat";
            this.lbl_charging_bat.Size = new System.Drawing.Size(49, 14);
            this.lbl_charging_bat.TabIndex = 56;
            this.lbl_charging_bat.Text = "label8";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label60.Location = new System.Drawing.Point(165, 303);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(119, 14);
            this.label60.TabIndex = 55;
            this.label60.Text = "充电状态(电池)：";
            // 
            // lbl_st_currentLevel
            // 
            this.lbl_st_currentLevel.AutoSize = true;
            this.lbl_st_currentLevel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_currentLevel.Location = new System.Drawing.Point(83, 277);
            this.lbl_st_currentLevel.Name = "lbl_st_currentLevel";
            this.lbl_st_currentLevel.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_currentLevel.TabIndex = 54;
            this.lbl_st_currentLevel.Text = "label47";
            // 
            // lbl_st_currentBay
            // 
            this.lbl_st_currentBay.AutoSize = true;
            this.lbl_st_currentBay.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_currentBay.Location = new System.Drawing.Point(247, 225);
            this.lbl_st_currentBay.Name = "lbl_st_currentBay";
            this.lbl_st_currentBay.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_currentBay.TabIndex = 53;
            this.lbl_st_currentBay.Text = "label48";
            // 
            // lbl_st_currentRow
            // 
            this.lbl_st_currentRow.AutoSize = true;
            this.lbl_st_currentRow.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_currentRow.Location = new System.Drawing.Point(83, 250);
            this.lbl_st_currentRow.Name = "lbl_st_currentRow";
            this.lbl_st_currentRow.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_currentRow.TabIndex = 52;
            this.lbl_st_currentRow.Text = "label10";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label50.Location = new System.Drawing.Point(24, 277);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(63, 14);
            this.label50.TabIndex = 51;
            this.label50.Text = "当前层：";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label51.Location = new System.Drawing.Point(182, 225);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(63, 14);
            this.label51.TabIndex = 50;
            this.label51.Text = "当前列：";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label52.Location = new System.Drawing.Point(24, 250);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(63, 14);
            this.label52.TabIndex = 49;
            this.label52.Text = "当前排：";
            // 
            // lbl_st_taskStatus
            // 
            this.lbl_st_taskStatus.AutoSize = true;
            this.lbl_st_taskStatus.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_taskStatus.Location = new System.Drawing.Point(86, 125);
            this.lbl_st_taskStatus.Name = "lbl_st_taskStatus";
            this.lbl_st_taskStatus.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_taskStatus.TabIndex = 48;
            this.lbl_st_taskStatus.Text = "label9";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label46.Location = new System.Drawing.Point(13, 125);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 14);
            this.label46.TabIndex = 47;
            this.label46.Text = "任务状态：";
            // 
            // lbl_st_barCode
            // 
            this.lbl_st_barCode.AutoSize = true;
            this.lbl_st_barCode.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_barCode.Location = new System.Drawing.Point(216, 360);
            this.lbl_st_barCode.Name = "lbl_st_barCode";
            this.lbl_st_barCode.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_barCode.TabIndex = 46;
            this.lbl_st_barCode.Text = "label8";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label44.Location = new System.Drawing.Point(165, 360);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 14);
            this.label44.TabIndex = 45;
            this.label44.Text = "条码：";
            // 
            // lbl_st_taskType
            // 
            this.lbl_st_taskType.AutoSize = true;
            this.lbl_st_taskType.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_taskType.Location = new System.Drawing.Point(86, 152);
            this.lbl_st_taskType.Name = "lbl_st_taskType";
            this.lbl_st_taskType.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_taskType.TabIndex = 44;
            this.lbl_st_taskType.Text = "label8";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label42.Location = new System.Drawing.Point(10, 152);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(77, 14);
            this.label42.TabIndex = 43;
            this.label42.Text = "任务类型：";
            // 
            // lbl_st_workMode
            // 
            this.lbl_st_workMode.AutoSize = true;
            this.lbl_st_workMode.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_workMode.Location = new System.Drawing.Point(247, 68);
            this.lbl_st_workMode.Name = "lbl_st_workMode";
            this.lbl_st_workMode.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_workMode.TabIndex = 42;
            this.lbl_st_workMode.Text = "label9";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label40.Location = new System.Drawing.Point(168, 68);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 14);
            this.label40.TabIndex = 41;
            this.label40.Text = "工作模式：";
            // 
            // lbl_st_deviceStatus
            // 
            this.lbl_st_deviceStatus.AutoSize = true;
            this.lbl_st_deviceStatus.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_deviceStatus.Location = new System.Drawing.Point(247, 98);
            this.lbl_st_deviceStatus.Name = "lbl_st_deviceStatus";
            this.lbl_st_deviceStatus.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_deviceStatus.TabIndex = 40;
            this.lbl_st_deviceStatus.Text = "label8";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label18.Location = new System.Drawing.Point(168, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 14);
            this.label18.TabIndex = 39;
            this.label18.Text = "设备状态：";
            // 
            // lbl_st_endLevel
            // 
            this.lbl_st_endLevel.AutoSize = true;
            this.lbl_st_endLevel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_endLevel.Location = new System.Drawing.Point(247, 202);
            this.lbl_st_endLevel.Name = "lbl_st_endLevel";
            this.lbl_st_endLevel.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_endLevel.TabIndex = 38;
            this.lbl_st_endLevel.Text = "label19";
            // 
            // lbl_st_endBay
            // 
            this.lbl_st_endBay.AutoSize = true;
            this.lbl_st_endBay.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_endBay.Location = new System.Drawing.Point(247, 174);
            this.lbl_st_endBay.Name = "lbl_st_endBay";
            this.lbl_st_endBay.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_endBay.TabIndex = 37;
            this.lbl_st_endBay.Text = "label20";
            // 
            // lbl_st_endRow
            // 
            this.lbl_st_endRow.AutoSize = true;
            this.lbl_st_endRow.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_endRow.Location = new System.Drawing.Point(247, 152);
            this.lbl_st_endRow.Name = "lbl_st_endRow";
            this.lbl_st_endRow.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_endRow.TabIndex = 36;
            this.lbl_st_endRow.Text = "label10";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label22.Location = new System.Drawing.Point(182, 202);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 14);
            this.label22.TabIndex = 35;
            this.label22.Text = "目的层：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label23.Location = new System.Drawing.Point(182, 174);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 14);
            this.label23.TabIndex = 34;
            this.label23.Text = "目的列：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label24.Location = new System.Drawing.Point(182, 152);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 14);
            this.label24.TabIndex = 33;
            this.label24.Text = "目的排：";
            // 
            // lbl_st_startLevel
            // 
            this.lbl_st_startLevel.AutoSize = true;
            this.lbl_st_startLevel.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_startLevel.Location = new System.Drawing.Point(86, 225);
            this.lbl_st_startLevel.Name = "lbl_st_startLevel";
            this.lbl_st_startLevel.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_startLevel.TabIndex = 32;
            this.lbl_st_startLevel.Text = "label27";
            // 
            // lbl_st_startBay
            // 
            this.lbl_st_startBay.AutoSize = true;
            this.lbl_st_startBay.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_startBay.Location = new System.Drawing.Point(86, 202);
            this.lbl_st_startBay.Name = "lbl_st_startBay";
            this.lbl_st_startBay.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_startBay.TabIndex = 31;
            this.lbl_st_startBay.Text = "label28";
            // 
            // lbl_st_startRow
            // 
            this.lbl_st_startRow.AutoSize = true;
            this.lbl_st_startRow.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_startRow.Location = new System.Drawing.Point(86, 174);
            this.lbl_st_startRow.Name = "lbl_st_startRow";
            this.lbl_st_startRow.Size = new System.Drawing.Size(56, 14);
            this.lbl_st_startRow.TabIndex = 30;
            this.lbl_st_startRow.Text = "label10";
            // 
            // lbl_st_loadGoods
            // 
            this.lbl_st_loadGoods.AutoSize = true;
            this.lbl_st_loadGoods.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_loadGoods.Location = new System.Drawing.Point(247, 125);
            this.lbl_st_loadGoods.Name = "lbl_st_loadGoods";
            this.lbl_st_loadGoods.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_loadGoods.TabIndex = 29;
            this.lbl_st_loadGoods.Text = "label9";
            // 
            // lbl_st_taskId
            // 
            this.lbl_st_taskId.AutoSize = true;
            this.lbl_st_taskId.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_taskId.Location = new System.Drawing.Point(86, 98);
            this.lbl_st_taskId.Name = "lbl_st_taskId";
            this.lbl_st_taskId.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_taskId.TabIndex = 28;
            this.lbl_st_taskId.Text = "label8";
            // 
            // lbl_st_heartBeat
            // 
            this.lbl_st_heartBeat.AutoSize = true;
            this.lbl_st_heartBeat.Font = new System.Drawing.Font("宋体", 10.5F);
            this.lbl_st_heartBeat.Location = new System.Drawing.Point(86, 68);
            this.lbl_st_heartBeat.Name = "lbl_st_heartBeat";
            this.lbl_st_heartBeat.Size = new System.Drawing.Size(49, 14);
            this.lbl_st_heartBeat.TabIndex = 27;
            this.lbl_st_heartBeat.Text = "label7";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label33.Location = new System.Drawing.Point(24, 225);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(63, 14);
            this.label33.TabIndex = 26;
            this.label33.Text = "起始层：";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label34.Location = new System.Drawing.Point(24, 202);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(63, 14);
            this.label34.TabIndex = 25;
            this.label34.Text = "起始列：";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label35.Location = new System.Drawing.Point(24, 174);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(63, 14);
            this.label35.TabIndex = 24;
            this.label35.Text = "起始排：";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label36.Location = new System.Drawing.Point(168, 125);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(77, 14);
            this.label36.TabIndex = 23;
            this.label36.Text = "载货状态：";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label37.Location = new System.Drawing.Point(24, 98);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 14);
            this.label37.TabIndex = 22;
            this.label37.Text = "任务号：";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 10.5F);
            this.label38.Location = new System.Drawing.Point(13, 68);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 14);
            this.label38.TabIndex = 21;
            this.label38.Text = "小车心跳：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 16F);
            this.label26.Location = new System.Drawing.Point(12, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 22);
            this.label26.TabIndex = 12;
            this.label26.Text = "设备状态";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.btn_expert_cmd);
            this.panel3.Controls.Add(this.btn_reversal_cmd);
            this.panel3.Controls.Add(this.btn_forwardFRotation_cmd);
            this.panel3.Controls.Add(this.but_close_cmd);
            this.panel3.Controls.Add(this.btn_retreat_cmd);
            this.panel3.Controls.Add(this.btn_forward_cmd);
            this.panel3.Controls.Add(this.but_inital_cmd);
            this.panel3.Controls.Add(this.but_clear_cmd);
            this.panel3.Controls.Add(this.but_manual_cmd);
            this.panel3.Location = new System.Drawing.Point(5, 415);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(667, 55);
            this.panel3.TabIndex = 2;
            // 
            // btn_expert_cmd
            // 
            this.btn_expert_cmd.Animation.Options.AllTransformTime = 250D;
            this.btn_expert_cmd.Animation.Options.Data = null;
            this.btn_expert_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.btn_expert_cmd.FlatAppearance.BorderSize = 0;
            this.btn_expert_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.btn_expert_cmd.ForeColor = System.Drawing.Color.Black;
            this.btn_expert_cmd.Location = new System.Drawing.Point(231, 8);
            this.btn_expert_cmd.Name = "btn_expert_cmd";
            this.btn_expert_cmd.Size = new System.Drawing.Size(64, 40);
            this.btn_expert_cmd.TabIndex = 8;
            this.btn_expert_cmd.Text = "专家";
            this.btn_expert_cmd.Click += new System.EventHandler(this.btn_expert_cmd_Click);
            // 
            // btn_reversal_cmd
            // 
            this.btn_reversal_cmd.Animation.Options.AllTransformTime = 250D;
            this.btn_reversal_cmd.Animation.Options.Data = null;
            this.btn_reversal_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.btn_reversal_cmd.FlatAppearance.BorderSize = 0;
            this.btn_reversal_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.btn_reversal_cmd.ForeColor = System.Drawing.Color.Black;
            this.btn_reversal_cmd.Location = new System.Drawing.Point(512, 8);
            this.btn_reversal_cmd.Name = "btn_reversal_cmd";
            this.btn_reversal_cmd.Size = new System.Drawing.Size(64, 40);
            this.btn_reversal_cmd.TabIndex = 7;
            this.btn_reversal_cmd.Text = "反转";
            this.btn_reversal_cmd.Click += new System.EventHandler(this.btn_reversal_cmd_Click);
            // 
            // btn_forwardFRotation_cmd
            // 
            this.btn_forwardFRotation_cmd.Animation.Options.AllTransformTime = 250D;
            this.btn_forwardFRotation_cmd.Animation.Options.Data = null;
            this.btn_forwardFRotation_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.btn_forwardFRotation_cmd.FlatAppearance.BorderSize = 0;
            this.btn_forwardFRotation_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.btn_forwardFRotation_cmd.ForeColor = System.Drawing.Color.Black;
            this.btn_forwardFRotation_cmd.Location = new System.Drawing.Point(442, 8);
            this.btn_forwardFRotation_cmd.Name = "btn_forwardFRotation_cmd";
            this.btn_forwardFRotation_cmd.Size = new System.Drawing.Size(64, 40);
            this.btn_forwardFRotation_cmd.TabIndex = 6;
            this.btn_forwardFRotation_cmd.Text = "正转";
            this.btn_forwardFRotation_cmd.Click += new System.EventHandler(this.btn_forwardFRotation_cmd_Click);
            // 
            // but_close_cmd
            // 
            this.but_close_cmd.Animation.Options.AllTransformTime = 250D;
            this.but_close_cmd.Animation.Options.Data = null;
            this.but_close_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.but_close_cmd.FlatAppearance.BorderSize = 0;
            this.but_close_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.but_close_cmd.ForeColor = System.Drawing.Color.Black;
            this.but_close_cmd.Location = new System.Drawing.Point(587, 8);
            this.but_close_cmd.Name = "but_close_cmd";
            this.but_close_cmd.Size = new System.Drawing.Size(64, 40);
            this.but_close_cmd.TabIndex = 3;
            this.but_close_cmd.Text = "关闭";
            this.but_close_cmd.Click += new System.EventHandler(this.but_close_cmd_Click);
            // 
            // btn_retreat_cmd
            // 
            this.btn_retreat_cmd.Animation.Options.AllTransformTime = 250D;
            this.btn_retreat_cmd.Animation.Options.Data = null;
            this.btn_retreat_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.btn_retreat_cmd.FlatAppearance.BorderSize = 0;
            this.btn_retreat_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.btn_retreat_cmd.ForeColor = System.Drawing.Color.Black;
            this.btn_retreat_cmd.Location = new System.Drawing.Point(372, 8);
            this.btn_retreat_cmd.Name = "btn_retreat_cmd";
            this.btn_retreat_cmd.Size = new System.Drawing.Size(64, 40);
            this.btn_retreat_cmd.TabIndex = 5;
            this.btn_retreat_cmd.Text = "后退";
            this.btn_retreat_cmd.Click += new System.EventHandler(this.btn_retreat_cmd_Click);
            // 
            // btn_forward_cmd
            // 
            this.btn_forward_cmd.Animation.Options.AllTransformTime = 250D;
            this.btn_forward_cmd.Animation.Options.Data = null;
            this.btn_forward_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.btn_forward_cmd.FlatAppearance.BorderSize = 0;
            this.btn_forward_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.btn_forward_cmd.ForeColor = System.Drawing.Color.Black;
            this.btn_forward_cmd.Location = new System.Drawing.Point(301, 8);
            this.btn_forward_cmd.Name = "btn_forward_cmd";
            this.btn_forward_cmd.Size = new System.Drawing.Size(64, 40);
            this.btn_forward_cmd.TabIndex = 4;
            this.btn_forward_cmd.Text = "前进";
            this.btn_forward_cmd.Click += new System.EventHandler(this.btn_forward_cmd_Click);
            // 
            // but_inital_cmd
            // 
            this.but_inital_cmd.Animation.Options.AllTransformTime = 250D;
            this.but_inital_cmd.Animation.Options.Data = null;
            this.but_inital_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.but_inital_cmd.FlatAppearance.BorderSize = 0;
            this.but_inital_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.but_inital_cmd.ForeColor = System.Drawing.Color.Black;
            this.but_inital_cmd.Location = new System.Drawing.Point(157, 8);
            this.but_inital_cmd.Name = "but_inital_cmd";
            this.but_inital_cmd.Size = new System.Drawing.Size(64, 40);
            this.but_inital_cmd.TabIndex = 2;
            this.but_inital_cmd.Text = "初始化";
            this.but_inital_cmd.Click += new System.EventHandler(this.but_inital_cmd_Click);
            // 
            // but_clear_cmd
            // 
            this.but_clear_cmd.Animation.Options.AllTransformTime = 250D;
            this.but_clear_cmd.Animation.Options.Data = null;
            this.but_clear_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.but_clear_cmd.FlatAppearance.BorderSize = 0;
            this.but_clear_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.but_clear_cmd.ForeColor = System.Drawing.Color.Black;
            this.but_clear_cmd.Location = new System.Drawing.Point(87, 8);
            this.but_clear_cmd.Name = "but_clear_cmd";
            this.but_clear_cmd.Size = new System.Drawing.Size(64, 40);
            this.but_clear_cmd.TabIndex = 1;
            this.but_clear_cmd.Text = "清错";
            this.but_clear_cmd.Click += new System.EventHandler(this.but_clear_cmd_Click);
            // 
            // but_manual_cmd
            // 
            this.but_manual_cmd.Animation.Options.AllTransformTime = 250D;
            this.but_manual_cmd.Animation.Options.Data = null;
            this.but_manual_cmd.BackColor = System.Drawing.SystemColors.Control;
            this.but_manual_cmd.FlatAppearance.BorderSize = 0;
            this.but_manual_cmd.Font = new System.Drawing.Font("宋体", 12F);
            this.but_manual_cmd.ForeColor = System.Drawing.Color.Black;
            this.but_manual_cmd.Location = new System.Drawing.Point(11, 8);
            this.but_manual_cmd.Name = "but_manual_cmd";
            this.but_manual_cmd.Size = new System.Drawing.Size(64, 40);
            this.but_manual_cmd.TabIndex = 0;
            this.but_manual_cmd.Text = "手动";
            this.but_manual_cmd.Click += new System.EventHandler(this.but_manual_cmd_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.meter_speed);
            this.panel4.Location = new System.Drawing.Point(6, 250);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(323, 159);
            this.panel4.TabIndex = 3;
            // 
            // meter_speed
            // 
            this.meter_speed.ArcAngle = 180;
            this.meter_speed.ArcBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.meter_speed.ArcValueColor = System.Drawing.Color.Chartreuse;
            this.meter_speed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.meter_speed.BackInnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.meter_speed.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.meter_speed.CausesValidation = false;
            this.meter_speed.ForeColor = System.Drawing.Color.Black;
            this.meter_speed.Location = new System.Drawing.Point(24, 5);
            this.meter_speed.Name = "meter_speed";
            this.meter_speed.PointerCapColor = System.Drawing.Color.CornflowerBlue;
            this.meter_speed.PointerColor = System.Drawing.Color.CornflowerBlue;
            this.meter_speed.ScaleColor = System.Drawing.Color.White;
            this.meter_speed.ScaleInterval = 20F;
            this.meter_speed.Size = new System.Drawing.Size(259, 142);
            this.meter_speed.SplitScaleColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.meter_speed.TabIndex = 0;
            this.meter_speed.TabStop = false;
            this.meter_speed.Text = "速度";
            // 
            // FormSeedingWallShuttle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 471);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormSeedingWallShuttle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SeedingWallShuttle-";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSeedingWallShuttle_FormClosed);
            this.Load += new System.EventHandler(this.FormSeedingWallShuttle_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lab_command_startLevel;
        private System.Windows.Forms.Label lab_command_startBay;
        private System.Windows.Forms.Label lab_command_startRow;
        private System.Windows.Forms.Label lab_command_taskType;
        private System.Windows.Forms.Label lab_command_taskId;
        private System.Windows.Forms.Label lab_command_heart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private WinformControlLibraryExtension.ButtonExt but_inital_cmd;
        private WinformControlLibraryExtension.ButtonExt but_clear_cmd;
        private WinformControlLibraryExtension.ButtonExt but_manual_cmd;
        private WinformControlLibraryExtension.ButtonExt but_close_cmd;
        private System.Windows.Forms.Label lab_command_barCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lab_command_endLevel;
        private System.Windows.Forms.Label lab_command_endBay;
        private System.Windows.Forms.Label lab_command_endRow;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private WinformControlLibraryExtension.ButtonExt btn_reversal_cmd;
        private WinformControlLibraryExtension.ButtonExt btn_forwardFRotation_cmd;
        private WinformControlLibraryExtension.ButtonExt btn_retreat_cmd;
        private WinformControlLibraryExtension.ButtonExt btn_forward_cmd;
        private System.Windows.Forms.Label lbl_st_deviceStatus;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lbl_st_endLevel;
        private System.Windows.Forms.Label lbl_st_endBay;
        private System.Windows.Forms.Label lbl_st_endRow;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lbl_st_startLevel;
        private System.Windows.Forms.Label lbl_st_startBay;
        private System.Windows.Forms.Label lbl_st_startRow;
        private System.Windows.Forms.Label lbl_st_loadGoods;
        private System.Windows.Forms.Label lbl_st_taskId;
        private System.Windows.Forms.Label lbl_st_heartBeat;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lbl_st_workMode;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lbl_st_taskType;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lbl_st_barCode;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lbl_st_taskStatus;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lbl_st_currentLevel;
        private System.Windows.Forms.Label lbl_st_currentBay;
        private System.Windows.Forms.Label lbl_st_currentRow;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label lbl_st_socBat;
        private System.Windows.Forms.Label lbl_st_setUp;
        private System.Windows.Forms.Label lbl_st_position;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label lbl_charging_bat;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label lbl_charging_cap;
        private System.Windows.Forms.Label lbl_st_socCap;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private WinformControlLibraryExtension.ButtonExt btn_expert_cmd;
        private System.Windows.Forms.Panel panel4;
        private WinformControlLibraryExtension.MeterExt meter_speed;
    }
}