﻿using Kengic.RCS.Models.Table;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;
using WinformControlLibraryExtension;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace RCS.DeviceForm
{
    public partial class FormAreaSystemSet : Form
    {
        private ISqlSugarClient sqlSugarClient = new SqlSugarScope(new ConnectionConfig()
        {
            ConnectionString = ConfigurationManager.AppSettings["LocalConnectionString"],
            DbType = SqlSugar.DbType.SqlServer,
            InitKeyType = InitKeyType.Attribute,
            IsAutoCloseConnection = true  //是否自动关闭连接  Yes
        });
        public FormAreaSystemSet()
        {
            InitializeComponent();
        }

        private void FormAreaSystemSet_Load(object sender, EventArgs e)
        {
            this.Text = "库区设置";

            //this.sbe_show_status.CheckedChanged += new WinformControlLibraryExtension.SwitchButtonExt.CheckedChangedEventHandler(this.sbe_show_status_CheckedChanged);

            #region 绑定combox 

            var area_list = sqlSugarClient.Queryable<table_rcs_system_area_set>().ToList();

            DataTable dt = new DataTable();
            dt.Columns.Add("area_no");
            dt.Columns.Add("area_name");
            dt.Columns.Add("area_show_status");
            foreach (var area in area_list)
            {
                DataRow dr = dt.NewRow();
                dr[0] = area.AREA_NO;
                dr[1] = area.AREA_NAME;
                dr[2] = area.AREA_SHOW_STATUS;
                dt.Rows.Add(dr);
            }
            this.cob_area_no.DataSource = dt;
            this.cob_area_no.DisplayMember = "area_name";
            this.cob_area_no.ValueMember = "area_no";

            //获取是否展示状态
            /*  var area_show_sttaus = dt.Rows[this.cob_area_no.SelectedIndex].ItemArray[2].ToString() ;
              //将按钮设置为正常开启
              this.sbe_show_status.Checked = area_show_sttaus == "1" ? true : false;
  */
            #endregion

        }

        private void btn_insert_area_Click(object sender, EventArgs e)
        {
            //判断库区编号
            var area_no = this.tb_area_box.Text.Trim();
            if (string.IsNullOrEmpty(area_no))
            {
                toolStripStatusLabel1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "库区编号不允许为空！";
                //MessageBoxExt.Show(this, "库区编号不允许为空！", "提示");
                return;
            }
            //判断库区名称
            var area_name = this.tb_area_name_box.Text.Trim();
            if (string.IsNullOrEmpty(area_name))
            {
                toolStripStatusLabel1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "库区名称不允许为空！";
                //MessageBoxExt.Show(this, "库区名称不允许为空！", "提示");
                return;
            }

            var tab_rcs_system_area_set = new table_rcs_system_area_set()
            {
                Id = Guid.NewGuid().ToString(),
                AREA_NO = area_no,
                AREA_NAME = area_name,
                CREATE_TIME = DateTime.Now,
                AREA_SHOW_STATUS = "1"
            };

            var system_set_result = sqlSugarClient.Insertable<table_rcs_system_area_set>(tab_rcs_system_area_set).ExecuteCommand();
            if (system_set_result > 0)
            {
                toolStripStatusLabel1.Text = area_no + "添加成功！";
                //MessageBoxExt.Show(this, area_no + "添加失败！", "提示");
            }
            else
            {
                toolStripStatusLabel1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = area_no + "添加失败！";
                //MessageBoxExt.Show(this, area_no + "添加失败！", "提示");
            }

        }

        private void tb_area_box_KeyPress(object sender, KeyPressEventArgs e)
        {
            Regex rg = new Regex("^[\u4e00-\u9fa5a-zA-Z0-9\b]$");
            if (!rg.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }

        }

        private void cob_area_no_SelectedIndexChanged(object sender, EventArgs e)
        {

            var area_List = sqlSugarClient.Queryable<table_rcs_system_area_set>().ToList();
            var areas = area_List[this.cob_area_no.SelectedIndex];

            //将按钮设置为正常开启
            this.sbe_show_status.Checked = areas.AREA_SHOW_STATUS == "1" ? true : false;
        }

        private void sbe_show_status_CheckedChanged(object sender, SwitchButtonExt.CheckedChangedEventArgs e)
        {

            var area_row_view = (System.Data.DataRowView)this.cob_area_no.SelectedValue;
            var area_view = area_row_view.Row[0].ToString();
            var result = sqlSugarClient.Updateable<table_rcs_system_area_set>().SetColumns(it => new table_rcs_system_area_set() { AREA_SHOW_STATUS = e.Checked == true ? "1" : "0" })
                         .Where(it => it.AREA_NO == area_view).ExecuteCommand();

            if (result > 0)
            {
                toolStripStatusLabel1.Text = this.cob_area_no.SelectedText + "更新状态成功！";
                //MessageBoxExt.Show(this, this.cob_area_no.SelectedText + "更新状态失败！", "提示");
            }
        }

        private void sbe_show_status_Click(object sender, EventArgs e)
        {
            var area_show_status = this.sbe_show_status.Checked == true ? "0" : "1";
            var area_view = this.cob_area_no.SelectedValue;
            var result = sqlSugarClient.Updateable<table_rcs_system_area_set>().SetColumns(it => new table_rcs_system_area_set() { AREA_SHOW_STATUS = area_show_status })
                       .Where(it => it.AREA_NO == area_view).ExecuteCommand();

            if (result > 0)
            {
                toolStripStatusLabel1.Text = this.cob_area_no.SelectedText + "更新状态成功！";
                //MessageBoxExt.Show(this, this.cob_area_no.SelectedText + "更新状态失败！", "提示");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                       panel1.ClientRectangle,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                       panel2.ClientRectangle,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid,
                                       Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255))))),
                                       1,
                                       ButtonBorderStyle.Solid);
        }
    }
}
