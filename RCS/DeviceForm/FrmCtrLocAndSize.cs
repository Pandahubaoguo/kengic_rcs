﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Kengic.RCS.Models.Table;
using SqlSugar;
using System.Configuration;

namespace RCS.DeviceSupportClass
{
    public partial class FrmCtrLocAndSize : Form
    {
        private PictureBox pic;
        private Label lbl;
        private String deviceId;
        private ISqlSugarClient sqlSugarClient = new SqlSugarScope(new ConnectionConfig()
        {
            ConnectionString = ConfigurationManager.AppSettings["LocalConnectionString"],
            DbType = SqlSugar.DbType.SqlServer,
            InitKeyType = InitKeyType.Attribute,
            IsAutoCloseConnection = true  //是否自动关闭连接  Yes
        });
        string controlType = string.Empty;
        public FrmCtrLocAndSize(PictureBox pic, string deviceId)
        {
            InitializeComponent();
            this.pic = pic;
            this.deviceId = deviceId;
            controlType = "PictureBox";
        }

        public FrmCtrLocAndSize(Label lbl, string deviceId)
        {
            InitializeComponent();
            this.lbl = lbl;
            this.deviceId = deviceId;
            controlType = "Label";
        }

        private void FrmCtrLocAndSize_Load(object sender, EventArgs e)
        {
            this.Text = Text + "-" + deviceId;

            if (controlType == "PictureBox")
            {
                NumUdX.Value = pic.Left;
                NumUdY.Value = pic.Top;
                NumUdWidth.Value = pic.Width;
                NumUdHeight.Value = pic.Height;
                this.NumUdX.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdY.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdWidth.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdHeight.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
            }
            if (controlType == "Label")
            {
                NumUdX.Value = lbl.Left;
                NumUdY.Value = lbl.Top;
                NumUdWidth.Value = lbl.Width;
                NumUdHeight.Value = lbl.Height;
                this.NumUdX.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdY.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdWidth.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
                this.NumUdHeight.ValueChanged += new System.EventHandler(this.NumUd_ValueChanged);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            var result = sqlSugarClient.Updateable<table_rcs_base_device>().SetColumns(it =>
            new table_rcs_base_device()
            {
                X = (int)(NumUdX.Value),
                Y = (int)(NumUdY.Value),
                WIDTH = (int)(NumUdWidth.Value),
                HEIGHT = (int)(NumUdHeight.Value)
            }
            ).Where(it => it.DEVICE_ID == deviceId).ExecuteCommand();
        }

        private void NumUd_ValueChanged(object sender, EventArgs e)
        {
            if (controlType == "PictureBox")
            {
                pic.Location = new Point((int)(NumUdX.Value), (int)(NumUdY.Value));
                pic.Size = new Size((int)(NumUdWidth.Value), (int)(NumUdHeight.Value));
            }
            if (controlType == "Label")
            {
                lbl.Location = new Point((int)(NumUdX.Value), (int)(NumUdY.Value));
                lbl.Size = new Size((int)(NumUdWidth.Value), (int)(NumUdHeight.Value));
            }
        }
    }
}
