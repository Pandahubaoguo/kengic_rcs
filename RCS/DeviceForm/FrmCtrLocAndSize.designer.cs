﻿namespace RCS.DeviceSupportClass
{
    partial class FrmCtrLocAndSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.NumUdX = new System.Windows.Forms.NumericUpDown();
            this.NumUdHeight = new System.Windows.Forms.NumericUpDown();
            this.NumUdWidth = new System.Windows.Forms.NumericUpDown();
            this.NumUdY = new System.Windows.Forms.NumericUpDown();
            this.btSave = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdY)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "X坐标";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y坐标";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "宽";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "高";
            // 
            // NumUdX
            // 
            this.NumUdX.Location = new System.Drawing.Point(108, 14);
            this.NumUdX.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.NumUdX.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NumUdX.Name = "NumUdX";
            this.NumUdX.Size = new System.Drawing.Size(120, 26);
            this.NumUdX.TabIndex = 4;
            this.NumUdX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // NumUdHeight
            // 
            this.NumUdHeight.Location = new System.Drawing.Point(108, 125);
            this.NumUdHeight.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.NumUdHeight.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumUdHeight.Name = "NumUdHeight";
            this.NumUdHeight.Size = new System.Drawing.Size(120, 26);
            this.NumUdHeight.TabIndex = 5;
            this.NumUdHeight.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // NumUdWidth
            // 
            this.NumUdWidth.Location = new System.Drawing.Point(108, 88);
            this.NumUdWidth.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.NumUdWidth.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumUdWidth.Name = "NumUdWidth";
            this.NumUdWidth.Size = new System.Drawing.Size(120, 26);
            this.NumUdWidth.TabIndex = 6;
            this.NumUdWidth.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // NumUdY
            // 
            this.NumUdY.Location = new System.Drawing.Point(108, 51);
            this.NumUdY.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.NumUdY.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NumUdY.Name = "NumUdY";
            this.NumUdY.Size = new System.Drawing.Size(120, 26);
            this.NumUdY.TabIndex = 7;
            this.NumUdY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(52, 166);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(85, 29);
            this.btSave.TabIndex = 8;
            this.btSave.Text = "保存";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(163, 166);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(85, 29);
            this.btClose.TabIndex = 9;
            this.btClose.Text = "关闭";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // FrmCtrLocAndSize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 216);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.NumUdY);
            this.Controls.Add(this.NumUdWidth);
            this.Controls.Add(this.NumUdHeight);
            this.Controls.Add(this.NumUdX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCtrLocAndSize";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "位置和大小设置";
            this.Load += new System.EventHandler(this.FrmCtrLocAndSize_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumUdX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUdY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NumUdX;
        private System.Windows.Forms.NumericUpDown NumUdHeight;
        private System.Windows.Forms.NumericUpDown NumUdWidth;
        private System.Windows.Forms.NumericUpDown NumUdY;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btClose;
    }
}