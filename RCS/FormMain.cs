﻿
using Kengic.RCS.Models.Table;
using RCS.Charts;
using RCS.Common;
using RCS.DeviceForm;
using RCS.DeviceSupportClass;
using S7.Net;
using S7Comm;
using SqlSugar;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformControlLibraryExtension;

namespace RCS
{
    public partial class FormMain : Form
    {

        #region 自定义变量
        /// <summary>
        /// 日志处理
        /// </summary>
        //public LogHandling logHandling;
        /// <summary>
        /// 初始化字典
        /// </summary>
        public bool dicInited = false;
        /// <summary>
        /// 连接PLC
        /// </summary>
        public bool plcIsOK = false;
        /// <summary>
        /// 连接PLC时创建该类，PLC主站
        /// </summary>
        //public PlcSite plcSiteMS;
        /// <summary>
        /// 连接PLC时创建该类，PLC从站
        /// </summary>
        //public PlcSite plcSiteCS;
        /// <summary>
        /// 条码扫描类
        /// </summary>
        //public ReadBarCodeFromSP readBarCodeFromSP;
        /// <summary>
        /// 创建画布
        /// </summary>
        public Graphics g;
        /// <summary>
        /// 创建显示画布
        /// </summary>
        public Graphics gDisplay;
        /// <summary>
        /// 创建缓存画布
        /// </summary>
        public Graphics gBuffer;
        /// <summary>
        /// 创建缓存图片
        /// </summary>
        public Bitmap bBuffer;

        /// <summary>
        /// 提升机框线图片
        /// </summary>
        public Image IMAGE;

        AreaControl ac;

        public struct AreaControl {
            /// <summary>
            /// TabPage 实例
            /// </summary>
            public TabPage tabPage;

            public Panel panelMainRcs;

            public Panel panel1;
            public Panel panel2;
            public Panel panel3;

            /// <summary>
            /// ListView 列表 系统列表
            /// </summary>
            public ListView lvSystemList;
            /// <summary>
            /// ListView 列表 任务列表
            /// </summary>
            public ListView lvTaskList;
        }
        //显示库区元素的布局父页面
        public Dictionary<string, AreaControl> displayAreaElement;
        /// <summary>
        /// 区域空置声明变量
        /// </summary>
        
        #region 字典变量
        /// <summary>
        /// 设备功能控制字典
        /// </summary>
        //public Dictionary<string, DeviceControl> deviceControlDic = new Dictionary<string, DeviceControl>();
        /// <summary>
        /// PLC站状态字典
        /// </summary>
        /*public Dictionary<string, PlcSiteStatus> plcSiteStatusDic = new Dictionary<string, PlcSiteStatus>();*/
        /// <summary>
        /// LED地址字典
        /// </summary>
        public Dictionary<string, string> ledIpAddrDic = new Dictionary<string, string>(); //LED的IP地址
        /// <summary>
        /// plc站字典
        /// </summary>
        //public Dictionary<string, PlcSite> plcSites = new Dictionary<string, PlcSite>();
        /// <summary>
        /// 库区字典
        /// </summary>
        //public Dictionary<string, PlcKQ> plcKQs = new Dictionary<string, PlcKQ>();
        /// <summary>
        /// 设备接口地址字典
        /// </summary>
        public Dictionary<string, string> deviceIntf = new Dictionary<string, string>();
        /// <summary>
        /// 扫码器
        /// </summary>
        public Dictionary<string, string> ScanIpAddrDic = new Dictionary<string, string>(); //扫码器的IP地址
        /// <summary>
        /// 输送设备
        /// </summary>
        public Dictionary<string, Conveyer> conveyers = new Dictionary<string, Conveyer>();
        /// <summary>
        /// 播种墙货格
        /// </summary>
        public Dictionary<string, SeedingWallLocation> seedingWalls = new Dictionary<string, SeedingWallLocation>();
        /// <summary>
        /// 入库提升机设备
        /// </summary>
        //public Dictionary<string, InputLifter> inputLifters = new Dictionary<string, InputLifter>();
        /// <summary>
        /// 入库站台
        /// </summary>
        //public Dictionary<string, InputStation> inputStations = new Dictionary<string, InputStation>();
        /// <summary>
        /// 出库站台
        /// </summary>
        //public Dictionary<string, OutputStation> outputStations = new Dictionary<string, OutputStation>();
        /// <summary>
        /// 出库提升机设备
        /// </summary>
        //public Dictionary<string, OutputLifter> outputLifters = new Dictionary<string, OutputLifter>();
        /// <summary>
        /// 多层穿梭车设备
        /// </summary>
        public Dictionary<string, Image> seedingWallShuttleImage = new Dictionary<string, Image>();
        /// <summary>
        /// 站台显示图片
        /// </summary>
        public Dictionary<string, Image> seedingWallImage = new Dictionary<string, Image>();
        /// <summary>
        /// 提升机图片显示
        /// </summary>
        public Dictionary<string, Image> LifterPic = new Dictionary<string, Image>();
        /// <summary>
        /// 穿梭车显示位置
        /// </summary>
        //public Dictionary<string, CSCPos> shuttlePos = new Dictionary<string, CSCPos>();
        /// <summary>
        /// 换层提升机
        /// </summary>
        public Dictionary<string, Image> hcLifterPic = new Dictionary<string, Image>();
        /// <summary>
        /// 换层提升机
        /// </summary>
        //public Dictionary<string, ChangeLayerLifter> hcLifters = new Dictionary<string, ChangeLayerLifter>();
        /// <summary>
        /// 维修平台
        /// </summary>
        //public Dictionary<string, RepairStation> repairStations = new Dictionary<string, RepairStation>();
        /// <summary>
        /// 正在运行的线程数量
        /// </summary>
        private int threadNum = 0;
        /// <summary>
        /// 设备是否初始化
        /// </summary>
        public bool deviceInited = false;
        /// <summary>
        /// 运行模式，来控制版本
        /// </summary>
        public string runMode = string.Empty;
        /// <summary>
        /// 工作模式，是否联机0脱机工作，1联机工作
        /// </summary>
        public int workMode = 0;
        /// <summary>
        /// 任务模式，0入库模式，1出库模式
        /// </summary>
        public int taskMode = 0;
        /// <summary>
        /// 界面刷新线程
        /// </summary>
        public Thread autoPicThread;
        /// <summary>
        /// 是否自动刷新
        /// </summary>
        public static bool IsAuto = false;
        /// <summary>
        /// 当前库区
        /// </summary>
        private string currentArea;
        /// <summary>
        /// 库区名称
        /// </summary>
        private string[] rcsAreaName;
        
        //布局画图用
        public struct PaintStruct
        {
            /// <summary>
            /// 创建显示画布
            /// </summary>
            public Graphics gDisplay;
            /// <summary>
            /// 创建缓存画布
            /// </summary>
            public Graphics gBuffer;
            /// <summary>
            /// 创建缓存图片
            /// </summary>
            public Bitmap bBuffer;
        }
        public Dictionary<string, PaintStruct> paintDic = new Dictionary<string, PaintStruct>();

        /// <summary>
        /// 播种墙图片显示
        /// </summary>
        public Dictionary<string, Image> dicSeedingWallPics = new Dictionary<string, Image>();
        #endregion

        #endregion

        #region 参数
        //数据库连接
        private ISqlSugarClient sqlSugarClient;
        //PLC访问类
        public Plc plc;
        public readonly SiemensS7 s7Plc = new SiemensS7();
        //PLC Ip
        public string plcIp = ConfigurationManager.AppSettings["PlcIp"];
        #endregion

        public FormMain()
        {
            InitializeComponent();
            IsAuto = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //将当前库区赋值
            this.tabControlExt1.SelectedIndex = 0; //默认第一个页面
            //currentArea = this.tabControlExt1.SelectedTab.Tag.ToString();
            this.btn_exit_system.Enabled = false;
            #region  绘制总体布局

            #region 设定主窗体长宽
            this.Width = 1440;
            this.Height = 900;
            #endregion
          
            #region Panel Top
            //初始化 panel 值
            this.panl_top.Width = this.Width - 25;
            this.panl_top.Height = 50;
            this.panl_top.Location = new Point(2, 1);
            #endregion

            #region Panel Main  tabControlExt1
            //初始化 panel 值
            this.panl_main.Width = this.Width - 25;
            //this.panl_main.Height = this.Height - this.panl_top.Height - 330;
            this.panl_main.Height = this.Height - this.panl_top.Height;
            this.panl_main.Location = new Point(2, 53);

            this.tabControlExt1.Width = this.panl_main.Width;
            this.tabControlExt1.Height = this.panl_main.Height;
            this.tabControlExt1.Location = new Point(1, 1);
            #endregion

            #endregion

            
            // 开启线程自动刷新
            autoPicThread = new Thread(new ThreadStart(AutoRefreshView));
            autoPicThread.IsBackground = true;
            /*if (!autoPicThread.IsAlive)
                autoPicThread.Start();*/
        }

        #region 初始化界面
        private void InitView()
        {
            //查询系统设置切状态允许显示的
            var rcsAreaNames = sqlSugarClient.Queryable<table_rcs_system_area_set>().Where(r => r.AREA_SHOW_STATUS == "1").ToList();
            if (rcsAreaNames == null || !rcsAreaNames.Any())
            {
                MessageBox.Show("配置文件中监控界面配置非法,未能检测到数据！!");
                Environment.Exit(0);
            }
            #region 定义画布
            displayAreaElement = new Dictionary<string, AreaControl>();
            var i = 0;
            foreach (var rcsAreas in rcsAreaNames)
            {
                ac = new AreaControl();
                
                //开始循环
                ac.tabPage = new TabPage();
                //初始化panel
                ac.panelMainRcs = new Panel();
                //系统事件Panel
                ac.panel1 = new Panel();
                //任务事件Panel
                ac.panel2 = new Panel();
                //控制Panel
                ac.panel3 = new Panel();
                //系统事件列表
                ac.lvSystemList = new ListView();
                //任务事件列表
                ac.lvTaskList = new ListView();

                if (rcsAreas.AREA_NO.Equals("SeedingWallArea"))
                {
                    ac.panelMainRcs.BackgroundImage = Image.FromFile(Application.StartupPath + @"\SeedingWallImage\background.PNG");
                }
                ac.panelMainRcs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                ac.tabPage.ImageIndex = 0;
                ac.tabPage.Location = new System.Drawing.Point(4, 26);
                ac.tabPage.Name = rcsAreas.AREA_NO;

                ac.tabPage.Padding = new System.Windows.Forms.Padding(3);
                ac.tabPage.Size = new System.Drawing.Size(1202, this.tabControlExt1.Height);
                ac.tabPage.TabIndex = i;
                ac.tabPage.Tag = rcsAreas.AREA_NO;
                ac.tabPage.Text = rcsAreas.AREA_NAME;
                ac.tabPage.UseVisualStyleBackColor = true;
                //添加TabPage
                this.panl_main.Controls.Add(this.tabControlExt1);
                this.tabControlExt1.TabPages.Add(ac.tabPage);


                #region TabPage 里面的 panMain
                ac.panelMainRcs.BackColor = System.Drawing.Color.White;
                ac.panelMainRcs.Name = "system" + i;
                ac.panelMainRcs.TabIndex = 0;

                ac.panelMainRcs.Width = ac.tabPage.Width -10;
                ac.panelMainRcs.Height = ac.tabPage.Height - 350;
                ac.panelMainRcs.Location = new Point(ac.tabPage.Location.X+1, ac.tabPage.Location.Y);
                ac.panelMainRcs.Paint += new System.Windows.Forms.PaintEventHandler(this.pan_mainRcs_Paint);
                ac.panelMainRcs.Dock = DockStyle.Top;
                ac.tabPage.Controls.Add(ac.panelMainRcs);
                //ac.tabPage.Controls.Add(this.statusStripExt1);
                #endregion

                #region Panel Buttom 1 2 3

                this.panl_main.Name = rcsAreas.AREA_NO;
                
                ac.panel1.BackColor = System.Drawing.Color.White;
                ac.panel1.Name = "system" + rcsAreas.AREA_NO;
                ac.panel1.TabIndex = 1;
                
                ac.panel2.BackColor = System.Drawing.Color.White;
                ac.panel2.Name = "task" + rcsAreas.AREA_NO;
                ac.panel2.TabIndex = 2;
                
                ac.panel3.BackColor = System.Drawing.Color.White;
                ac.panel3.Name = "control" + rcsAreas.AREA_NO;
                ac.panel3.TabIndex = 3;

                #region 系统指令Panel
                //this.panl_bottom1.Visible = true;
                //初始化 panel 值
                ac.panel1.Width = ac.panelMainRcs.Width / 3 - 9;
                ac.panel1.Height = 251;
                ac.panel1.Location = new Point(ac.panelMainRcs.Location.X, ac.panelMainRcs.Location.Y + ac.panelMainRcs.Height + 10);
                ac.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panl_bottom1_Paint);
                ac.tabPage.Controls.Add(ac.panel1);

                Label lbl1 = new Label();
                lbl1.AutoSize = true;
                ac.panel1.Controls.Add(lbl1);
                lbl1.Text = "系统指令";
                lbl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
                lbl1.Font = new Font("宋体", 16, FontStyle.Bold);
                lbl1.TextAlign = ContentAlignment.MiddleCenter;
                lbl1.BackColor = Color.White;
                lbl1.Location = new Point((ac.panel1.Width / 2 - lbl1.Width / 2), 10);
                lbl1.BringToFront();
                lbl1.SendToBack();
                #region 系统指令列表
                
                ac.panel1.Controls.Add(ac.lvSystemList);
                ac.lvSystemList.View = View.Details;
                ac.lvSystemList.Size = new Size(ac.panel1.Width - 30, ac.panel1.Height - lbl1.Location.Y - lbl1.Height - 15);
                ac.lvSystemList.Location = new Point(20, lbl1.Bottom + 5);
                ac.lvSystemList.BorderStyle = BorderStyle.None;
                ac.lvSystemList.Columns.Add("时间", (int)(ac.panel1.Width * 0.2), HorizontalAlignment.Center);
                ac.lvSystemList.Columns.Add("系统指令", (int)(ac.panel1.Width * 0.8), HorizontalAlignment.Left);
                #endregion
                #endregion

                #region 任务指令Panel
                //this.panl_bottom2.Visible = true;
                ac.panel2.Width = ac.panel1.Width;
                ac.panel2.Height = ac.panel1.Height;
                ac.panel2.Location = new Point(19 + ac.panel1.Width, ac.panelMainRcs.Location.Y + ac.panelMainRcs.Height + 10);
                ac.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panl_bottom2_Paint);
                ac.tabPage.Controls.Add(ac.panel2);

                Label lbl2 = new Label();
                lbl2.AutoSize = true;
                ac.panel2.Controls.Add(lbl2);
                lbl2.Text = "任务指令";
                lbl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
                lbl2.Font = new Font("宋体", 16, FontStyle.Bold);
                lbl2.TextAlign = ContentAlignment.MiddleCenter;
                lbl2.BackColor = Color.White;
                lbl2.Location = new Point((ac.panel2.Width / 2 - lbl1.Width / 2), 10);
                lbl2.BringToFront();
                lbl2.SendToBack();

                #region 任务指令列表
                
                ac.panel2.Controls.Add(ac.lvTaskList);
                ac.lvTaskList.View = View.Details;
                ac.lvTaskList.Size = new Size(ac.panel2.Width - 30, ac.panel1.Height - lbl2.Location.Y - lbl2.Height - 15);
                ac.lvTaskList.Location = new Point(20, lbl2.Bottom + 5);
                ac.lvTaskList.BorderStyle = BorderStyle.None;
                ac.lvTaskList.Columns.Add("时间", (int)(ac.panel2.Width * 0.2), HorizontalAlignment.Center);
                ac.lvTaskList.Columns.Add("任务指令", (int)(ac.panel2.Width * 0.8), HorizontalAlignment.Left);
                #endregion
                #endregion

                #region 控制指令Panel
                //this.panl_bottom3.Visible = true;
                ac.panel3.Width = ac.panel2.Width;
                ac.panel3.Height = ac.panel2.Height;
                ac.panel3.Location = new Point(15 + ac.panel2.Location.X + ac.panel2.Width, ac.panelMainRcs.Location.Y + ac.panelMainRcs.Height + 10);
                ac.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panl_bottom3_Paint);
                ac.tabPage.Controls.Add(ac.panel3);

                Label lbl3 = new Label();
                lbl3.AutoSize = true;
                ac.panel3.Controls.Add(lbl3);
                lbl3.Text = "控制指令";
                lbl3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
                lbl3.Font = new Font("宋体", 16, FontStyle.Bold);
                lbl3.TextAlign = ContentAlignment.MiddleCenter;
                lbl3.BackColor = Color.White;
                lbl3.Location = new Point((ac.panel3.Width / 2 - lbl3.Width / 2), 10);
                lbl3.BringToFront();
                lbl3.SendToBack();
                #endregion

                #endregion

                PaintStruct ps = new PaintStruct();
                ps.bBuffer = new Bitmap(ac.panelMainRcs.Width, ac.panelMainRcs.Height);
                ps.gBuffer = Graphics.FromImage(ps.bBuffer);
                ps.gDisplay = ac.panelMainRcs.CreateGraphics();
                paintDic.Add(rcsAreas.AREA_NO, ps);

                displayAreaElement.Add(rcsAreas.AREA_NO, ac);

                i++;
            }
            
            #endregion

        }
        #endregion

        #region 链接数据库
        private bool ConnectDB()
        {
            //看一下数据库是否正常
            try
            {
                using (sqlSugarClient.Ado.OpenAlways())
                {
                    return true;
                }
            }
            catch (Exception)
            {
                //return false;
                sqlSugarClient = new SqlSugarScope(new ConnectionConfig()
                {
                    ConnectionString = ConfigurationManager.AppSettings["LocalConnectionString"],
                    DbType = SqlSugar.DbType.SqlServer,
                    InitKeyType = InitKeyType.Attribute,
                    IsAutoCloseConnection = true  //是否自动关闭连接  Yes
                });
            }
            //实例化数据库访问类  
            //baseDAL = new BaseDAL(sqlSugarClient);
            return true;
        }
        #endregion

        #region 初始化字典
        private bool InitDeviceDictionary()
        {
            return false;
        }
        #endregion

        #region 初始化 PLC
        private bool InitPLC()
        {

            var connectFlag = s7Plc.Connect("172.16.120.20", 0, 1);
            if (!connectFlag)
            {
                return false;
            }
            return true;
            /*//初始化PLC 连接
            try
            {
                plc = new Plc(CpuType.S71200, plcIp, 0, 1);
                //判断PLC是否可用
                if (!plc.IsAvailable || !plc.IsConnected)
                {
                    plc.Close();
                    return false;
                }
                else
                {
                    plc.Open();
                    //var AAAA = plc.Read("DB67.DBD0.5");
                    return true;
                }
            }
            catch (PlcException ex)
            {
                plc.Close();
                return false;
            }*/

        }
        #endregion

        #region 初始化设备
        private bool InitDevice()
        {
            Control control = GetControlOfName("systemSeedingWallArea", ac.panel1.Controls);

            //IBaseBLL baseBLL = new BaseBLL(baseDAL);

            //DataSet ds = new DataSet();

            //查询设备数据
            try
            {
                var deviceList = sqlSugarClient.Queryable<table_rcs_base_device>().Where(r => r.DEVICE_STATUS == 0 && (r.AREA_NO == "MultiArea" || r.AREA_NO == "SeedingWallArea")).OrderBy(r => r.DEVICE_ID).ToList();
                if (deviceList.Any() && deviceList.Count > 0)
                {
                    //遍历循环设备数据
                    foreach (var deviceBase in deviceList)
                    {
                        switch (deviceBase.DEVICE_TYPE)
                        {
                            //输送
                            case "Coneyer":
                                //Conveyer conveyer = new Conveyer(this, deviceBase,plc);
                                Conveyer conveyer = new Conveyer(this, deviceBase, s7Plc);
                                conveyers.Add(conveyer.deviceId, conveyer);//将设备添加到设备字典中
                                displayAreaElement[deviceBase.AREA_NO].panelMainRcs.Controls.Add(conveyer.pic);
                                conveyer.pic.BringToFront();
                                break;
                            //IN提升机
                            case "InHoist":
                                break;
                            //出提升机
                            case "OutHoist":
                                break;
                            //IN动力站台
                            case "InStation":
                                break;
                            //出动力站台
                            case "OutStation":
                                break;
                            //换层提升机
                            case "ChangeHoist":
                                break;
                            //穿梭车
                            case "MulitCar":
                                break;
                            //播种墙货位
                            case "Location":
                                SeedingWallLocation seedingWall = new SeedingWallLocation(this, deviceBase, s7Plc);
                                seedingWalls.Add(seedingWall.deviceId, seedingWall);
                                displayAreaElement[deviceBase.AREA_NO].panelMainRcs.Controls.Add(seedingWall.pic1);
                                displayAreaElement[deviceBase.AREA_NO].panelMainRcs.Controls.Add(seedingWall.pic2);
                                this.tabControlExt1.TabPages[deviceBase.AREA_NO].Controls.Add(seedingWall.pic1);
                                this.tabControlExt1.TabPages[deviceBase.AREA_NO].Controls.Add(seedingWall.pic2);
                                seedingWall.pic2.BringToFront();
                                seedingWall.pic1.BringToFront();

                                break;
                            default:
                                break;
                        }
                    }
                }
                deviceInited = true;
            }
            catch (Exception ex)
            {
                deviceInited = false;
            }
            
            return deviceInited;
        }
        #endregion

        #region 加载图片 InitImg()
        /// <summary>
        /// 加载图片
        /// </summary>
        public void InitImg()
        {
            #region 播种墙小车
            seedingWallImage.Add("正常无货", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\SeedingWallShuttle.PNG"));
            seedingWallImage.Add("正常有货", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\SeedingWallShuttleGoods.PNG"));
            seedingWallImage.Add("报警无货", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\SeedingWallShuttleNoError.PNG"));
            seedingWallImage.Add("报警有货", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\SeedingWallShuttleYesError.PNG"));
            seedingWallImage.Add("格口正常1", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wall11.PNG"));
            seedingWallImage.Add("格口正常2", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wall22.PNG"));
            seedingWallImage.Add("格口异常1", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wallRedError1.PNG"));
            seedingWallImage.Add("格口异常2", Image.FromFile(Application.StartupPath + @"\SeedingWallImage\wallRedError2.PNG"));
            #endregion


            #region 多穿车
           /* //站台A
            dicStationsPics.Add("正常无货,A", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_normal_nogoods.PNG"));
            dicStationsPics.Add("正常有货,A", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_normal_goods.PNG"));
            dicStationsPics.Add("报错,A", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_warning_nogoods.PNG"));
            dicStationsPics.Add("有货报警,A", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_warning_goods.PNG"));
            //站台B
            dicStationsPics.Add("正常无货,B", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_normal_nogoods.PNG"));
            dicStationsPics.Add("正常有货,B", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_normal_goods.PNG"));
            dicStationsPics.Add("报错,B", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_warning_nogoods.PNG"));
            dicStationsPics.Add("有货报警,B", Image.FromFile(Application.StartupPath + @"\Assets\TunnelBackImg\station_warning_goods.PNG"));*/

            #endregion


        }
        #endregion

        #region 播种墙框线绘制
        /// <summary>
        /// 提升机框线绘制
        /// </summary>
        private void Rail_Paint()
        {
            if (currentArea == "SeedingWallArea")
            {
                //93 相差
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 200, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 293, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 386, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 479, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 572, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 665, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 758, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 851, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 944, 20);
                paintDic[currentArea].gBuffer.DrawImageUnscaled(IMAGE, 1037, 20);

                paintDic[currentArea].gDisplay.DrawImage(paintDic[currentArea].bBuffer, 0, 0);
            }

            //gBuffer.DrawImageUnscaled(IMAGE, 390, 2);
            //gBuffer.DrawImageUnscaled(IMAGE, 510, 2);

        }

        #endregion

        #region 自动刷新视图
        private void AutoRefreshView()
        {
            while (IsAuto)
            {
                #region 将播种墙线框加载出来
                //Rail_Paint();
                
                #endregion
                Thread.Sleep(1000);
            }
        }
        #endregion

        #region tabControl重写

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            #region 绘制所有Tab选项

            SolidBrush back_normal_sb = new SolidBrush(this.tabControlExt1.TabBackNormalColor);
            SolidBrush back_selected_sb = new SolidBrush(this.tabControlExt1.TabBackSelectedColor);
            SolidBrush text_normal_sb = new SolidBrush(this.tabControlExt1.TabTextNormalColor);
            SolidBrush text_selected_sb = new SolidBrush(this.tabControlExt1.TabTextSelectedColor);
            StringFormat text_sf = new StringFormat() { Alignment = this.tabControlExt1.TabTextAlignment, LineAlignment = StringAlignment.Center, Trimming = StringTrimming.EllipsisCharacter };
            Pen close_pen = new Pen(this.tabControlExt1.TabCloseBackColor, 2) { StartCap = LineCap.Round, EndCap = LineCap.Round };

            Rectangle tab_rect = this.GetTabRectangle();
            Region client_region = null;
            Region tabitem_region = null;
            if (this.tabControlExt1.Alignment == TabAlignment.Top || this.tabControlExt1.Alignment == TabAlignment.Bottom)
            {
                client_region = g.Clip.Clone();
                tabitem_region = new Region(tab_rect);
                g.Clip = tabitem_region;

            }
            for (int i = 0; i < this.tabControlExt1.TabCount; i++)
            {
                Rectangle rect = this.tabControlExt1.GetTabRect(i);
                GraphicsPath path = ControlCommom.TransformCircular(rect, this.tabControlExt1.TabRadiusLeftTop, this.tabControlExt1.TabRadiusRightTop, this.tabControlExt1.TabRadiusRightBottom, this.tabControlExt1.TabRadiusLeftBottom);

                #region  绘制Tab选项背景颜色
                g.FillPath((i == this.tabControlExt1.SelectedIndex) ? back_selected_sb : back_normal_sb, path);
                #endregion

                #region 绘制Tab选项图片
                if (this.tabControlExt1.ImageList != null && this.tabControlExt1.ImageList.Images.Count > 0)
                {
                    Image img = null;
                    if (this.tabControlExt1.TabPages[i].ImageIndex > -1)
                    {
                        img = this.tabControlExt1.ImageList.Images[this.tabControlExt1.TabPages[i].ImageIndex];
                    }
                    else if (this.tabControlExt1.TabPages[i].ImageKey.Trim().Length > 0)
                    {
                        img = this.tabControlExt1.ImageList.Images[this.tabControlExt1.TabPages[i].ImageKey];
                    }
                    if (img != null)
                    {
                        g.DrawImage(img, rect.X + this.tabControlExt1.tabImageMarginLeft, rect.Y + (rect.Height - this.TabImageSize.Height) / 2, this.TabImageSize.Width, this.TabImageSize.Height);
                    }
                }
                #endregion

                #region 绘制Tab选项文本
                if (this.tabControlExt1.ImageList != null && ((this.tabControlExt1.TabPages[i].ImageIndex > -1) || (this.tabControlExt1.TabPages[i].ImageKey.Trim().Length > 0)))
                {
                    rect = new Rectangle(rect.Left + this.TabImageSize.Width + this.tabControlExt1.tabImageMarginLeft * 2, rect.Top, rect.Width - this.TabImageSize.Width - this.tabControlExt1.tabImageMarginLeft * 2, rect.Height);
                }

                if (this.tabControlExt1.TextVertical)
                {
                    string text = this.tabControlExt1.TabPages[i].Text;
                    float sum = 0;
                    SizeF text_size = g.MeasureString(text, this.Font, new PointF(), text_sf);
                    for (int j = 0; j < text.Length; j++)
                    {
                        RectangleF char_rect = new RectangleF(this.tabControlExt1.Padding.X + rect.X, this.tabControlExt1.Padding.Y + rect.Y + sum, text_size.Width, text_size.Height + 1);
                        g.DrawString(text.Substring(j, 1), this.Font, (i == this.tabControlExt1.SelectedIndex) ? text_selected_sb : text_normal_sb, char_rect, text_sf);
                        sum += text_size.Height + 1;
                    }
                }
                else
                {
                    g.DrawString(this.tabControlExt1.TabPages[i].Text, this.Font, (i == this.tabControlExt1.SelectedIndex) ? text_selected_sb : text_normal_sb, rect, text_sf);
                }
                #endregion

                #region 绘制关闭按钮
                if (this.tabControlExt1.TabCloseShow && this.GetIsShowCloseButton(i))
                {
                    RectangleF close_rect = this.GetTabCloseRectangle(i);
                    g.DrawLine(close_pen, new PointF(close_rect.X, close_rect.Y), new PointF(close_rect.Right, close_rect.Bottom));
                    g.DrawLine(close_pen, new PointF(close_rect.Right, close_rect.Y), new PointF(close_rect.Left, close_rect.Bottom));
                }
                #endregion
            }

            if (tabitem_region != null)
            {
                g.Clip = client_region;
                tabitem_region.Dispose();
            }

            if (back_normal_sb != null)
                back_normal_sb.Dispose();
            if (back_selected_sb != null)
                back_selected_sb.Dispose();
            if (text_normal_sb != null)
                text_normal_sb.Dispose();
            if (text_selected_sb != null)
                text_selected_sb.Dispose();
            if (text_sf != null)
                text_sf.Dispose();
            if (close_pen != null)
                close_pen.Dispose();
            #endregion

            #region 设置TabPage内容页边框色
            if (this.tabControlExt1.TabCount > 0)
            {
                Pen border_pen = new Pen(this.tabControlExt1.TabPageBorderColor, 1);
                Rectangle borderRect = this.tabControlExt1.TabPages[0].Bounds;
                borderRect.Inflate(1, 1);
                g.DrawRectangle(border_pen, borderRect);
                border_pen.Dispose();
            }
            #endregion
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (!this.tabControlExt1.DesignMode)
            {
                if (this.tabControlExt1.TabCloseShow && e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    #region 关闭
                    Rectangle tab_rect = this.GetTabRectangle();
                    if (tab_rect.Contains(e.Location))
                    {
                        for (int i = 0; i < this.tabControlExt1.TabPages.Count; i++)
                        {
                            if (this.GetTabCloseRectangle(i).Contains(e.Location) && this.GetIsShowCloseButton(i))
                            {
                                int index = 0;
                                if (i >= this.tabControlExt1.TabPages.Count - 1)
                                {
                                    index = i - 1;
                                    if (i < 0)
                                    {
                                        i = 0;
                                    }
                                }
                                else
                                {
                                    index = i + 1;
                                }
                                this.tabControlExt1.SelectedIndex = index;
                                this.tabControlExt1.TabPages.Remove(this.tabControlExt1.TabPages[i]);
                                return;
                            }
                        }
                    }
                    #endregion
                }
            }

            base.OnMouseClick(e);
        }

        #region Tab选项

        private Color tabBackNormalColor = Color.FromArgb(153, 204, 153);
        /// <summary>
        /// Tab选项背景颜色(正常)
        /// </summary>
        [DefaultValue(typeof(Color), "153, 204, 153")]
        [Description("Tab选项背景颜色(正常)")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color TabBackNormalColor
        {
            get { return this.tabControlExt1.tabBackNormalColor; }
            set
            {
                if (this.tabControlExt1.tabBackNormalColor == value)
                    return;
                this.tabControlExt1.tabBackNormalColor = value;
                base.Invalidate();
            }
        }

        private Color tabBackSelectedColor = Color.FromArgb(201, 153, 204, 153);
        /// <summary>
        /// Tab选项背景颜色(选中)
        /// </summary>
        [DefaultValue(typeof(Color), "201, 153, 204, 153")]
        [Description("Tab选项背景颜色(选中)")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color TabBackSelectedColor
        {
            get { return this.tabControlExt1.tabBackSelectedColor; }
            set
            {
                if (this.tabControlExt1.tabBackSelectedColor == value)
                    return;
                this.tabControlExt1.tabBackSelectedColor = value;
                base.Invalidate();
            }
        }

        private Color tabTextNormalColor = Color.FromArgb(255, 255, 255);
        /// <summary>
        /// Tab选项文本颜色(正常)
        /// </summary>
        [DefaultValue(typeof(Color), "255, 255, 255")]
        [Description("Tab选项文本颜色(正常)")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color TabTextNormalColor
        {
            get { return this.tabControlExt1.tabTextNormalColor; }
            set
            {
                if (this.tabControlExt1.tabTextNormalColor == value)
                    return;
                this.tabControlExt1.tabTextNormalColor = value;
                base.Invalidate();
            }
        }

        private Color tabTextSelectedColor = Color.FromArgb(255, 255, 255);
        /// <summary>
        /// Tab选项文本颜色(选中)
        /// </summary>
        [DefaultValue(typeof(Color), "255, 255, 255")]
        [Description("Tab选项文本颜色(选中)")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color TabTextSelectedColor
        {
            get { return this.tabControlExt1.tabTextSelectedColor; }
            set
            {
                if (this.tabControlExt1.tabTextSelectedColor == value)
                    return;
                this.tabControlExt1.tabTextSelectedColor = value;
                base.Invalidate();
            }
        }

        private StringAlignment tabTextAlignment = StringAlignment.Near;
        /// <summary>
        /// Tab选项文本对齐方式
        /// </summary>
        [DefaultValue(StringAlignment.Near)]
        [Description("Tab选项文本对齐方式")]
        public StringAlignment TabTextAlignment
        {
            get { return this.tabControlExt1.tabTextAlignment; }
            set
            {
                if (this.tabControlExt1.tabTextAlignment == value)
                    return;
                this.tabControlExt1.tabTextAlignment = value;
                base.Invalidate();
            }
        }

        private bool textVertical = false;
        /// <summary>
        /// Tab选项文本是否垂直
        /// </summary>
        [Description("Tab选项文本是否垂直")]
        [DefaultValue(false)]
        public bool TextVertical
        {
            get { return this.tabControlExt1.textVertical; }
            set
            {
                if (this.tabControlExt1.textVertical == value)
                    return;
                this.tabControlExt1.textVertical = value;
                this.Invalidate();
            }
        }

        private Size tabImageSize = new Size(16, 16);
        /// <summary>
        /// tab选项图标大小
        /// </summary>
        [Description("左下角圆角")]
        [DefaultValue(typeof(Size), "16,16")]
        public Size TabImageSize
        {
            get { return this.tabControlExt1.tabImageSize; }
            set
            {
                if (this.tabControlExt1.tabImageSize == value)
                    return;
                this.tabControlExt1.tabImageSize = value;
                this.Invalidate();
            }
        }

        #endregion

        #region  关闭

        private bool tabCloseShow = false;
        /// <summary>
        /// Tab选项关闭按钮是否显示
        /// </summary>
        [Description("Tab选项关闭按钮是否显示")]
        [DefaultValue(false)]
        public bool TabCloseShow
        {
            get { return this.tabControlExt1.tabCloseShow; }
            set
            {
                if (this.tabControlExt1.tabCloseShow == value)
                    return;
                this.tabControlExt1.tabCloseShow = value;
                this.Invalidate();
            }
        }

        private Size tabCloseSize = new Size(10, 10);
        /// <summary>
        /// Tab关闭按钮Size
        /// </summary>
        [DefaultValue(typeof(Size), "10, 10")]
        [Description("Tab关闭按钮Size")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Size TabCloseSize
        {
            get { return this.tabControlExt1.tabCloseSize; }
            set
            {
                if (this.tabControlExt1.tabCloseSize == value)
                    return;
                this.tabControlExt1.tabCloseSize = value;
                base.Invalidate();
            }
        }
        private Color tabCloseBackColor = Color.FromArgb(255, 255, 255);
        /// <summary>
        /// Tab关闭按钮背景颜色
        /// </summary>
        [DefaultValue(typeof(Color), "255, 255, 255")]
        [Description("Tab关闭按钮背景颜色")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color TabCloseBackColor
        {
            get { return tabControlExt1.tabCloseBackColor; }
            set
            {
                if (this.tabControlExt1.tabCloseBackColor == value)
                    return;
                this.tabControlExt1.tabCloseBackColor = value;
                base.Invalidate();
            }
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 获取Tab选项区Rectangle
        /// </summary>
        /// <returns></returns>
        private Rectangle GetTabRectangle()
        {
            int tabitem_width = this.tabControlExt1.TabPages.Count * this.tabControlExt1.ItemSize.Width;
            if (this.tabControlExt1.Alignment == TabAlignment.Top || this.tabControlExt1.Alignment == TabAlignment.Bottom)
            {
                if (tabitem_width > this.tabControlExt1.ClientRectangle.Width - this.tabControlExt1.pd * 2)
                {
                    tabitem_width = this.tabControlExt1.ClientRectangle.Width - this.tabControlExt1.preNextBtnWidth - this.tabControlExt1.pd * 2;
                }
            }
            int y = 0;
            if (this.tabControlExt1.Alignment == TabAlignment.Top)
                y = this.tabControlExt1.ClientRectangle.Y + this.tabControlExt1.pd;
            else if (this.tabControlExt1.Alignment == TabAlignment.Bottom)
                y = this.tabControlExt1.ClientRectangle.Bottom - this.tabControlExt1.pd - this.tabControlExt1.ItemSize.Height;

            return new Rectangle(this.ClientRectangle.X + this.tabControlExt1.pd, y, tabitem_width, this.tabControlExt1.ItemSize.Height + this.tabControlExt1.pd);
        }

        /// <summary>
        /// 获取Tab选项关闭按钮Rectangle
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private RectangleF GetTabCloseRectangle(int index)
        {
            Rectangle rect = this.tabControlExt1.GetTabRect(index);
            RectangleF close_rect = new RectangleF(rect.Right - 10 - this.tabControlExt1.TabCloseSize.Width, rect.Y + (rect.Height - this.tabControlExt1.TabCloseSize.Height) / 2f, this.tabControlExt1.TabCloseSize.Width, this.tabControlExt1.TabCloseSize.Height);
            return close_rect;
        }

        /// <summary>
        /// 是否不显示关闭按钮
        /// </summary>
        /// <param name="index"></param>
        private bool GetIsShowCloseButton(int index)
        {
            if (this.tabControlExt1.TabPages[index].Tag != null && this.tabControlExt1.TabPages[index].Tag.ToString() == "不显示关闭按钮")
            {
                return false;
            }

            return true;
        }

        #endregion

        #endregion

        #region Panel TOP Bottom 1 2 3

        private void panl_top_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                 this.panl_top.ClientRectangle,
                                this.tabControlExt1.TabPageBorderColor, // 7f9db9 
                                 1,
                                ButtonBorderStyle.Solid,
                                this.tabControlExt1.TabPageBorderColor,
                                 1,
                                ButtonBorderStyle.Solid,
                                this.tabControlExt1.TabPageBorderColor,
                                 1,
                                ButtonBorderStyle.Solid,
                                this.tabControlExt1.TabPageBorderColor,
                                 1,
                                ButtonBorderStyle.Solid);
        }

        private void pan_mainRcs_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                  ac.panelMainRcs.ClientRectangle,
                                 this.tabControlExt1.TabPageBorderColor, // 7f9db9 
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid);
        }

        private void panl_bottom1_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                  ac.panel1.ClientRectangle,
                                 this.tabControlExt1.TabPageBorderColor, // 7f9db9 
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid);
        }

        private void panl_bottom2_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                  ac.panel1.ClientRectangle,
                                  this.tabControlExt1.TabPageBorderColor, // 7f9db9 
                                   1,
                                  ButtonBorderStyle.Solid,
                                  this.tabControlExt1.TabPageBorderColor,
                                   1,
                                  ButtonBorderStyle.Solid,
                                  this.tabControlExt1.TabPageBorderColor,
                                   1,
                                  ButtonBorderStyle.Solid,
                                  this.tabControlExt1.TabPageBorderColor,
                                   1,
                                  ButtonBorderStyle.Solid);
        }

        private void panl_bottom3_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
                                  ac.panel1.ClientRectangle,
                                 this.tabControlExt1.TabPageBorderColor, // 7f9db9 
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid,
                                 this.tabControlExt1.TabPageBorderColor,
                                  1,
                                 ButtonBorderStyle.Solid);
        }

        #endregion

        #region Tab页 索引变化 触发当前库区变化
        private void tabControlExt1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //currentArea = this.tabControlExt1.SelectedTab.Tag.ToString();
        /*    if (this.tabControlExt1.SelectedIndex == 0)
            {
                TextBox tbb = new TextBox();
                tbb.BorderStyle = BorderStyle.None;
                tbb.Size = new Size(500, 80);
                tbb.Location = new Point(this.tabControlExt1.TabPages[this.tabControlExt1.SelectedIndex].Width/3, this.tabControlExt1.TabPages[this.tabControlExt1.SelectedIndex].Height/2);
                tbb.Text = "这是第" + (this.tabControlExt1.SelectedIndex + 1) + "个库区";
                tbb.Font = new Font("宋体", 36F);
                this.tabControlExt1.TabPages[this.tabControlExt1.SelectedIndex].Controls.Add(tbb);
            }*/
            
        }
        #endregion

        #region 窗体关闭
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsAuto = false;
        }
        #endregion

        #region 启动系统
        private void btn_start_system_Click(object sender, EventArgs e)
        {
            #region LoadExit 加载
            MaskingExt.Show(this, new MaskingExt.MaskingSettings() { TextOrientation = MaskingExt.MaskingTextOrientations.Right });
            #endregion 
            //判断数据库连接
            var connectDBFlag = ConnectDB();
            if (!connectDBFlag)
            {
                MaskingExt.Hide(this);
                toolStripStatusLabel1.ForeColor = Color.Red;
                toolStripStatusLabel1.Text = "数据库连接失败！";
                return;
            }
            toolStripStatusLabel1.Text = "数据库连接成功！";
            //判断PLC 连接
            var plcConnectFlag = InitPLC();
            if (!plcConnectFlag)
            {
                MaskingExt.Hide(this);
                toolStripStatusLabel2.ForeColor = Color.Red;
                toolStripStatusLabel2.Text = "PLC连接失败！";
                return;
            }
            toolStripStatusLabel2.Text = "PLC连接成功！";
            //初始化界面
            InitView();
            ///初始化设备
            var initDeviceFlag = InitDevice();
            if (!initDeviceFlag)
            {
                MaskingExt.Hide(this);
                toolStripStatusLabel3.ForeColor = Color.Red;
                toolStripStatusLabel3.Text = "初始化字典失败！";
                return;
            }
            toolStripStatusLabel3.Text = "初始化字典成功!";
            //加载完毕  消失当前加载框
            MaskingExt.Hide(this);
            #region 关闭启动按钮
            this.btn_start_system.Enabled = false;
            this.btn_exit_system.Enabled = true;
            #endregion
        }
        #endregion

        #region 设置
        private void btn_set_system_Click(object sender, EventArgs e)
        {
            FormCommon fCommon = new FormCommon();
            fCommon.ShowDialog();
        }
        #endregion

        #region 退出系统
        private void btn_exit_system_Click(object sender, EventArgs e)
        {
            if (MessageBoxExt.Show(this, @"是否退出系统？", "系统提示", MessageBoxExtButtons.YesNo, MessageBoxExtIcon.Question) ==  DialogResult.No) {
                return;
            }
            KillProcess("RCS");
            this.Close();
        }
        /// <summary>
        /// 杀死进程  防止程序退出崩溃
        /// </summary>
        /// <param name="processname"></param>
        private void KillProcess(string processname)
        {
            foreach (var item in Process.GetProcessesByName(processname))
            {
                item.Kill();
            }

        }
        #endregion

        #region 显示List列表数据
        private string lastLvEventMsg = string.Empty;
        private delegate void InsertEventListCallback(string tabpageName, string text);
        /// <summary>
        /// 显示系统事件列表
        /// </summary>
        /// <param name="text">事件内容</param>
        public void InsertLvEvent(string currentArea,string text)
        {
            //var currentAreaTab = this.tabControlExt1.SelectedTab.Tag.ToString();
            if (displayAreaElement == null)
                return;
            if (!displayAreaElement.ContainsKey(currentArea))
                return;
            if (displayAreaElement[currentArea].lvSystemList.InvokeRequired)
            {
                InsertEventListCallback d = new InsertEventListCallback(InsertLvEvent);
                if (IsHandleCreated)
                {
                    this.Invoke(d, new object[] { currentArea, text });
                }
            }
            else
            {
                // 保留最新的信息，删除之前的
                if (displayAreaElement[currentArea].lvSystemList.Items.Count > 20)
                {
                    displayAreaElement[currentArea].lvSystemList.BeginUpdate();
                    displayAreaElement[currentArea].lvSystemList.Items.RemoveAt(displayAreaElement[currentArea].lvSystemList.Items.Count - 1);
                    displayAreaElement[currentArea].lvSystemList.EndUpdate();
                }
                string[] items = new string[2];
                items[0] = DateTime.Now.ToString("HH:mm:ss:ffff");
                items[1] = text;
                ListViewItem lvi = new ListViewItem(items);
                lvi.Font = new Font("宋体", 9, System.Drawing.FontStyle.Regular);
                //var abc = displayAreaElement[this.tabControlExt1.SelectedTab.Tag.ToString()];
                displayAreaElement[currentArea].lvSystemList.Items.Insert(0, lvi);
            }
        }

        private string lastLvTaskMsg = string.Empty;
        private delegate void InsertTaskListCallback(string tabpageName, string text);
        /// <summary>
        /// 显示任务事件列表
        /// </summary>
        /// <param name="text">任务内容</param>
        public void InsertLvTask(string currentArea,string text)
        {
            //var currentAreaTab = this.tabControlExt1.SelectedTab.Tag.ToString();
            if (displayAreaElement == null)
                return;
            if (!displayAreaElement.ContainsKey(currentArea))
                return;

            if (displayAreaElement[currentArea].lvTaskList.InvokeRequired)
            {
                InsertTaskListCallback d = new InsertTaskListCallback(InsertLvTask);
                if (IsHandleCreated)
                {
                    this.Invoke(d, new object[] { currentArea, text });
                }
            }
            else
            {
                // 保留最新的信息，删除之前的
                if (displayAreaElement[currentArea].lvTaskList.Items.Count > 20)
                {
                    displayAreaElement[currentArea].lvTaskList.BeginUpdate();
                    displayAreaElement[currentArea].lvTaskList.Items.RemoveAt(displayAreaElement[currentArea].lvTaskList.Items.Count - 1);
                    displayAreaElement[currentArea].lvTaskList.EndUpdate();
                }
                string[] items = new string[2];
                items[0] = DateTime.Now.ToString("HH:mm:ss:ffff");//DateTime.Now.ToString("MM-dd HH:mm:ss");
                items[1] = text;
                ListViewItem lvi = new ListViewItem(items);
                lvi.Font = new Font("宋体", 9, System.Drawing.FontStyle.Regular);
                displayAreaElement[currentArea].lvTaskList.Items.Insert(0, lvi);
            }
        }
        #endregion

        #region 清空List列表数据
        /// <summary>
        /// 清空Send List列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsSendListClear_Click(object sender, EventArgs e)
        {
            DeleteSendLvEvent();
        }
        private object deleteSendLvEventLock = new object();
        private delegate void DeleteSendLvEventCallback();
        /// <summary>
        /// 清空系统事件列表数据
        /// </summary>
        public void DeleteSendLvEvent()
        {
            lock (deleteSendLvEventLock)
            {
                var currentAreaTab = this.tabControlExt1.SelectedTab.Tag.ToString();
                if (displayAreaElement[currentAreaTab].lvSystemList.InvokeRequired)
                {
                    DeleteSendLvEventCallback d = new DeleteSendLvEventCallback(DeleteSendLvEvent);
                    this.Invoke(d, new object[] { });
                }
                else
                {
                    displayAreaElement[currentAreaTab].lvSystemList.Items.Clear();
                }
            }
        }

        /// <summary>
        /// 清空Recv List列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsRecvListClear_Click(object sender, EventArgs e)
        {
            DeleteRecvLvEvent();
        }
        private object deleteRecvLvEventLock = new object();
        private delegate void DeleteRecvLvEventCallback();
        /// <summary>
        /// 清空任务事件列表数据
        /// </summary>
        public void DeleteRecvLvEvent()
        {
            lock (deleteRecvLvEventLock)
            {
                var currentAreaTab = this.tabControlExt1.SelectedTab.Tag.ToString();
                if (displayAreaElement[currentAreaTab].lvTaskList.InvokeRequired)
                {
                    DeleteRecvLvEventCallback d = new DeleteRecvLvEventCallback(DeleteRecvLvEvent);
                    this.Invoke(d, new object[] { });
                }
                else
                {
                    displayAreaElement[currentAreaTab].lvTaskList.Items.Clear();
                }
            }
        }
        #endregion

        #region
        /// <summary>
        /// 通过控件名称获取到控件
        /// </summary>
        /// <param name="controlName">控件名称</param>
        /// <param name="containChildControlWindow">包含子控件的窗体（比如窗体是[Controls]、panel是[panel.controls]）</param>
        /// <returns></returns>
        private Control GetControlOfName(string controlName, Control.ControlCollection containChildControlWindow)
        {
            if (string.IsNullOrEmpty(controlName)) return null;

            foreach (Control item in containChildControlWindow)
            {
                if (item.Name.Equals(controlName))
                {
                    return item;
                }
            }

            return null;
        }
        #endregion

    }
}