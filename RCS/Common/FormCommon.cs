﻿using RCS.Charts;
using RCS.DeviceForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RCS.Common
{
    public partial class FormCommon : Form
    {
        public FormAreaSystemSet f1; //创建用户控件一变量
        public FormCharts f2; //创建用户控件二变量
        public FormDoughnutCharts f3; //创建用户控件三变量

        public FormCommon()
        {
            InitializeComponent();
        }
        //库区设置
        private void uiButton1_Click(object sender, EventArgs e)
        {
            f1.Show();   //将窗体一进行显示
            fromAreaSet();
        }
        //柱状图
        private void uiButton2_Click(object sender, EventArgs e)
        {
            f2.Show();   //将窗体一进行显示
            fromCharts();
        }
        //饼状图
        private void uiButton3_Click(object sender, EventArgs e)
        {
            f3.Show();   //将窗体一进行显示
            fromDoughnutCharts();
        }

        private void FormCommon_Load(object sender, EventArgs e)
        {
            //默认 展示库区设置
            f1 = new FormAreaSystemSet();    //实例化f1
            fromAreaSet();

            f2 = new FormCharts();    //实例化f2
            f3 = new FormDoughnutCharts();    //实例化f3

        }
        /// <summary>
        /// 库区设置 方法
        /// </summary>
        private void fromAreaSet()
        {
            f1.Show();   //将窗体一进行显示
            f1.TopLevel = false;
            f1.FormBorderStyle = FormBorderStyle.None;       //设置窗体为非边框样式
            f1.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Controls.Clear();    //清空原容器上的控件
            panel2.Controls.Add(f1);
        }
        /// <summary>
        /// 饼状图
        /// </summary>
        private void fromCharts()
        {
            Control c = new Control();

            f2.Show();   //将窗体一进行显示
            f2.TopLevel = false;
            f2.FormBorderStyle = FormBorderStyle.None;       //设置窗体为非边框样式
            f2.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Controls.Clear();    //清空原容器上的控件
            panel2.Controls.Add(f2);
        }
        /// <summary>
        /// 柱状图
        /// </summary>
        private void fromDoughnutCharts()
        {
            f3.Show();   //将窗体一进行显示
            f3.TopLevel = false;
            f3.FormBorderStyle = FormBorderStyle.None;       //设置窗体为非边框样式
            f3.Dock = System.Windows.Forms.DockStyle.Fill;
            panel2.Controls.Clear();    //清空原容器上的控件
            panel2.Controls.Add(f3);
        }

    }
}
