﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 播种墙货格表
    ///</summary>
    [SugarTable("table_channel_seedingwall")]
    public class table_channel_seedingwall
    {
        /// <summary>
        /// 1 
        ///</summary>
         [SugarColumn(ColumnName="Id" ,IsPrimaryKey = true   )]
         public string Id { get; set; }
        /// <summary>
        /// 货格 
        ///</summary>
         [SugarColumn(ColumnName="UNITS"    )]
         public string UNITS { get; set; }
        /// <summary>
        /// 货格横坐标 
        ///</summary>
         [SugarColumn(ColumnName="UNITS_X"    )]
         public string UNITS_X { get; set; }
        /// <summary>
        /// 货格纵坐标 
        ///</summary>
         [SugarColumn(ColumnName="UNITS_Y"    )]
         public string UNITS_Y { get; set; }
        /// <summary>
        /// 货格纵坐标等级 
        ///</summary>
         [SugarColumn(ColumnName="UNITS_Y_LEVEL"    )]
         public string UNITS_Y_LEVEL { get; set; }
        /// <summary>
        /// 通道 
        ///</summary>
         [SugarColumn(ColumnName="CHANNEL_NO"    )]
         public string CHANNEL_NO { get; set; }
        /// <summary>
        /// 占用状态(0未占用,1已占用) 
        ///</summary>
         [SugarColumn(ColumnName="USE_STATUS"    )]
         public string USE_STATUS { get; set; }
        /// <summary>
        /// 绑定SKU
        ///</summary>
        [SugarColumn(ColumnName = "BING_SKU")]
        public string BING_SKU { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
        [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime? CREATE_TIME { get; set; }
    }
}
