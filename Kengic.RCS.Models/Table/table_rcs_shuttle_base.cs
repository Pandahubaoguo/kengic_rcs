﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 穿梭车基础表
    ///</summary>
    [SugarTable("table_rcs_shuttle_base")]
    public class table_rcs_shuttle_base
    {
        /// <summary>
        /// 小车编号 
        ///</summary>
         [SugarColumn(ColumnName="SHUTTLE_ID" ,IsPrimaryKey = true   )]
         public string SHUTTLE_ID { get; set; }
        /// <summary>
        /// 库区编号 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NO"    )]
         public string AREA_NO { get; set; }
        /// <summary>
        /// 巷道编号 
        ///</summary>
         [SugarColumn(ColumnName="CHANNEL_NO"    )]
         public int CHANNEL_NO { get; set; }
        /// <summary>
        /// 层编号 
        ///</summary>
         [SugarColumn(ColumnName="LEVEL_NO"    )]
         public int LEVEL_NO { get; set; }
        /// <summary>
        /// 使用状态(0正常 1任务占用 2车辆损坏) 
        ///</summary>
         [SugarColumn(ColumnName="USE_STATUS"    )]
         public int USE_STATUS { get; set; }
        /// <summary>
        /// 是否换层(0 不换 1换) 
        ///</summary>
         [SugarColumn(ColumnName="IS_CHANGE"    )]
         public int IS_CHANGE { get; set; }
        /// <summary>
        /// 备注 
        ///</summary>
         [SugarColumn(ColumnName="NOTE"    )]
         public string NOTE { get; set; }
    }
}
