﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 任务主表
    ///</summary>
    [SugarTable("table_rcs_task_master")]
    public class table_rcs_task_master
    {
        /// <summary>
        /// 任务号 
        ///</summary>
         [SugarColumn(ColumnName="TASK_ID" ,IsPrimaryKey = true   )]
         public string TASK_ID { get; set; }
        /// <summary>
        /// 任务类型 
        ///</summary>
         [SugarColumn(ColumnName="TASK_TYPE"    )]
         public string TASK_TYPE { get; set; }
        /// <summary>
        /// 任务状态 
        ///</summary>
         [SugarColumn(ColumnName="STATUS"    )]
         public int STATUS { get; set; }
        /// <summary>
        /// 起始地址 
        ///</summary>
         [SugarColumn(ColumnName="FROM_ADDR"    )]
         public string FROM_ADDR { get; set; }
        /// <summary>
        /// 目的地址 
        ///</summary>
         [SugarColumn(ColumnName="TO_ADDR"    )]
         public string TO_ADDR { get; set; }
        /// <summary>
        /// 容器号 
        ///</summary>
         [SugarColumn(ColumnName="CONTAINER_NO"    )]
         public string CONTAINER_NO { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
         [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime CREATE_TIME { get; set; }
        /// <summary>
        /// 执行时间 
        ///</summary>
         [SugarColumn(ColumnName="EXECUTE_TIME"    )]
         public DateTime EXECUTE_TIME { get; set; }
        /// <summary>
        /// 完成时间 
        ///</summary>
         [SugarColumn(ColumnName="FINISH_TIME"    )]
         public DateTime FINISH_TIME { get; set; }
        /// <summary>
        /// 任务优先级 
        ///</summary>
         [SugarColumn(ColumnName="PRIORITY"    )]
         public int? PRIORITY { get; set; }
        /// <summary>
        /// 备注 
        ///</summary>
         [SugarColumn(ColumnName="NOTE_M"    )]
         public string NOTE_M { get; set; }
    }
}
