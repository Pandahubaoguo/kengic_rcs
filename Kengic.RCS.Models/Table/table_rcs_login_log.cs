﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 登录日志记录表
    ///</summary>
    [SugarTable("table_rcs_login_log")]
    public class table_rcs_login_log
    {
        /// <summary>
        /// 1 
        ///</summary>
         [SugarColumn(ColumnName="Id" ,IsPrimaryKey = true   )]
         public string Id { get; set; }
        /// <summary>
        /// 登录人员编号 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_ID"    )]
         public string LOGIN_ID { get; set; }
        /// <summary>
        /// 登录人 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_PERSON"    )]
         public string LOGIN_PERSON { get; set; }
        /// <summary>
        /// 地点 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_PERSON"    )]
         public string LOGIN_PLACE { get; set; }
        /// <summary>
        /// 登录项目 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_PROGRAM"    )]
         public string LOGIN_PROGRAM { get; set; }
        /// <summary>
        /// 登录描述 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_DESC"    )]
         public string LOGIN_DESC { get; set; }
        /// <summary>
        /// 登录时间 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_TIME"    )]
         public string LOGIN_TIME { get; set; }
        /// <summary>
        /// 登陆类型 
        ///</summary>
         [SugarColumn(ColumnName="LOGIN_TYPE"    )]
         public string LOGIN_TYPE { get; set; }
    }
}
