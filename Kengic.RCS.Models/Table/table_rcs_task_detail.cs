﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 任务明细表
    ///</summary>
    [SugarTable("table_rcs_task_detail")]
    public class table_rcs_task_detail
    {
        /// <summary>
        /// 任务号 
        ///</summary>
         [SugarColumn(ColumnName="TASK_ID" ,IsPrimaryKey = true   )]
         public string TASK_ID { get; set; }
        /// <summary>
        /// 任务行号 
        ///</summary>
         [SugarColumn(ColumnName="LINE_NO"    )]
         public int? LINE_NO { get; set; }
        /// <summary>
        /// SKU 
        ///</summary>
         [SugarColumn(ColumnName="SKU"    )]
         public string SKU { get; set; }
        /// <summary>
        /// SKU名称 
        ///</summary>
         [SugarColumn(ColumnName="SKU_NAME"    )]
         public string SKU_NAME { get; set; }
        /// <summary>
        /// 批次 
        ///</summary>
         [SugarColumn(ColumnName="BATCH"    )]
         public string BATCH { get; set; }
        /// <summary>
        /// SKU数量 
        ///</summary>
         [SugarColumn(ColumnName="SKU_NUM"    )]
         public int? SKU_NUM { get; set; }
        /// <summary>
        /// 备注 
        ///</summary>
         [SugarColumn(ColumnName="NOTE_D"    )]
         public string NOTE_D { get; set; }
    }
}
