﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 任务路由表
    ///</summary>
    [SugarTable("table_rcs_task_route")]
    public class table_rcs_task_route
    {
        /// <summary>
        /// 路由任务号 
        ///</summary>
         [SugarColumn(ColumnName="TASK_ROUTE_ID" ,IsPrimaryKey = true   )]
         public string TASK_ROUTE_ID { get; set; }
        /// <summary>
        /// 主任务号 
        ///</summary>
         [SugarColumn(ColumnName="TASK_ID"    )]
         public string  TASK_ID { get; set; }
        /// <summary>
        /// 路由行号 
        ///</summary>
         [SugarColumn(ColumnName="ROUTE_LINE_NO"    )]
         public int? ROUTE_LINE_NO { get; set; }
        /// <summary>
        /// 路由起始地址 
        ///</summary>
         [SugarColumn(ColumnName="ROUTE_FROM_ADDR"    )]
         public string ROUTE_FROM_ADDR { get; set; }
        /// <summary>
        /// 路由目的地址 
        ///</summary>
         [SugarColumn(ColumnName="ROUTE_TO_ADDR"    )]
         public string ROUTE_TO_ADDR { get; set; }
        /// <summary>
        /// 命令设备 
        ///</summary>
         [SugarColumn(ColumnName="COMMAND_DEVICE"    )]
         public string COMMAND_DEVICE { get; set; }
        /// <summary>
        /// 状态 
        ///</summary>
         [SugarColumn(ColumnName="STATUS"    )]
         public int STATUS { get; set; }
        /// <summary>
        /// 起始地址 
        ///</summary>
         [SugarColumn(ColumnName="FROM_ADDR"    )]
         public string FROM_ADDR { get; set; }
        /// <summary>
        /// 目的地址 
        ///</summary>
         [SugarColumn(ColumnName="TO_ADDR"    )]
         public string TO_ADDR { get; set; }
        /// <summary>
        /// 优先级 
        ///</summary>
         [SugarColumn(ColumnName="SEND_SEQ"    )]
         public string SEND_SEQ { get; set; }
        /// <summary>
        /// 任务类型 
        ///</summary>
         [SugarColumn(ColumnName="TASK_TYPE"    )]
         public string TASK_TYPE { get; set; }
        /// <summary>
        /// 容器号 
        ///</summary>
         [SugarColumn(ColumnName="CONTAINER_NO"    )]
         public string CONTAINER_NO { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
         [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime CREATE_TIME { get; set; }
        /// <summary>
        /// 执行时间 
        ///</summary>
         [SugarColumn(ColumnName="EXECUTE_TIME"    )]
         public DateTime EXECUTE_TIME { get; set; }
        /// <summary>
        /// 完成时间 
        ///</summary>
         [SugarColumn(ColumnName="FINISH_TIME"    )]
         public DateTime FINISH_TIME { get; set; }
        /// <summary>
        /// 备注 
        ///</summary>
         [SugarColumn(ColumnName="NOTE"    )]
         public string NOTE { get; set; }
        /// <summary>
        /// 上一个路由任务号 
        ///</summary>
         [SugarColumn(ColumnName="LASTROUTESEQ"    )]
         public int? LASTROUTESEQ { get; set; }
    }
}
