﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 人员操作日志记录表
    ///</summary>
    [SugarTable("table_rcs_operate_log")]
    public class table_rcs_operate_log
    {
        /// <summary>
        /// 1 
        ///</summary>
         [SugarColumn(ColumnName="Id" ,IsPrimaryKey = true   )]
         public string Id { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_ID"    )]
         public string OP_ID { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_PERSON"    )]
         public string OP_PERSON { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_PLACE"    )]
         public string OP_PLACE { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_PROGRAM"    )]
         public string OP_PROGRAM { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_DESC"    )]
         public string OP_DESC { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_TIME"    )]
         public DateTime? OP_TIME { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="OP_TYPE"    )]
         public string OP_TYPE { get; set; }
    }
}
