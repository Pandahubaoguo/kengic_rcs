﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 库存子表
    ///</summary>
    [SugarTable("table_rcs_node_detail")]
    public class table_rcs_node_detail
    {
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="NODE_M_GUID", IsPrimaryKey = true)]
         public string NODE_M_GUID { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="LOCATION_NO"    )]
         public string LOCATION_NO { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="SKU"    )]
         public string SKU { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="SKU_NAME"    )]
         public string SKU_NAME { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="CONTAINER_NO"    )]
         public string CONTAINER_NO { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="BATCH"    )]
         public string BATCH { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="QTY"    )]
         public int? QTY { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="USE_QTY"    )]
         public int USE_QTY { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="CHECK_QTY"    )]
         public int? CHECK_QTY { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="IS_FREEZE"    )]
         public int IS_FREEZE { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime CREATE_TIME { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="MODIFY_TIME"    )]
         public DateTime MODIFY_TIME { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="VERSION"    )]
         public int? VERSION { get; set; }
    }
}
