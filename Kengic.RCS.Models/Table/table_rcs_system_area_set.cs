﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 系统配置表
    ///</summary>
    [SugarTable("table_rcs_system_area_set")]
    public class table_rcs_system_area_set
    {
        /// <summary>
        /// 主键 
        ///</summary>
         [SugarColumn(ColumnName="Id" ,IsPrimaryKey = true   )]
         public string Id { get; set; }
        /// <summary>
        /// 区域号 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NO"    )]
         public string AREA_NO { get; set; }
        /// <summary>
        /// 区域名称 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NAME"    )]
         public string AREA_NAME { get; set; }

        /// <summary>
        /// 区域是否显示 0不显示 1显示
        ///</summary>
        [SugarColumn(ColumnName = "AREA_SHOW_STATUS")]
        public string AREA_SHOW_STATUS { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
        [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime? CREATE_TIME { get; set; }
        /// <summary>
        /// 备选 
        ///</summary>
         [SugarColumn(ColumnName="ALTERNATIVE"    )]
         public string ALTERNATIVE { get; set; }
    }
}
