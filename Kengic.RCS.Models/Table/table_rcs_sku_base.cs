﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// SKU基础数据表
    ///</summary>
    [SugarTable("table_rcs_sku_base")]
    public class table_rcs_sku_base
    {
        /// <summary>
        /// SKU 
        ///</summary>
         [SugarColumn(ColumnName="SKU_NO" ,IsPrimaryKey = true   )]
         public string SKU_NO { get; set; }
        /// <summary>
        /// SKU名称 
        ///</summary>
         [SugarColumn(ColumnName="SKU_NAME"    )]
         public string SKU_NAME { get; set; }
        /// <summary>
        /// 批次 
        ///</summary>
         [SugarColumn(ColumnName="BATCH"    )]
         public string BATCH { get; set; }
        /// <summary>
        /// 批次名称 
        ///</summary>
         [SugarColumn(ColumnName="BATCH_NAME"    )]
         public string BATCH_NAME { get; set; }
        /// <summary>
        /// SKU数量 
        ///</summary>
         [SugarColumn(ColumnName="SKU_NUM"    )]
         public int? SKU_NUM { get; set; }
        /// <summary>
        /// SKU规格(包、盒) 
        ///</summary>
         [SugarColumn(ColumnName="SKU_SPECIFICATIONS"    )]
         public string SKU_SPECIFICATIONS { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
         [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime? CREATE_TIME { get; set; }
        /// <summary>
        /// 更新时间 
        ///</summary>
         [SugarColumn(ColumnName="MODIFY_TIME"    )]
         public DateTime? MODIFY_TIME { get; set; }
    }
}
