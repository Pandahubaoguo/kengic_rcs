﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 设备基础数据表
    ///</summary>
    [SugarTable("table_rcs_base_device")]
    public class table_rcs_base_device
    {
        /// <summary>
        /// 设备编号 
        ///</summary>
         [SugarColumn(ColumnName="DEVICE_ID" ,IsPrimaryKey = true   )]
         public string DEVICE_ID { get; set; }
        /// <summary>
        /// 命令DB 
        ///</summary>
         [SugarColumn(ColumnName="COMMAND_DB"    )]
         public string COMMAND_DB { get; set; }
        /// <summary>
        /// 返回DB 
        ///</summary>
         [SugarColumn(ColumnName="RETURN_DB"    )]
         public string RETURN_DB { get; set; }
        /// <summary>
        /// 控制DB 
        ///</summary>
         [SugarColumn(ColumnName="CONTROL_DB"    )]
         public string CONTROL_DB { get; set; }
        /// <summary>
        /// 状态DB 
        ///</summary>
         [SugarColumn(ColumnName="STATUS_DB"    )]
         public string STATUS_DB { get; set; }
        /// <summary>
        /// 状态DB 
        ///</summary>
         [SugarColumn(ColumnName="LOAD_DB"    )]
         public string LOAD_DB { get; set; }
        /// <summary>
        /// 设备类型 
        ///</summary>
         [SugarColumn(ColumnName="DEVICE_TYPE"    )]
         public string DEVICE_TYPE { get; set; }
        /// <summary>
        /// 横坐标 
        ///</summary>
         [SugarColumn(ColumnName="X"    )]
         public int X { get; set; }
        /// <summary>
        /// 纵坐标 
        ///</summary>
         [SugarColumn(ColumnName="Y"    )]
         public int Y { get; set; }
        /// <summary>
        /// 宽 
        ///</summary>
         [SugarColumn(ColumnName="WIDTH"    )]
         public int WIDTH { get; set; }
        /// <summary>
        /// 高 
        ///</summary>
         [SugarColumn(ColumnName="HEIGHT"    )]
         public int HEIGHT { get; set; }
        /// <summary>
        /// 设备名称 
        ///</summary>
         [SugarColumn(ColumnName="DEVICE_NAME"    )]
         public string DEVICE_NAME { get; set; }
        /// <summary>
        /// 设备状态 
        ///</summary>
         [SugarColumn(ColumnName="DEVICE_STATUS"    )]
         public int DEVICE_STATUS { get; set; }
        /// <summary>
        /// 区域 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NO"    )]
         public string AREA_NO { get; set; }
        /// <summary>
        /// 是否自动执行 
        ///</summary>
         [SugarColumn(ColumnName="IS_AUTO"    )]
         public int? IS_AUTO { get; set; }
        /// <summary>
        /// 扫码器编号(备选) 
        ///</summary>
         [SugarColumn(ColumnName="SCANNER_NO"    )]
         public string SCANNER_NO { get; set; }
        /// <summary>
        /// 序列 
        ///</summary>
         [SugarColumn(ColumnName="SEQ"    )]
         public int? SEQ { get; set; }
    }
}
