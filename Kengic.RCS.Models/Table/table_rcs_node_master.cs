﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 库存主表
    ///</summary>
    [SugarTable("table_rcs_node_master")]
    public class table_rcs_node_master
    {
        /// <summary>
        /// 货位guid 
        ///</summary>
         [SugarColumn(ColumnName="NODE_M_GUID" ,IsPrimaryKey = true   )]
         public string NODE_M_GUID { get; set; }
        /// <summary>
        /// 货位 
        ///</summary>
         [SugarColumn(ColumnName="LOCATION_NO"    )]
         public string LOCATION_NO { get; set; }
        /// <summary>
        /// 区域 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NO"    )]
         public string AREA_NO { get; set; }
        /// <summary>
        /// 区域名称 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NAME"    )]
         public string AREA_NAME { get; set; }
        /// <summary>
        /// 仓库 
        ///</summary>
         [SugarColumn(ColumnName="WAREOUSE_NO"    )]
         public string WAREOUSE_NO { get; set; }
        /// <summary>
        /// 仓库名称 
        ///</summary>
         [SugarColumn(ColumnName="WAREOUSE_NAME"    )]
         public string WAREOUSE_NAME { get; set; }
        /// <summary>
        /// 巷道号 
        ///</summary>
         [SugarColumn(ColumnName="TUNNEL_NO"    )]
         public int TUNNEL_NO { get; set; }
        /// <summary>
        /// 排 
        ///</summary>
         [SugarColumn(ColumnName="ROW_NO"    )]
         public int ROW_NO { get; set; }
        /// <summary>
        /// 列 
        ///</summary>
         [SugarColumn(ColumnName="BAY_NO"    )]
         public int BAY_NO { get; set; }
        /// <summary>
        /// 层 
        ///</summary>
         [SugarColumn(ColumnName="LEVEL_NO"    )]
         public int? LEVEL_NO { get; set; }
        /// <summary>
        /// 深度 
        ///</summary>
         [SugarColumn(ColumnName="DEPTH_NO"    )]
         public int DEPTH_NO { get; set; }
        /// <summary>
        /// 货位状态(1有货 0 无货 2货位入占用 3货位出占用) 
        ///</summary>
         [SugarColumn(ColumnName="UNIT_STATUS"    )]
         public int UNIT_STATUS { get; set; }
        /// <summary>
        /// 是否冻结(1冻结 0未冻结) 
        ///</summary>
         [SugarColumn(ColumnName="IS_FREEZE"    )]
         public int IS_FREEZE { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
         [SugarColumn(ColumnName="CREATE_TIME"    )]
         public DateTime CREATE_TIME { get; set; }
        /// <summary>
        /// 修改时间 
        ///</summary>
         [SugarColumn(ColumnName="MODIFY_TIME"    )]
         public DateTime MODIFY_TIME { get; set; }
        /// <summary>
        /// 版本号 
        ///</summary>
         [SugarColumn(ColumnName="VERSION"    )]
         public int? VERSION { get; set; }
        /// <summary>
        /// 货位状态(1不可用 0 可用) 
        ///</summary>
         [SugarColumn(ColumnName="USE_STATUS"    )]
         public int USE_STATUS { get; set; }
    }
}
