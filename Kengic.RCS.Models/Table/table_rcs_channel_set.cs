﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 库区设备基础表
    ///</summary>
    [SugarTable("table_rcs_channel_set")]
    public class table_rcs_channel_set
    {
        /// <summary>
        /// 库区 
        ///</summary>
         [SugarColumn(ColumnName="AREA_NO" ,IsPrimaryKey = true   )]
         public string AREA_NO { get; set; }
        /// <summary>
        /// 巷道 
        ///</summary>
         [SugarColumn(ColumnName="CHANNEL_NO"    )]
         public int CHANNEL_NO { get; set; }
        /// <summary>
        /// 排 
        ///</summary>
         [SugarColumn(ColumnName="ROW_NO"    )]
         public int ROW_NO { get; set; }
        /// <summary>
        /// 使用状态(0正常 1异常) 
        ///</summary>
         [SugarColumn(ColumnName="USE_STATUS"    )]
         public int USE_STATUS { get; set; }
    }
}
