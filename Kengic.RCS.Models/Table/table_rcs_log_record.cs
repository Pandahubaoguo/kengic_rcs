﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Kengic.RCS.Models.Table
{
    /// <summary>
    /// 日志记录表
    ///</summary>
    [SugarTable("table_rcs_log_record")]
    public class table_rcs_log_record
    {
        /// <summary>
        /// 1 
        ///</summary>
         [SugarColumn(ColumnName="Id" ,IsPrimaryKey = true   )]
         public string Id { get; set; }
        /// <summary>
        /// 设备编号 
        ///</summary>
         [SugarColumn(ColumnName="device_id"    )]
         public string device_id { get; set; }
        /// <summary>
        /// 设备状态编号 
        ///</summary>
         [SugarColumn(ColumnName="status_id"    )]
         public string status_id { get; set; }
        /// <summary>
        /// 设备状态名称 
        ///</summary>
         [SugarColumn(ColumnName="status_name"    )]
         public string status_name { get; set; }
        /// <summary>
        /// 设备报警级别 
        ///</summary>
         [SugarColumn(ColumnName="status_type"    )]
         public string status_type { get; set; }
        /// <summary>
        /// 创建时间 
        ///</summary>
         [SugarColumn(ColumnName="create_time"    )]
         public DateTime? create_time { get; set; }
        /// <summary>
        /// 结束时间 
        ///</summary>
         [SugarColumn(ColumnName="end_time"    )]
         public DateTime? end_time { get; set; }
    }
}
