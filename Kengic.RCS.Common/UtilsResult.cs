﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.Common
{
    public class UtilsResult
    {
        //返回结果数
        public int ri_ret { get; set; }
        //返回结果值
        public string rs_ret { get; set; }
    }
}
