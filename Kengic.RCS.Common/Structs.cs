﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.Common
{
    #region 字典结构体定义
    /// <summary>
    /// 设备状态字典用
    /// </summary>
    public struct DeviceStatus  //设备状态字典用
    {
        /// <summary>
        /// 状态名称
        /// </summary>
        public string statusDesc;
        /// <summary>
        /// 状态性质
        /// </summary>
        public string statusKind;
    }
    /// <summary>
    /// 设备控制(四个字符串类型成员变量)
    /// </summary>
    public struct DeviceControl
    {
        /// <summary>
        /// 控制类型
        /// </summary>
        public string controlType;
        /// <summary>
        /// 控制ID
        /// </summary>
        public string controlId;
        /// <summary>
        /// 控制名称
        /// </summary>
        public string controlDesc;
        /// <summary>
        /// 控制模式，0:手动模式下操作 1:任何模式下 2:故障模式下
        /// </summary>
        public string condition;
    }
    /// <summary>
    /// PLC站状态
    /// </summary>
    public struct PlcSiteStatus
    {
        public string statusDesc;
        public string statusKind;
    }

    /// <summary>
    /// 穿梭车缺省位置结构
    /// </summary>
    public struct CSCPos
    {
        public int x;
        public int y;
    }
    #endregion
}
