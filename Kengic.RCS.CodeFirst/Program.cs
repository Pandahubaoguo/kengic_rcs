﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.CodeFirst
{
    class Program
    {
        /// <summary>
        /// 通过代码生成数据库
        /// 暂时作为一个单独的块
        /// 后期整合进程序进行 检测 生成处理
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            //数据库链接字符串
            //string ConnectionString2 = "Data Source=DESKTOP-T2D6ILD;Initial Catalog=SqlSugarCustomerDB_20211120;Persist Security Info=True;User ID=sa;Password=sa123";
            string ConnectionString2 = ConfigurationManager.AppSettings["LocalConnectionString"];
            ConnectionConfig config2 = new ConnectionConfig()
            {
                ConnectionString = ConnectionString2,
                DbType = DbType.SqlServer,
                InitKeyType = InitKeyType.Attribute, //初始化主键和自增列信息到ORM的方式
                IsAutoCloseConnection = true //是否自动关闭连接 Yes
            };


            //通过实体生成数据库 
            Assembly assembly = Assembly.LoadFrom("Kengic.RCS.Models.dll");
            IEnumerable<Type> typelist

                = assembly.GetTypes().Where(c => c.Namespace == "Kengic.RCS.Models.Table");

            bool Backup = false;  //是否备份
            using (SqlSugarClient Client = new SqlSugarClient(config2))
            {

                Client.Aop.OnLogExecuting = (sql, par) =>
                {
                    Console.WriteLine($"sql语句：{sql}");
                };

                Client.DbMaintenance.CreateDatabase(); //创建一个数据库出来
                if (Backup)
                {
                    Client.CodeFirst.BackupTable().InitTables(typelist.ToArray());
                }
                else
                {
                    Client.CodeFirst.InitTables(typelist.ToArray());
                }

                //Console.Read();
            }

        }
    }
}
