﻿using Kengic.RCS.BLL.StoredProcedure;
using Kengic.RCS.Common;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.BLL
{
    class Program
    {
        protected static UtilsResult _UtilsResult;
        static void Main(string[] args)
        {
            TaskFinishProcedure serviceProcedure = new TaskFinishProcedure(new SqlSugarClient(new ConnectionConfig() {
                ConnectionString = ConfigurationManager.AppSettings["LocalConnectionString"],
                DbType = SqlSugar.DbType.SqlServer,
                InitKeyType = InitKeyType.Attribute,
                IsAutoCloseConnection = true  //是否自动关闭连接  Yes
            }));
            serviceProcedure.SP_FinishTask("1", out _UtilsResult);
        }
    }
}
