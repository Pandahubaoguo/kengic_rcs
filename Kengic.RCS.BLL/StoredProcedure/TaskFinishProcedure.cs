﻿using Kengic.RCS.Common;
using Kengic.RCS.Models.Table;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.BLL.StoredProcedure
{
    /// <summary>
    /// 业务存储过程 在程序里面完成处理
    /// Author：Panda.HU Yeah
    /// 2022-02-09 Fighting!
    /// </summary>
    public class TaskFinishProcedure
    {
        protected ISqlSugarClient sqlSugarClient;

        protected UtilsResult _UtilsResult;

        public TaskFinishProcedure(ISqlSugarClient sqlSugarClient)
        {
            this.sqlSugarClient = sqlSugarClient;
        }

        #region FinishTaskMaster
        /// <summary>
        /// 主任务报完成
        /// </summary>
        /// <param name="task_no">主任务号</param>
        /// <param name="status">完成状态</param>
        /// <param name="utilsResult">反馈结果</param>
        public void SP_FinishTaskMaster(string task_no, int status, out UtilsResult utilsResult)
        {
            //-------1新生成   3执行中    10已完成
            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };

            //看一下当前任务号是否存在
            try
            {
                var tm = sqlSugarClient.Queryable<table_rcs_task_master>().Where(r => r.TASK_ID == task_no).ToList().FirstOrDefault();
                if (string.IsNullOrEmpty(tm?.TASK_ID))
                {
                    utilsResult.ri_ret = -1;
                    utilsResult.rs_ret = task_no + ",没有此任务！";
                    return;
                }
                //两个状态相等 没必要修改返回即可
                //10也是完成  没必要在进行修改
                if (tm.STATUS == status || tm.STATUS == 10) return;

                //实例化任务主类
                /*table_rcs_task_master trtm = new table_rcs_task_master()
                {
                   
                    STATUS = status,
                    EXECUTE_TIME = DateTime.Now
                };*/
                //tm.STATUS = status;
                tm.EXECUTE_TIME = DateTime.Now;
                //暂时屏蔽吧  taskd 只进行物品明细的记录即可 不需要繁琐的进行完成 无意义
                /*   table_rcs_task_detail trtd = new table_rcs_task_detail()
                   {
                       TASK_ID = task_no,
                        = status,
                       EXECUTE_TIME = DateTime.Now
                   };*/

                //分为入库出库
                if (tm.TASK_TYPE == "1")
                {
                    #region 入库IN
                    switch (status)
                    {
                        case 3:
                            /*var updateStatus = sqlSugarClient.Updateable<table_rcs_task_master>(tm).ExecuteCommand();
                            if (updateStatus == 0)
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                                return;
                            };
                            break;*/
                        case 10:
                            if (tm.STATUS != 3)
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",任务为非执行状态！报完成失败";
                                return;
                            }
                            //更新任务状态
                           /* if (! _baseDAL.Update<table_rcs_task_master>(tm))
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                                return;
                            };*/
                            break;
                        default:
                            utilsResult.ri_ret = -1;
                            utilsResult.rs_ret = task_no + ",不能识别的任务状态！";
                            return;


                    }
                    //更新任务状态
                    var updateStatus = sqlSugarClient.Updateable<table_rcs_task_master>(tm).ExecuteCommand();
                    if (updateStatus == 0)
                    {
                        utilsResult.ri_ret = -1;
                        utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                        return;
                    };
                    #endregion
                }
                else
                {
                    #region 出库OUT
                    switch (status)
                    {
                        case 3:
                            /*if (!_baseDAL.Update<table_rcs_task_master>(tm))
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                                return;
                            };*/
                            break;
                        case 10:
                            if (tm.STATUS != 3)
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",任务为非执行状态！报完成失败";
                                return;
                            }
                            //更新任务状态
                            /*if (!_baseDAL.Update<table_rcs_task_master>(tm))
                            {
                                utilsResult.ri_ret = -1;
                                utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                                return;
                            };*/
                            break;
                        default:
                            utilsResult.ri_ret = -1;
                            utilsResult.rs_ret = task_no + ",不能识别的任务状态！";
                            return;
                    }
                    #endregion
                    //更新任务状态
                    var updateStatus = sqlSugarClient.Updateable<table_rcs_task_master>(tm).ExecuteCommand();
                    if (updateStatus == 0)
                    {
                        utilsResult.ri_ret = -1;
                        utilsResult.rs_ret = task_no + ",更新任务状态失败！";
                        return;
                    };
                }
            }
            catch (Exception ex)
            {
                utilsResult.ri_ret = 99;
                utilsResult.rs_ret = ex.ToString();
                return;
            }
            //库存处理
            SP_NodeOperation(task_no, out _UtilsResult);
            if (_UtilsResult.ri_ret != 0)
            {
                return;
            }

        }

        #endregion

        #region NodeOperation
        /// <summary>
        /// 库存处理
        /// </summary>
        /// <param name="task_no">主任务号</param>
        /// <param name="utilsResult">反馈结果</param>
        public void SP_NodeOperation(string task_no, out UtilsResult utilsResult)
        {
            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };

            //看一下当前任务号是否存在
            try
            {
                var tm = sqlSugarClient.Queryable<table_rcs_task_master>().Where(r => r.TASK_ID == task_no).First();
                var td = sqlSugarClient.Queryable<table_rcs_task_detail>().Where(r => r.TASK_ID == task_no).First();
                //判断目的地 不为空 并且符合库存数据
                if (string.IsNullOrEmpty(tm.TO_ADDR))
                {
                    utilsResult.ri_ret = -1;
                    utilsResult.rs_ret = "目的地址为空！不能进行库存处理";
                    return;
                }

                //验证一下 货位中是否存在此地址  否则说明货位未维护  或者 任务表中的目的地不正确
                table_rcs_node_master tnm;
                var tnmCount = sqlSugarClient.Queryable<table_rcs_node_master>().Where(r => r.LOCATION_NO == tm.TO_ADDR).Count();
                if (tnmCount <= 0)
                {
                    utilsResult.ri_ret = -1;
                    utilsResult.rs_ret = tm.TO_ADDR + ",货位未维护！";
                    return;
                }
                else
                { tnm = sqlSugarClient.Queryable<table_rcs_node_master>().Where(r => r.LOCATION_NO == tm.TO_ADDR).First(); }

                //下面查询库存中数据 是否存在 存在 即可进行更新  不存在则新增
                var tnd = sqlSugarClient.Queryable<table_rcs_node_detail>().Where(r => r.CONTAINER_NO == tm.CONTAINER_NO).First();
                //看一下是否存在数据
                if (!string.IsNullOrEmpty(tnd.LOCATION_NO))
                {
                    tnd.LOCATION_NO = tm.TO_ADDR;
                    tnd.MODIFY_TIME = DateTime.Now;
                    tnd.QTY = td.SKU_NUM;
                    tnd.USE_QTY = 0;
                    /*table_rcs_node_detail trnd = new table_rcs_node_detail()
                    {

                        LOCATION_NO = tm.TO_ADDR,
                        MODIFY_TIME = DateTime.Now,
                        QTY = td.SKU_NUM,
                        USE_QTY = 0
                    };*/
                    //不空
                    var updateResult = sqlSugarClient.Updateable<table_rcs_node_detail>(tnd).ExecuteCommand();
                    if (updateResult == 0)
                    {
                        utilsResult.ri_ret = -1;
                        utilsResult.rs_ret = tm.TO_ADDR + ",更新库存失败！";
                        return;
                    }
                }
                else
                {
                    //没有库存 则进行新增
                    table_rcs_node_detail trnd = new table_rcs_node_detail()
                    {
                        NODE_M_GUID = tnm?.NODE_M_GUID,
                        SKU = td.SKU,
                        SKU_NAME = td.SKU_NAME,
                        BATCH = td.BATCH,
                        CONTAINER_NO = tm.CONTAINER_NO,
                        CREATE_TIME = DateTime.Now,
                        LOCATION_NO = tm.TO_ADDR,
                        QTY = td.SKU_NUM,
                        USE_QTY = 0,
                        CHECK_QTY = 0
                    };
                    //增加库存
                    var insertResult = sqlSugarClient.Insertable<table_rcs_node_detail>(trnd).ExecuteCommand();
                    if (insertResult == 0)
                    {
                        utilsResult.ri_ret = -1;
                        utilsResult.rs_ret = tm.TO_ADDR + ",增加库存失败！";
                        return;
                    }
                }

                //最后将货位更新为有货状态
               /* tnm = new table_rcs_node_master()
                {
                    LOCATION_NO = tm.TO_ADDR,
                    UNIT_STATUS = 1, //0无货 1有货 2入库占用 3出库占用  可以用枚举 暂时先这么简单一写
                    MODIFY_TIME = DateTime.Now
                };*/

                tnm.LOCATION_NO = tm.TO_ADDR;
                tnm.UNIT_STATUS = 1;
                tnm.MODIFY_TIME = DateTime.Now;

                var updateLocationResult = sqlSugarClient.Updateable<table_rcs_node_master>(tnm).ExecuteCommand();
                if (updateLocationResult == 0) {
                    utilsResult.ri_ret = -1;
                    utilsResult.rs_ret = tm.TO_ADDR + ",更新货位失败！";
                    return;
                }
            }
            catch (Exception ex)
            {
                utilsResult.ri_ret = 99;
                utilsResult.rs_ret = ex.ToString();
                return;
            }

        }

        #endregion

        #region FinishTask
        /// <summary>
        /// 任务报完成
        /// </summary>
        /// <param name="route_no">路由任务号</param>
        /// <param name="utilsResult">反馈结果</param>
        public void SP_FinishTask(string route_no, out UtilsResult utilsResult)
        {
            var status = 10;

            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };

            //看一下当前任务号是否存在
            var tr = sqlSugarClient.Queryable<table_rcs_task_route>().Where(r => r.TASK_ROUTE_ID == route_no).First();
            if (string.IsNullOrEmpty(tr?.TASK_ROUTE_ID))
            {
                utilsResult.ri_ret = -1;
                utilsResult.rs_ret = route_no + ",没有此任务！";
                return;
            }
            //不空说明存在任务
            //接着看是否已经完成状态 完成不需要在进行修改状态数据
            if (tr.STATUS == status) return;

            tr.FINISH_TIME = DateTime.Now;
            tr.STATUS = status;

            //进行修改状态
            /*table_rcs_task_route trtr = new table_rcs_task_route()
            {
                FINISH_TIME = DateTime.Now,
                STATUS = status
            };*/
            //更新结果
            var updateResult = sqlSugarClient.Updateable<table_rcs_task_route>(tr).ExecuteCommand();
            if (updateResult == 0)
            {
                utilsResult.ri_ret = -1;
                utilsResult.rs_ret = route_no + "报完成失败！";
                return;
            }
            //查找路由表 中最大行数
            var routeLineNo = sqlSugarClient.Queryable<table_rcs_task_route>().Where(r => r.TASK_ID == tr.TASK_ID).Max(r => tr.ROUTE_LINE_NO);
            if (routeLineNo == tr.ROUTE_LINE_NO && tr.TASK_ID != "0")
            {
                //调用主任务报完成方法
                SP_FinishTaskMaster(tr.TASK_ID, status, out _UtilsResult);
                if (_UtilsResult.ri_ret != 0) return;
            }
           
        }

        #endregion


    }
}
