﻿using Kengic.RCS.Common;
using Kengic.RCS.Models.Table;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.BLL.StoredProcedure
{
    /// <summary>
    /// 业务存储过程 在程序里面完成处理
    /// Author：Panda.HU Yeah
    /// 2022-02-10 Fighting!
    /// </summary>
    public class FindUnitProcedure
    {
        protected ISqlSugarClient sqlSugarClient;

        protected UtilsResult _UtilsResult;

        public FindUnitProcedure(ISqlSugarClient sqlSugarClient)
        {
            this.sqlSugarClient = sqlSugarClient;
        }


        #region 查找货位
        /// <summary>
        /// 根据 库区 SKU进行查找
        /// </summary>
        /// <param name="wareHouse_no">库区</param>
        /// <param name="sku">SKU</param>
        /// <param name="location_no">返回货位</param>
        /// <param name="utilsResult"></param>
        public void FindBySkuUints(string wareHouse_no, string sku, out string location_no, out UtilsResult utilsResult)
        {
            location_no = "";
            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };
            #region 三种 多表连接实例
            /*var tmBase = _baseDAL.Query<table_rcs_node_master>();
            var tnBase = _baseDAL.Query<table_rcs_node_detail>();
            var taskmBase = _baseDAL.Query<table_rcs_task_master>();

            //linq
            var resultLinq = 
                from nodeM in tmBase join nodeD 
                in tnBase on new { nodeM.NODE_M_GUID, nodeM.LOCATION_NO } 
                equals new { nodeD.NODE_M_GUID, nodeD.LOCATION_NO } select new { nodeM.UNIT_STATUS,nodeD.CONTAINER_NO};

            // linq where 条件
            var resultLinq2 =
                from nodeM in tmBase
                from nodeD in tnBase
                where nodeM.NODE_M_GUID == nodeD.NODE_M_GUID && nodeM.LOCATION_NO == nodeD.LOCATION_NO
                select new { nodeM.UNIT_STATUS, nodeD.CONTAINER_NO };

            //lamdba 
            var resultJoin =
                tmBase.Join(tnBase, m => m.LOCATION_NO, n => n.LOCATION_NO,(m,n)=>new {m.LOCATION_NO, n.CONTAINER_NO })
                .Join(taskmBase,a=>a.LOCATION_NO,b => b.TO_ADDR,(a,b)=>new { a.CONTAINER_NO,b.TASK_ID}).Count();*/
            #endregion

            #region  查找货位
            //获取到货位主表信息
            var tmBase = sqlSugarClient.Queryable<table_rcs_node_master>().ToList();

            //获取到货位子表信息
            var tnBase = sqlSugarClient.Queryable<table_rcs_node_detail>().ToList();

            //获取巷道基础数据表
            var tcSet = sqlSugarClient.Queryable<table_rcs_channel_set>().ToList();

            //获取穿梭车数据表
            var tsBase = sqlSugarClient.Queryable<table_rcs_shuttle_base>().ToList();

            //货物SKU基础数据表
            var tskuBase = sqlSugarClient.Queryable<table_rcs_sku_base>().ToList();

            //First 判断是否有可用库位
            var useUnitsCount = (
                from nodeM in tmBase
                from channelSet in tcSet
                where nodeM.AREA_NO == channelSet.AREA_NO && nodeM.TUNNEL_NO == channelSet.CHANNEL_NO && nodeM.ROW_NO == channelSet.ROW_NO
                && nodeM.AREA_NO == wareHouse_no && nodeM.IS_FREEZE == 0 && channelSet.USE_STATUS == 0// 未冻结 巷道可使用
                select new { nodeM.LOCATION_NO }).Count();

            //判断是否大于0
            if (useUnitsCount == 0)
            {
                utilsResult.ri_ret = -1;
                utilsResult.rs_ret = "(具体巷道)无可用货位！返回";
                return;
            }

            //判断穿梭车是否可用
            var useDeviceCount = (
                from nodeM in tmBase
                from channelSet in tcSet
                from shuttleBase in tsBase
                where nodeM.AREA_NO == channelSet.AREA_NO && nodeM.TUNNEL_NO == channelSet.CHANNEL_NO && nodeM.ROW_NO == channelSet.ROW_NO
                && channelSet.AREA_NO == shuttleBase.AREA_NO && channelSet.CHANNEL_NO == shuttleBase.CHANNEL_NO && nodeM.LEVEL_NO == shuttleBase.LEVEL_NO
                && nodeM.AREA_NO == wareHouse_no && nodeM.IS_FREEZE == 0 && channelSet.USE_STATUS == 0 && (new int?[] { 0, 1 }).Contains(shuttleBase.USE_STATUS)
                select new { nodeM.TUNNEL_NO }).Count();
            if (useDeviceCount == 0)
            {
                utilsResult.ri_ret = -1;
                utilsResult.rs_ret = "(具体层)无可用货位！返回";
                return;
            }

            //判断SKU基础数据表是否存在当前SKU  不存在也返回
            var tskuBaseCount = tskuBase.Where(r => r.SKU_NO == sku).Count();
            if (tskuBaseCount == 0)
            {
                utilsResult.ri_ret = -1;
                utilsResult.rs_ret = sku + ",SKU未维护！返回";
                return;
            }

            //-------------------------查找货位BEGIN-----------------------------------
            //分6步走进行 来达到最优货位分配

            #region 1.查找深度2有货、深度1无货  且深度2货与入库货物同品规  且小车在当前层的

            var a =
                from nodeM in tmBase
                    //group nodeM by nodeM.LEVEL_NO 
                from channelSet in tcSet
                from shuttleBase in tsBase
                where nodeM.AREA_NO == channelSet.AREA_NO && nodeM.TUNNEL_NO == channelSet.CHANNEL_NO && nodeM.ROW_NO == channelSet.ROW_NO
                    && channelSet.AREA_NO == shuttleBase.AREA_NO && channelSet.CHANNEL_NO == shuttleBase.CHANNEL_NO && nodeM.LEVEL_NO == shuttleBase.LEVEL_NO
                    && nodeM.AREA_NO == wareHouse_no && nodeM.IS_FREEZE == 0 && channelSet.USE_STATUS == 0 && (new int?[] { 0, 1 }).Contains(shuttleBase.USE_STATUS) select new { };
            //tnBase
                #endregion

                #region 2.查找深度2有货、深度1无货  且深度2货与入库货物同品规  
                #endregion

                #region 3.查找深度2、深度1都无货    且小车在当前层的
                #endregion

                #region 4.查找深度2、深度1都无货  
                #endregion

                #region 5.查找深度2有货、深度1无货  且深度2货与入库货物 --NO-- 同品规  且小车在当前层的 
                #endregion

                #region 6.查找深度2有货、深度1无货  且深度2货与入库货物 --NO-- 同品规 
                #endregion


                #endregion



        }

        #endregion


    }
}
