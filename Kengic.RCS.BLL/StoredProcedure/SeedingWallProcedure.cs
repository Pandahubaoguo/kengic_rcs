﻿using Kengic.RCS.Common;
using Kengic.RCS.Models.Table;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kengic.RCS.BLL.StoredProcedure
{
    /// <summary>
    /// 播种墙调度
    /// Time:2022-2-14 11.41.51
    /// Author:Panda.HU
    /// 
    /// </summary>
    public class SeedingWallProcedure
    {
        protected ISqlSugarClient sqlSugarClient;

        protected UtilsResult _UtilsResult;
        public SeedingWallProcedure(ISqlSugarClient sqlSugarScope)
        {
            this.sqlSugarClient = sqlSugarScope;
        }


        #region 播种墙穿梭车任务控制调度
        /// <summary>
        /// 思路：
        /// ①做一个动态聚单算法：将SKU种类由多到少排布下来
        /// ②进行‘折线’算法处理，将SKU种类有多到少折线排布
        /// ③动态处理当前SKU 分配 比如：SKU在A货格 我们这个时候选置一个A货格备用，在A货格占用的情况下 放置到 A货格备用
        /// ④考虑设备故障等因素进行禁用 不进行分配
        /// </summary>

       
        private void DistributionUnits()
        {
            //假设播种墙货格已经处于折线分布状态
        }

        #endregion


        #region 创建任务
        private void CreateTask(string container_no, string sku_no, out UtilsResult utilsResult)
        {
            //声明初始值 -返回结果
            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };
            //看一下条码是否存在任务  不存在在创建 
            var taskExist = sqlSugarClient.Queryable<table_rcs_task_master>().Where(r=>r.CONTAINER_NO == container_no).Count();
            if (taskExist >0 ) {
                utilsResult = new UtilsResult()
                {
                    ri_ret = -1,
                    rs_ret = container_no+"已存在任务！不允许创建新的任务"
                };
                return;
            }
            //开始创建任务
            //声明对象
            var t_rcs_task_m = new table_rcs_task_master() {
                TASK_ID = Guid.NewGuid().ToString(),
                TASK_TYPE = "IN",
                STATUS = 1,//新生成
                FROM_ADDR = "JXT",
                TO_ADDR ="",
                CONTAINER_NO = container_no,
                CREATE_TIME = DateTime.Now,
                PRIORITY = 1,
                NOTE_M = DateTime.Now+",创建，容器号："+ container_no
            };

            //先创建主任务
            var insertMCount = sqlSugarClient.Insertable<table_rcs_task_master>(t_rcs_task_m).ExecuteCommand();
            if (insertMCount == 0)
            {
                utilsResult = new UtilsResult()
                {
                    ri_ret = -1,
                    rs_ret = container_no + "插入任务主表失败！"
                };
                return;
            }

            //声明任务子表
            var t_rcs_task_d = new table_rcs_task_detail()
            {
                TASK_ID = Guid.NewGuid().ToString(),
                LINE_NO = 1,
                SKU = sku_no,
                SKU_NAME = "",
                BATCH = "",
                SKU_NUM = 1,
                NOTE_D =  DateTime.Now + ",创建，容器号：" + container_no
            };
            //先创建主任务
            var insertDCount = sqlSugarClient.Insertable<table_rcs_task_detail>(t_rcs_task_d).ExecuteCommand();
            if (insertDCount == 0) {

                utilsResult = new UtilsResult()
                {
                    ri_ret = -1,
                    rs_ret = container_no + "插入任务子表失败！"
                };
                return;
            }

        }
        #endregion

        #region 任务分配
        private void TaskDistribution(string task_no, int status, out UtilsResult utilsResult)
        {
            //-------1新生成   3执行中    10已完成
            utilsResult = new UtilsResult()
            {
                ri_ret = 0,
                rs_ret = ""
            };


        }

        #endregion

        #region 
        #endregion

        #region 
        #endregion

        #region 
        #endregion

        #region 
        #endregion

        #region 
        #endregion

        #region 
        #endregion

    }
}
